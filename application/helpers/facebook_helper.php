<?php

class FacebookHelper
{
    private $ci;
    private $uri;
    private $sec;

    public function __construct()
    {
        /* $this->ci = setting('facebook-app-id');
        $this->uri = setting('facebook-app-redirect');
        $this->sec = setting('facebook-app-secret');
        */
        
        $this->ci = '1030202554168958';
        $this->uri = CF_BASE_URL.'/facebook-redirect';
        $this->sec ='2e9ff8d3aebd563414083a7c0a4f1d6b';
    }

    public function getLink()
    {
        //$loginLink = 'https://www.facebook.com/oauth/v2/authorization?response_type=code';
        $loginLink = 'https://www.facebook.com/v9.0/dialog/oauth?response_type=code';
        
        $loginLink .= '&client_id='.$this->ci;
        $loginLink .= '&redirect_uri='.$this->uri;
        $loginLink .= '&state=adsfa3432asdf';
        $loginLink .= '&scope=public_profile,email,user_location';

        return $loginLink;
    }

    public function getAccessToken($code)
    {
       
        //http://www.facebook.com/dialog/oauth?client_id=203311771439272&redirect_uri=https://developers.facebook.com/docs/graph-api/overview&scope=email
        $url = 'https://graph.facebook.com/oauth/access_token'; 
        $url .= '?grant_type=authorization_code';
        
        $url .= '&code='.$code;
        $url .= '&redirect_uri='.$this->uri;
        $url .= '&client_id='.$this->ci;
        $url .= '&client_secret='.$this->sec;
        $result = json_decode(file_get_contents($url));
          //print_r($url);
        //$cURLConnection = curl_init();

        //curl_setopt($cURLConnection, CURLOPT_URL, $url);
        //curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

        //result = curl_exec($cURLConnection);
        //curl_close($cURLConnection);
        //var_dump($result);die();

        //$jsonArrayResponse - json_decode($result);

        return isset($result->access_token) ? $result->access_token : '';
    }

    public function getFacebookRefinedData($accessToken)
    {
         //print_r($accessToken);
         $emailData = $this->fetchFacebookData($accessToken, 'email');
        // var_dump($emailData);
          $idNameImageData = $this->fetchFacebookData($accessToken,null,$emailData['id']);
          //var_dump($idNameImageData);die;
         // $email = isset($emailData['elements'][0]['handle~']['emailAddress']) ? $emailData['elements'][0]['handle~']['emailAddress'] : '';
          //var_dump($emailData);die();
          $firstName = isset($emailData['first_name']) ? $emailData['first_name'] : '';
          $lastName = isset($emailData['last_name']) ? $emailData['last_name'] : '';;
          $address = isset($emailData['address'])?$emailData['address']:(isset($emailData['hometown'])?$emailData['hometown'].', '.isset($emailData['location'])?$emailData['location']:'':'');
          /*$lastNameA = isset($idNameImageData['lastName']['localized']) ? $idNameImageData['lastName']['localized'] : '';
          foreach($lastNameA as $lname){
              $lastName = $lname;
          }*/
         // $image = isset($idNameImageData['profilePicture']['displayImage~']['elements'][0]['identifiers'][0]['identifier']) ?  $idNameImageData['profilePicture']['displayImage~']['elements'][0]['identifiers'][0]['identifier'] : '';
          // var_dump($firstName);
          $image=$idNameImageData['data']['url'];
          //$id = isset($idNameImageData['id']) ? $idNameImageData['id'] : '';
          $email = isset($emailData['email']) ? $emailData['email'] : '';
          $id = isset($emailData['id']) ? $emailData['id'] : '';
          return array(
              'email' => $email,
              'first_name' => $firstName,
              'last_name' => $lastName,
              'image' => $image,
              'id' => $id,
              'address'=>$address
          );
    }

    private function fetchFacebookData($access_token, $type = '',$id = null) {
        if ($type == 'email') {
            $l = 'https://graph.facebook.com/v2.12/me?fields=id,name,email,first_name,last_name,address,hometown,location&access_token='.$access_token;
        } else {
            $l ='https://graph.facebook.com/v9.0/'.$id.'/picture?access_token='.$access_token.'&redirect=false&size=large';
        }
        
        $curl = curl_init();
        $options = array(
            CURLOPT_URL => $l,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => array(
            'authorization: Bearer '.$access_token,
            'cache-control: no-cache',
            'connection: Keep-Alive',
            'Content-Type: application/json'
            ),
        );
        curl_setopt_array($curl, $options);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        //    var_dump($response);
        if ($err) {
            return 'failed';
        } else {
            $response = json_decode($response, true);
            return $response;
        }
    }    
}