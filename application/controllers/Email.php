<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'vendor/autoload.php';
// require_once FCPATH . 'vendor/sendgrid/sendgrid/sendgrid-php.php';

class Email extends CI_Controller
{/**
 * View Function to display account resume detail page
 *
 */
    function __construct()
    {
        parent::__construct();
        // $this->checkLogin();
        $this->load->model('AdminEmailTemplateModel', 'admin_email_template_model');
        $this->load->model('EmailModel');
        $this->load->library('email');

    }

    public function sendadmin()
    {	
        
        {	
    	
        $config = [];
		$config['protocol'] = "smtp";
		$config['smtp_host'] = "smtp.gmail.com";
		$config['smtp_user'] = "htmage.smtp@gmail.com";
		$config['smtp_pass'] = "myopqpubnuaamljy";
		$config['smtp_port'] = 587;
		$config['smtp_crypto'] = "tls";

		$config['crlf'] = "\r\n";
		$config['newline'] = "\r\n";
                
		$this->email->initialize($config);

    	$from_email = $_POST['email'];
    	$to_email = 'notificaciones@aspirantecertificadomkt.com';

    	if (!empty($_POST['firstname']) && !empty($_POST['email'])) {
    		$this->email->from($from_email, 'Aspirantecertificado notification');
	        $this->email->to($to_email);
	        $this->email->subject('You have an email from visitor');
	        $message = "Here is the information submitted \n" . "\n";
	        $message .= "First name: ". $_POST['firstname']. "\n";
	        $message .= "Last name: ". $_POST['lastname']. "\n";
	        $message .= "Email: ". $_POST['email']. "\n";
	        $message .= "Phone: ". $_POST['phone']. "\n";
	        $message .= "Country: ". $_POST['pais']. "\n";
	        $message .= "City: ". $_POST['city']. "\n";
	        $message .= "Company: ". $_POST['company']. "\n";
	        $message .= "Number of employees: ". $_POST['numemployees']. "\n";
	        $message .= "Looking for job: ". $_POST['lookingforjob']. "\n";
	        $message .= "Describe functions: ". $_POST['describfunctions']. "\n";
	        $message .= "Product interest: ". $_POST['productinterest']. "\n";
	        $message .= "Find about here: ". $_POST['findabouthere']. "\n";

	       
	        $this->email->message($message);
	        $this->email->send();
    	}
        
       
        return redirect()->to('/');
		
    }
		
    }

}