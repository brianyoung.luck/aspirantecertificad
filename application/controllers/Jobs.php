<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'vendor/autoload.php';
require_once FCPATH . 'vendor/sendgrid/sendgrid/sendgrid-php.php';

class Jobs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('EmailModel');
        $this->load->library('email');

    }

    /**
     * View Function to display account job listing page
     *
     * @return html/string
     */
    public function listing($page = null)
    {
        $search = urldecode($this->xssCleanInput('search', 'get'));
        $companies = $this->xssCleanInput('companies', 'get');
        $departments = $this->xssCleanInput('departments', 'get');
        $limit = setting('jobs-limit');
        $pageData['page'] = 'Job Listing | ' . setting('site-name');
        $data['page'] = 'jobs';
        $data['jobs'] = $this->JobModel->getAll($page, $search, $companies, $departments, $limit);
        $data['jobFavorites'] = $this->JobModel->getFavorites();
        $data['companies'] = $this->CompanyModel->getAll();
        $data['departments'] = $this->DepartmentModel->getAll();
        $data['pagination'] = $this->getPagination($page, $search, $companies, $departments, $limit);
        $data['search'] = $search;
        $data['companiesSel'] = $companies;
        $data['departmentsSel'] = $departments;
        $this->load->view('front/layout/header', $pageData);
        $this->load->view('front/jobs-listing', $data);
    }    

    /**
     * View Function to display jobs listing page
     *
     * @return html/string
     */
    public function detail($id = null)
    {
        $search = urldecode($this->xssCleanInput('search', 'get'));
        $companies = urldecode($this->xssCleanInput('search', 'get'));
        $departments = $this->xssCleanInput('departments', 'get');
        $data['job'] = $this->JobModel->getJob($id);
        if (!$data['job']) {
            redirect('404_override');
        }
        $data['jobFavorites'] = $this->JobModel->getFavorites();
        $data['companies'] = $this->CompanyModel->getAll();
        $data['departments'] = $this->DepartmentModel->getAll();
        $data['search'] = $search;
        $data['companiesSel'] = $companies;
        $data['departmentsSel'] = $departments;
        $data['resumes'] = $this->ResumeModel->getCandidateResumesList();
        $data['applied'] = $this->JobModel->getAppliedJobs();
        $data['resume_id'] = $this->ResumeModel->getFirstDetailedResume();

        $pageData['page'] = $data['job']['title'].' | ' . setting('site-name');

        $this->load->view('front/layout/header', $pageData);
        $this->load->view('front/job-detail', $data);
    } 

    /**
     * Function to mark jobs as favorite
     *
     * @return html/string
     */
    public function markFavorite($id = null)
    {
        if (candidateSession()) {
            if ($this->JobModel->markFavorite($id)) {
                echo json_encode(array('success' => 'true', 'messages' => ''));
            }
        } else {
            echo json_encode(array('success' => 'false', 'messages' => ''));
        }
    } 

    /**
     * Function to unmark jobs as favorite
     *
     * @return html/string
     */
    public function unmarkFavorite($id = null)
    {
        $this->JobModel->unmarkFavorite($id);
        echo json_encode(array('success' => 'true', 'messages' => ''));
    } 

    /**
     * Function to display refer job form
     *
     * @return html/string
     */
    public function referJobView()
    {
        echo $this->load->view('front/partials/refer-job', array(), TRUE);
    } 

    public function recordvideoview()
    {
        echo $this->load->view('front/partials/record_video', array(), TRUE);
    } 
    
    public function uploaddocumentview()
    {
        echo $this->load->view('front/partials/upload_document', array(), TRUE);
    } 

    /**
     * Function to refer job to a person
     *
     * @return html/string
     */
    public function referJob($id = null)
    {
        $this->checkIfDemo();
        if (candidateSession()) {
            $this->form_validation->set_rules('email', 'Email', 'required|min_length[2]|max_length[50]|valid_email');
            $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[2]|max_length[50]');
            $this->form_validation->set_rules('phone', 'Phone', 'max_length[50]|numeric');

            if ($this->form_validation->run() === FALSE) {
                echo json_encode(array(
                    'success' => 'error',
                    'messages' => $this->ajaxErrorMessage(array('error' => validation_errors()))
                ));
            } else if ($this->JobModel->ifAlreadyReferred()) {
                $this->JobModel->referJob();
                echo json_encode(array(
                    'success' => 'error',
                    'messages' => $this->ajaxErrorMessage(array('error' => lang('job_is_already_referred')))
                ));
            } else {
                $this->JobModel->referJob();
                $job_id = $this->xssCleanInput('job_id');
                $name = $this->xssCleanInput('name');
                $user = candidateSession('first_name').' '.candidateSession('last_name');
                $message = $this->load->view('front/emails/refer-job', compact('job_id', 'user', 'name'), TRUE);
                $this->sendEmail(
                    $message,
                    $this->xssCleanInput('email'),
                    'Job Referred'
                );
                echo json_encode(array(
                    'success' => 'true',
                    'messages' => $this->ajaxErrorMessage(array('success' => lang('job_referred_successfully')))
                ));
            }
        } else {
            echo json_encode(array('success' => 'false', 'messages' => ''));
        }
    }

    /**
     * Function to apply to a job
     *
     * @return html/string
     */
    public function applyJob($id = null)
    {
        $this->checkIfDemo();
        if (candidateSession()) {
            if (setting('enable-multiple-resume') == 'yes') {
                $this->form_validation->set_rules('resume', 'Resume', 'required');

                $job = $this->JobModel->getJob($this->xssCleanInput('job_id'));
                $resume = $this->ResumeModel->getFirst('resumes.resume_id', decode($this->xssCleanInput('resume')));

                if ($this->form_validation->run() === FALSE) {
                    echo json_encode(array(
                        'success' => 'error',
                        'messages' => $this->ajaxErrorMessage(array('error' => validation_errors()))
                    ));
                } elseif ($job['is_static_allowed'] != 1 && $resume['type'] != 'detailed') {
                    echo json_encode(array(
                        'success' => 'error',
                        'messages' => $this->ajaxErrorMessage(array('error' => lang('you_need_to_apply_via_detailed')))
                    ));
                } else {
                    $this->JobModel->applyJob();
                    echo json_encode(array(
                        'success' => 'true',
                        'messages' => $this->ajaxErrorMessage(array('success' => lang('job_applied_successfully')))
                    ));
                }
 
            } else {
                $this->JobModel->applyJob();
                echo json_encode(array(
                    'success' => 'true',
                    'messages' => $this->ajaxErrorMessage(array('success' => lang('job_applied_successfully')))
                ));
            }
        } else {
            echo json_encode(array('success' => 'false', 'messages' => ''));
        }
    }

    /**
     * View Function to display candidate job applications page
     *
     * @param integer $page
     * @return html/string
     */
     
    public function jobApplicationsView($page = null)
    {
        $this->checkLogin();
        $candidateData = $this->session->userdata('candidate');
        $total = $this->JobModel->getTotalAppliedJobs();
        $limit = 5;
        $data['pagination'] = $this->createPagination($page, '/account/job-applications/', $total, $limit);
        $pageData['page'] = lang('job_applications').' | ' . setting('site-name');
        
        $data['jobs'] = $this->JobModel->getAppliedJobsList($page, $limit);
        
        $data['page'] = 'applications';
        $this->db->where('candidate_id', $candidateData['candidate_id']);
        $this->db->select('*');
        $this->db->from('candidates');
        $result = $this->db->get();
        $candidateViewData = ($result->num_rows() == 1) ? objToArr($result->row(0)) : array();
        
        $data['candidates'] = $candidateViewData;

        if (isset($_GET["video_interview"]) && ($_GET["video_interview"])) {
            $videoUrl = $_GET["video_interview"];
            $this->db->where('candidate_id', $candidateData['candidate_id']);
            $this->db->update('candidates', ['video_interview' => "https://aspirantecertificado.com/".$videoUrl , 'updated_at' => date('Y-m-d G:i:s')]);
            
            $this->db->where('candidate_id', $candidateData['candidate_id']);
            $this->db->select('*');
            $this->db->from('candidates');
            $result = $this->db->get();
            $candidateViewData = ($result->num_rows() == 1) ? objToArr($result->row(0)) : array();
            
            $email = new \SendGrid\Mail\Mail();
            $email->setFrom('notificaciones@aspirantecertificadomkt.com', 'Aspirantecertificado notificaciones');
            $email->setSubject('Candidate has updated video');
            $email->addTo('agentes@aspirantecertificadomkt.com', 'Agentes');
            $email->addContent("text/plain", "subject");
            $email->addContent(
                "text/html",'<body ><div style="width:500px; margin:10px auto; background:#f1f1f1; border:1px solid #ccc">
                    <table  width="100%" border="0" cellspacing="5" cellpadding="10">
                        
                        <tr>
                            <td style="font-size:22px; text-align:center"> Candidate has updated video </td>
                        </tr>
                        <tr>
                            <td style="font-size:14px; color:#323232">First name: '.$candidateData['first_name'].'</td>
                        </tr>
                        <tr>
                            <td style="font-size:14px; color:#323232">Last name: '.$candidateData['last_name'].'</td>
                        </tr>
                        <tr>
                            <td style="font-size:14px; color:#323232">Email: '.$candidateData['email'].'</td>
                        </tr>
                        <tr>
                            <td style="font-size:14px; color:#323232">Created: '.$candidateViewData['updated_at'].'</td>
                        </tr>
                        <tr>
                            <td style="font-size:14px; color:#323232">You can check out the candidates at this link https://www.aspirantecertificado.com/admin/candidates</td>
                        </tr>
                        
                    </table></div></body>');
        
            $sendgrid = new \SendGrid(('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'));
           
            try {
                $response = $sendgrid->send($email);
                if($response->statusCode()==202){
                    echo json_encode(array(
                        'success' => 'true',
                        'messages' => $this->ajaxErrorMessage(array('success' => 'Success!' ))
                    ));
                }else{
                    echo json_encode(array(
                        'success' => 'false',
                        'messages' => $this->ajaxErrorMessage(array('error' => "faild" )) 
                    ));
                }
            } catch (Exception $e) {
                echo json_encode(array(
                    'success' => 'false',
                    'messages' => $this->ajaxErrorMessage(array('error' => $e->getMessage() )) 
                ));
            }
    
            redirect('account/job-applications');
        }
        $this->load->view('front/layout/header', $pageData);
        $this->load->view('front/account-job-applications', $data);
    }

    /**
     * View Function to display candidate job favorites page
     *
     * @param integer $page
     * @return html/string
     */
    public function jobFavoritesView($page = null)
    {
        $this->checkLogin();
        $total = $this->JobModel->getTotalFavoriteJobs();
        $limit = 5;
        $data['pagination'] = $this->createPagination($page, '/account/job-favorites/', $total, $limit);
        $pageData['page'] = lang('job_favorites').' | ' . setting('site-name');
        $data['jobs'] = $this->JobModel->getFavoriteJobsList($page, $limit);
        $data['page'] = 'favorites';
        $this->load->view('front/layout/header', $pageData);
        $this->load->view('front/account-job-favorites', $data);
    }

    /**
     * View Function to display candidate job referred page
     *
     * @param integer $page
     * @return html/string
     */
    public function jobReferredView($page = null)
    {
        $this->checkLogin();
        $total = $this->JobModel->getTotalReferredJobs();
        $limit = 5;
        $data['pagination'] = $this->createPagination($page, '/account/job-referred/', $total, $limit);
        $pageData['page'] = lang('job_referred').' | ' . setting('site-name');
        $data['jobs'] = $this->JobModel->getReferredJobsList($page, $limit);
        $data['jobFavorites'] = $this->JobModel->getFavorites();
        $data['page'] = 'referred';
        $this->load->view('front/layout/header', $pageData);
        $this->load->view('front/account-job-referred', $data);
    }
    
    /**
     * Private function to create pagination for jobs listing
     *
     * @return html/string
     */
    private function getPagination($page, $search, $companies, $departments, $limit)
    {
        $total = $this->JobModel->getTotal($search, $companies, $departments);
        $url = '/jobs/';
        return $this->createPagination($page, $url, $total, $limit);
    }    
}

