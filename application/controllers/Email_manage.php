<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'vendor/autoload.php';

class Email_manage extends CI_Controller
{/**
 * View Function to display account resume detail page
 *
 */
    function __construct()
    {
        parent::__construct();
        $this->checkLogin();
        $this->load->model('AdminEmailTemplateModel', 'admin_email_template_model');
        $this->load->model('EmailModel');
        $this->load->library('email');
    }
    public function actionEmail()
    {


        $data['templateList'] = $this->admin_email_template_model->map('id','name', 1, 1);
        $pageData['page'] = lang('email_manage').' | ' . setting('site-name');
        $data['page'] = 'email_manage';
        $this->load->view('front/layout/header', $pageData);
        $this->load->view('front/email_form', $data);
       // $this->load->view('front/email_compose', $data);
       // $this->load->view('front/email_inbox', $data);
       // $this->load->view('front/email_send', $data);
       // $this->load->view('front/email_trash', $data);
    }
    public function actionEmailSendList()
    {
        $pageData['page'] = lang('email_manage').' | ' . setting('site-name');
        $data['page'] = 'email_manage';
        $this->load->view('front/layout/header', $pageData);
        $this->load->view('front/email_send', $data);

    }
    public function actionEmailInboxList()
    {
        $pageData['page'] = lang('email_manage').' | ' . setting('site-name');
        $data['page'] = 'email_manage';
        $this->load->view('front/layout/header', $pageData);
        $this->load->view('front/email_inbox', $data);

    }
    public function actionEmailTrashList()
    {
        $pageData['page'] = lang('email_manage').' | ' . setting('site-name');
        $data['page'] = 'email_manage';
        $this->load->view('front/layout/header', $pageData);
        $this->load->view('front/email_trash', $data);

    }

    public function sendEmailFunction(){
        //var_dump($_POST);die();
        if($_POST['form_message']==''){
            $this->session->set_flashdata('flash_msg','<div class="alert alert-danger" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>Message Field Is Required</div>');
            redirect('email/action');
        }

        if($_POST['email']==''){
            $this->session->set_flashdata('flash_msg','<div class="alert alert-danger" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>Email Field Is Required</div>');
            redirect('email/action');
        }
        $searchForValue = ',';


        $attachment='';
        if($_FILES['attachFile']){
            $i=1;
            foreach($_FILES['attachFile'] as $eachFile){

                if($i>1){
                    $attachment='||'.$eachFile;
                }else{
                    $attachment=$eachFile;
                }

                $i++;
            }
        }
        $mail_to= $this->input->post('email');
        $mail_subject= $this->input->post('subject');
        $mail_content= $this->input->post('form_message');

        $data['sent_to']=$mail_to;
        $data['subject']=$mail_subject;
        $data['candidate_id'] = candidateSession();
        $data['attachment']=$attachment;
        $data['status']=1;
        $data['send']=1;
        $data['created']=date('Y-m-d');
        $data['modified']=date('Y-m-d');

        $mail_content = $this->admin_email_template_model->beforeSendEmail($_POST['template_id'],$mail_content);
        $data['mail_body']=$mail_content;
        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';
        // exit;
        //var_dump($mail_content);die;
        $path = FCPATH.$attachment;
        $insert= 0;
        $send_status = 0;
        if( strpos($_POST['email'], $searchForValue) !== false ) {
            $stringValue = trim(',',$_POST['email']);
            $explode = explode(',',$_POST['email']);
            if(count($explode)>0){
                foreach ($explode as $email){
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        $this->session->set_flashdata('flash_msg','<div class="alert alert-danger" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>Email Field Is Required</div>');
                        redirect('email/compose');
                    }
                    $mail_to = $email;
                    if($this->sendEmail($mail_content, $mail_to, $mail_subject, $path)) {
                        $this->EmailModel->saveData('sdcandidates_email', $data);
                        $send_status = 1;
                    }
                }
            }
        }else{
            if($this->sendEmail($mail_content, $mail_to, $mail_subject, $path)) {
                $this->EmailModel->saveData('sdcandidates_email', $data);
                $send_status = 1;
            }
			else{
				$this->session->set_flashdata('flash_msg','<div class="alert alert-danger" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>Email Not Sent</div>');
            redirect('email/compose');
			}
        }

        if($send_status==1){
            $this->session->set_flashdata('flash_msg','<div class="alert alert-success" role="alert">
<button type="button" class="close" data-dismiss="sucess" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>Email Sent Successfully</div>');
            redirect('email/compose');
        }else{
            $this->session->set_flashdata('flash_msg','<div class="alert alert-danger" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>Email Not Sent</div>');
            redirect('email/compose');
        }
    }

}

