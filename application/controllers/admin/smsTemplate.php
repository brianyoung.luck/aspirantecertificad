<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class smsTemplate extends CI_Controller
{
	
    /**
     * Constructor
     * 
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->checkAdminLogin();
		
    }

    /**
     * View Function to display blogs list view page
     *
     * @return html/string
     */
	 public function index(){
		
		 $this->load->model('AdminSmsTemplateModel');
		 
		$data['page'] = lang('SMS Template');
        $data['menu'] = 'SMS Template';
        $this->load->view('admin/layout/header', $data);
        
		$pageData['smses'] = objToArr($this->AdminSmsTemplateModel->getAll(false));
		$this->load->view('admin/sms_templates/index',$pageData);
	 }

    /**
     * Function to get data for blogs jquery datatable
     *
     * @return json
     */
    public function data()
    {
		$this->load->model('AdminSmsTemplateModel');
        echo json_encode($this->AdminSmsTemplateModel->list());
    }    

    /**
     * View Function (for ajax) to display create or edit view page via modal
     *
     * @param integer $id
     * @return html/string
     */
    public function createOrEdit($id = NULL)
    {
		$this->load->model('AdminSmsTemplateModel');
		
        $pagedata['template'] = objToArr($this->AdminSmsTemplateModel->get('sms_templates.id', $id));
        
        $data['page'] = lang('SMS Template');
        $data['menu'] = 'SMS Template';
        $this->load->view('admin/layout/header', $data);
        $this->load->view('admin/sms_templates/create-or-edit', $pagedata);       
	}
    /**
     * Function (for ajax) to process email template create or edit form request
     *
     * @return redirect
     */
    public function save()
    {
		//echo json_encode($_POST);die;
		$this->load->model('AdminSmsTemplateModel');
        $this->checkIfDemo();
		
        //$this->form_validation->set_rules('name', 'name', 'trim|required');
        //$this->form_validation->set_rules('template', 'template', 'required');

        $data = $this->xssCleanInput();
		 if (strpos($data['template'],'{{contents}}') == false) {
            echo json_encode(array(
                'success' => 'false',
                'messages' => $this->ajaxErrorMessage(array('error' => "In Your Email Template must have {{contents}} tag. To replace email text when you send email."))
                
            ));
			die;
        }
		
		$newId = $this->AdminSmsTemplateModel->store($data);
        echo json_encode(array(
            'success' => 'true',
            'messages' => $this->ajaxErrorMessage(array('success' => lang('SMS Template') .' '. ($data['id'] ? lang('updated') : lang('created')))),
            'data' => $data
        ));
         //$data = $this->AdminEmailTemplateModel->store($edit);
            // echo json_encode(array(
                // 'success' => 'true',
                // 'messages' => $this->ajaxErrorMessage(array('success' => lang('Email Template') . ($edit ? lang('updated') : lang('created')))),
                // 'data' => $data
            // ));
        // if ($this->form_validation->run() === FALSE) {
            // echo json_encode(array(
                // 'success' => 'false',
                // 'messages' => $this->ajaxErrorMessage(array('error' => validation_errors()))
                // 'messages' => $this->ajaxErrorMessage(array('error' => $__POST['name']))
                // 'messages' => $this->ajaxErrorMessage(array('error' => 'hello'))
            // ));
        // } elseif ($this->AdminEmailTemplateModel->valueExist('name', $this->xssCleanInput('name'), $edit)) {
            // echo json_encode(array(
                // 'success' => 'false',
                // 'messages' => $this->ajaxErrorMessage(array('error' => lang('Email_Template_already_Exists')))
            // ));
        // } else {
            // $data = $this->AdminEmailTemplateModel->store($edit);
            // echo json_encode(array(
                // 'success' => 'true',
                // 'messages' => $this->ajaxErrorMessage(array('success' => lang('Email Template') . ($edit ? lang('updated') : lang('created')))),
                // 'data' => $data
            // ));
        // }
		
    }

     /**
     * Function (for ajax) to process Template change status request
     *
     * @param integer $id
     * @param string $status
     * @return void
     */
    public function changeStatus($id = null, $status = null)
    {
		$this->load->model('AdminSmsTemplateModel');
        $this->checkIfDemo();
        $this->AdminSmsTemplateModel->changeStatus($id, $status);
    }

     /**
     * Function (for ajax) to process Template change user access request
     *
     * @param integer $id
     * @param string $userAccess
     * @return void
     */
    public function changeUserAccess($id = null, $userAccess = null)
    {
		$this->load->model('AdminSmsTemplateModel');
        $this->checkIfDemo();
        $this->AdminSmsTemplateModel->changeUserAccess($id, $userAccess);
    }

    /**
     * Function (for ajax) to process blog delete request
     *
     * @param integer $id
     * @return void
     */
    public function delete($id)
    {
		$this->load->model('AdminSmsTemplateModel');
        $this->checkIfDemo();
        $this->AdminSmsTemplateModel->remove($id);
		redirect('admin/sms-templates');
    }

    /**
     * Upload Function (for ajax) to upload images from the ckeditor window
     *
     * @return html
     */
    public function uploadCkEditorImages()
    {
        if (isset($_FILES['upload']['name'])) {
            $file = $_FILES['upload']['tmp_name'];
            $file_name = $_FILES['upload']['name'];
            $file_name_array = explode('.', $file_name);
            $extension = end($file_name_array);
            $new_image_name = makeSlug($file_name).'-'.rand().'.'.$extension;
            chmod(ASSET_ROOT.'/images/ckeditor/', 0777);
            $allowed_extension = array('jpg', 'gif', 'png');
            if (in_array($extension, $allowed_extension)) {
                move_uploaded_file($file, ASSET_ROOT.'/images/ckeditor/'.$new_image_name);
                $function_number = $_GET['CKEditorFuncNum'];
                $url = base_url().'/assets/images/ckeditor/'.$new_image_name;
                $message = '';
                echo "<script>window.parent.CKEDITOR.tools.callFunction('".$function_number."', '".$url."', '".$message."');</script>";
            }
        }
    }    
}
