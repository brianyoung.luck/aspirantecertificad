<?php

class EmailModel extends CI_Model
{
    protected $table = 'sdcandidates_email';
    protected $key = 'id';


	public function get($column, $value)
    {
        $this->db->where($column, $value);
        $result = $this->db->get($this->table);
        return ($result->num_rows() == 1) ? $result->row(0) : $this->emptyObject($this->table);
    }
	public function store($data)
    {
		unset($data['csrf_token']);
		$edit = isset($data['id'])?$data['id']:null;
        if ($edit) {
            $this->db->where('id', $edit);
            $data['updated_at'] = date('Y-m-d G:i:s');
            $this->db->update($this->table, $data);
        } else {
            $data['created_at'] = date('Y-m-d G:i:s');
			
            $this->db->insert($this->table, $data);
            $id = $this->db->insert_id();
            return $id;
        }
    }
	public function remove($id)
    {
        $this->db->delete($this->table, array('id' => $id));
    }
	public function getTotal($status = null)
    {
        $this->db->from($this->table);
        if ($status) {
            $this->db->where('status', $status);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
	public function getAll($active = true,$userAccess = null)
    {
        if ($active) {
            $this->db->where('status', 1);
        }
		if(!is_null($userAccess)){
			$this->db->where('user_access', $userAccess);
		}
        $this->db->from($this->table);
        $query = $this->db->get();
        return objToArr($query->result());
    }
    public function inbox($userId = null,$value = null,$status = 1){
		
        $this->db->where('status', $status);
        if(!is_null($userId)){
			$this->db->where('user_id', $userId);
		}
    
		if(!is_null($value)){
			$this->db->where('inbox', $value);
		}
        $this->db->from($this->table);
        $query = $this->db->get();
        return objToArr($query->result());
	}
	public function send($userId = null,$value = null,$status = 1){
		
        $this->db->where('status', $status);
		if(!is_null($userId)){
			$this->db->where('user_id', $userId);
		}
		if(!is_null($value)){
			$this->db->where('send', $value);
		}
        $this->db->from($this->table);
        $query = $this->db->get();
        return objToArr($query->result());
	}
	public function trash($userId = null,$value = null,$status = 1){
		
        $this->db->where('status', $status);
    
		if(!is_null($value)){
			$this->db->where('trash', $value);
		}
		if(!is_null($userId)){
			$this->db->where('user_id', $userId);
		}
        $this->db->from($this->table);
        $query = $this->db->get();
        return objToArr($query->result());
	}
    // Email Data Insert
    public function saveData($table, $data=array())
    {
        $this->db->insert($table, $data);
		return $this->db->insert_id();
    }


}