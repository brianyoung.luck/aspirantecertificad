<?php

class AdminSmsTemplateModel extends CI_Model
{
    protected $table = 'sms_templates';
    protected $key = 'id';

    public function get($column, $value)
    {
        $this->db->where($column, $value);
        $result = $this->db->get($this->table);
        return ($result->num_rows() == 1) ? $result->row(0) : $this->emptyObject($this->table);
    }

    public function store($data)
    {
		unset($data['csrf_token']);
		$edit = isset($data['id'])?$data['id']:null;
        if ($edit) {
            $this->db->where('id', $edit);
            $data['updated_at'] = date('Y-m-d G:i:s');
            $this->db->update($this->table, $data);
        } else {
            $data['created_at'] = date('Y-m-d G:i:s');
			
            $this->db->insert($this->table, $data);
            $id = $this->db->insert_id();
            return $id;
        }
    }

    public function changeStatus($id, $status)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, array('status' => ($status == 1 ? 0 : 1)));
    }
	
	public function changeUserAccess($id, $value)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, array('user_access' => ($value == 1 ? 0 : 1)));
    }
	
    public function remove($id)
    {
        $this->db->delete($this->table, array('id' => $id));
    }

    public function valueExist($field, $value, $edit = false)
    {
        $this->db->where($field, $value);
        if ($edit) {
            $this->db->where('id !=', $edit);
        }
        $query = $this->db->get($this->table);
        return $query->num_rows() > 0 ? true : false;
    }

    public function getAll($active = true,$userAccess = null)
    {
        if ($active) {
            $this->db->where('status', 1);
        }
		if(!is_null($userAccess)){
			$this->db->where('user_access', $userAccess);
		}
        $this->db->from($this->table);
        $query = $this->db->get();
        return objToArr($query->result());
    }

    public function getTotal($status = null)
    {
        $this->db->from($this->table);
        if ($status) {
            $this->db->where('status', $status);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function map($column1, $column2, $status = 1, $userAccess = null){
		$data = $this->getAll($status,$userAccess);
		$return = [] ;
		foreach($data as $d){
			$return[$d[$column1]] = $d[$column2];
		}
		return $return;
	}
	public function beforeSendSms($templateId,$smsText){
		$this->db->from($this->table);
		$this->db->where('id',$templateId);
		$q = $this->db->get();
		$d = objToArr($q->result());
		$ET = htmlentities($smsText);
		return html_entity_decode(str_replace('{{contents}}',$ET,$d['template']));
	}
	
		public function list()
    {
        $request = $this->input->get();
        $columns = array(
            "",
            "sms_templates.name",
            "sms_templates.user_access",
            "sms_templates.created_at",
            "sms_templates.status",
        );
        $orderColumn = $columns[($request['order'][0]['column'] == 0 ? 5 : $request['order'][0]['column'])];
        $orderDirection = $request['order'][0]['dir'];
        $srh = $request['search']['value'];
        $limit = $request['length'];
        $offset = $request['start'];

        $this->db->from($this->table);
        $this->db->select('
            sms_templates.*
        ');
        if ($srh) {
            $this->db->group_start()->like('name', $srh)->group_end();
        }
        if (isset($request['status']) && $request['status'] != '') {
            $this->db->where('sms_templates.status', $request['status']);
        }
        $this->db->group_by('sms_templates.id');
        $this->db->order_by($orderColumn, $orderDirection);
        $this->db->limit($limit, $offset);
        $query = $this->db->get();

        $result = array(
            'data' => $this->prepareDataForTable($query->result()),
            'recordsTotal' => $this->getTotal(),
            'recordsFiltered' => $this->getTotal($srh, $request),
        );

        return $result;
    }
	private function prepareDataForTable($blogs)
    {
        $sorted = array();
        foreach ($blogs as $b) {
            $actions = '';
            $b = objToArr($b);
            if ($b['status'] == 1) {
                $button_text = lang('active');
                $button_class = 'success';
                $button_title = lang('click_to_deactivate');
            } else {
                $button_text = lang('inactive');
                $button_class = 'danger';
                $button_title = lang('click_to_activate');
            }
			if ($b['user_access'] == 1) {
                $user_access_button_text = lang('active');
                $user_access_button_class = 'success';
                $user_access_button_title = lang('click_to_deactivate');
            } else {
                $user_access_button_text = lang('inactive');
                $user_access_button_class = 'danger';
                $user_access_button_title = lang('click_to_activate');
            }
            if (allowedTo('edit_blog')) { 
            $actions .= '
                <button type="button" class="btn btn-primary btn-xs create-or-edit-blog" data-id="'.$b['id'].'"><i class="far fa-edit"></i></button>
            ';
            }
            if (allowedTo('delete_blog')) { 
            $actions .= '
                <button type="button" class="btn btn-danger btn-xs delete-template" data-id="'.$b['id'].'"><i class="far fa-trash-alt"></i></button>
            ';
            }
            $default_image = base_url().'assets/images/not-found.png';
            $sorted[] = array(
                "<input type='checkbox' class='minimal single-check' data-id='".$b['id']."' />",
                esc_output($b['name']),
                '<button type="button" title="'.$user_access_button_title.'" class="btn btn-'.$user_access_button_class.' btn-xs change-template-user-access" data-status="'.$b['user_access'].'" data-id="'.$b['id'].'">'.$user_access_button_text.'</button>',
                date('d M, Y', strtotime($b['created_at'])),
                '<button type="button" title="'.$button_title.'" class="btn btn-'.$button_class.' btn-xs change-email-status" data-status="'.$b['status'].'" data-id="'.$b['id'].'">'.$button_text.'</button>',
                $actions
            );
        }
        return $sorted;
    }
}