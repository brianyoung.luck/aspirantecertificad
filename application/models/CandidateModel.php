<?php
require_once FCPATH . 'vendor/sendgrid/sendgrid/sendgrid-php.php';
class CandidateModel extends CI_Model
{
    protected $table = 'candidates';
    protected $key = 'candidate_id';

    public function getFirst($column, $value)
    {
        $this->db->where($column, $value);
        $this->db->select('candidates.*');
        $this->db->from($this->table);
        $result = $this->db->get();
        return ($result->num_rows() == 1) ? objToArr($result->row(0)) : array();
    }

    public function candidatesList()
    {
        $columns = array(
            "",
            "",
            "candidates.first_name",
            "candidates.last_name",
            "candidates.email",
            "",
            "resumes.experience",
            "candidates.account_type",
            "candidates.created_at",
            "candidates.status",
        );

        $this->db->from('candidates');
        $this->db->select('
            candidates.*,
            resumes.experience,
            GROUP_CONCAT('.CF_DB_PREFIX.'resume_experiences.title) as job_title
        ');
        
        $this->db->join('resumes','resumes.candidate_id = candidates.candidate_id AND resumes.is_default = 1', 'left');
        $this->db->join('resume_experiences','resume_experiences.resume_id = resumes.resume_id', 'left');
        $this->db->group_by('candidates.candidate_id');
        $this->db->order_by('resumes.experience', 'DESC');
        $this->db->limit(3, 1);
        $query = $this->db->get();
        return objToArr($query->result());
    }

    public function valueExist($field, $value, $edit = false)
    {
        $this->db->where($field, $value);
        if ($edit) {
            $this->db->where('candidate_id !=', $edit);
        }
        $query = $this->db->get('candidates');
        return $query->num_rows() > 0 ? true : false;
    }

    public function login($email, $password)
    {
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $this->db->where('status', 1);
        $result = $this->db->get('candidates');
        return ($result->num_rows() == 1) ? $result->row(0) : false;
    }
    
    public function sendmailboth($candidate_id)
    {
        $this->db->where('job_applications.candidate_id', $candidate_id);
        $this->db->select('*');
        $this->db->from('job_applications');
        $result = $this->db->get();
        
        $job_application = ($result->num_rows() == 1) ? objToArr($result->row(0)) : array();
        $job_application_id=$job_application["job_id"];
        
        $this->db->where('jobs.job_id', $job_application_id);
        $this->db->select('*');
        $this->db->from('jobs');
        $result = $this->db->get();
        
        $job = ($result->num_rows() == 1) ? objToArr($result->row(0)) : array();
        $job_title=$job["title"];
        
        $this->db->where('candidates.candidate_id', $candidate_id);
        $this->db->select('*');
        $this->db->from('candidates');
        $result = $this->db->get();
        
        $candidate = ($result->num_rows() == 1) ? objToArr($result->row(0)) : array();
        
        
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom('notificaciones@aspirantecertificadomkt.com', 'Notificaciones');
        $email->setSubject('Documentos recibidos - '.$candidate["first_name"]." ".$candidate["last_name"].' – ('.$job_title.')');
        $email->addTo($candidate['email'], 'Candidate');
        $email->addContent("text/plain", "subject");
        $email->addContent(
            "text/html",'<body ><div style="width:500px; margin:10px auto; background:#f1f1f1; border:1px solid #ccc">
                <table  width="100%" border="0" cellspacing="5" cellpadding="10">
                    <tr>
                        <td style="font-size:22px; text-align:center"> Hola '.$candidate["first_name"]." ".$candidate["last_name"].'</td>
                    </tr>
                    <tr>
                        <td style="font-size:14px; color:#323232"> Gracias por enviar tus documentos, en breve te estaremos avisando el progreso de tu contratación </td>
                    </tr>
                    <tr>
                        <td style="font-size:14px; color:#323232">
                            Gracias<br/>
                            www.aspirantecertificado.com
                        </td>
                    </tr>
                </table></div></body>');
    
        $sendgrid = new \SendGrid(('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'));
       
        try {
            $response = $sendgrid->send($email);
            if($response->statusCode()==202){$k=0;}else{$k=1;}
        } catch (Exception $e) {
            $k=$e;
        }
        
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom('notificaciones@aspirantecertificadomkt.com', 'Notificaciones');
        $email->setSubject('Documentos recibidos - '.$candidate["first_name"]." ".$candidate["last_name"].' – ('.$job_title.')');
        $email->addTo('agentes@aspirantecertificadomkt.com', 'Agentes');
        $email->addContent("text/plain", "subject");
        $email->addContent(
            "text/html",'<body ><div style="width:500px; margin:10px auto; background:#f1f1f1; border:1px solid #ccc">
                <table  width="100%" border="0" cellspacing="5" cellpadding="10">
                    <tr>
                        <td style="font-size:22px; text-align:center"> Hola, '.$candidate["first_name"]." ".$candidate["last_name"].' documentos recibidos</td>
                    </tr>
                    <tr>
                        <td style="font-size:14px; color:#323232"> Gracias por enviar tus documentos, en breve te estaremos avisando el progreso de tu contratación </td>
                    </tr>
                    <tr>
                        <td style="font-size:14px; color:#323232">
                            Gracias<br/>
                            www.aspirantecertificado.com
                        </td>
                    </tr>
                </table></div></body>');
    
        $sendgrid = new \SendGrid(('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'));
       
        try {
            $response = $sendgrid->send($email);
            if($response->statusCode()==202){$k=0;}else{$k=1;}
        } catch (Exception $e) {
            $k=$e;
        }
        
        
        
    }

    public function createTokenForCandidate($email)
    {
        $this->db->where('email', $email);
        $this->db->update('candidates', array('token' => token()));
    }

    public function createCandidate($verification = false)
    {
        $data = $this->xssCleanInput();
        if ($verification) {
            $token = token();
            $data['token'] = $token;
            $data['status'] = 0;
        } else {
            $data['token'] = '';
            $data['status'] = 1;
        }
        if (isset($data["job_id"])) {
            $jobId = $data["job_id"];
            unset($data["job_id"]);
        }
        unset($data['retype_password']);
        $data['image'] = '';
        $data['password'] = makePassword($data['password']);
        $data['account_type'] = 'site';
        $data['external_id'] = '';
        $data['created_at'] = date('Y-m-d G:i:s');
        $this->db->insert('candidates', $data);
        $id = $this->db->insert_id();
        if (isset($jobId)) {
            $this->db->insert('job_applications', ["job_id" => $jobId, "candidate_id" => $id]);
        }
        
        return $this->getFirst('candidates.candidate_id', $id);
    }

    public function updateProfile($image)
    {
        $data = $this->xssCleanInput();
        $data['updated_at'] = date('Y-m-d G:i:s');
        if (isset($image['file'])) {
            $data['image'] = $image['file'];
        }
        $this->db->where('candidate_id', candidateSession());
        return $this->db->update('candidates', $data);
    }
    public function updateProfile1($files)
    {
        $data = $this->xssCleanInput();
        $data['updated_at'] = date('Y-m-d G:i:s');
        if (isset($files['file'])) {
            $data['paddress'] = 'https://www.aspirantecertificado.com/assets/images/candidates/paddress_identification/'.$files['file'];
        }
        $this->db->where('candidate_id', candidateSession());
        return $this->db->update('candidates', $data);
    }
    public function updateProfile2($files)
    {
        $data = $this->xssCleanInput();
        $data['updated_at'] = date('Y-m-d G:i:s');
        if (isset($files['file'])) {
            $data['identification'] =  'https://www.aspirantecertificado.com/assets/images/candidates/paddress_identification/'.$files['file'];
        }
        $this->db->where('candidate_id', candidateSession());
        return $this->db->update('candidates', $data);
    }

    public function activateAccount($token)
    {
        $result = $this->getFirst('candidates.token', $token);
        if ($result) {
            $this->db->where('token', $token);
            $this->db->update(
                'candidates',
                array('status' => 1, 'token' => '', 'updated_at' => date('Y-m-d G:i:s'))
            );
            return true;
        } else {
            return false;
        }
    }

    public function updatePasswordByField($field, $value, $password)
    {
        $this->db->where($field, $value);
        $this->db->update('candidates', array('password' => $password, 'token' => ''));
        $this->session->set_userdata('password', $password);
        return true;
    }

    public function internalCandidate($email, $type)
    {
        $this->db->where('candidates.email', $email);
        $this->db->where('candidates.account_type != ', $type);
        $this->db->select('candidates.*');
        $this->db->from($this->table);
        $result = $this->db->get();
        return ($result->num_rows() == 1) ? true : false;
    }

    public function existingExternalCandidate($id, $email)
    {
        $this->db->where('candidates.email', $email);
        $this->db->where('candidates.external_id', $id);
        $this->db->select('candidates.*');
        $this->db->from($this->table);
        $result = $this->db->get();
        return ($result->num_rows() == 1) ? objToArr($result->row(0)) : array();
    }

    public function createGoogleCandidateIfNotExist($id, $email, $name, $image)
    {
        if ($this->internalCandidate($email, 'google')) {
            return false;
        } elseif ($this->existingExternalCandidate($id, $email)) {
            return $this->existingExternalCandidate($id, $email);
        } else {
            $this->insertCandidateImage($image, $id);
            $name = explode(' ', $name);
            $data['first_name'] = $name[0];
            $data['last_name'] = $name[1];
            $data['email'] = $name[0].$name[1];
            $data['email'] = $email;
            // $data['image'] = $id.'.jpg';
            $data['password'] = makePassword($name[0].$name[1].$email);
            $data['status'] = 1;
            $data['account_type'] = 'google';
            $data['external_id'] = $id;
            $data['created_at'] = date('Y-m-d G:i:s');
            $this->db->insert('candidates', $data);
            return $this->existingExternalCandidate($id, $email);
        }
    }

    public function createLinkedinCandidateIfNotExist($apiData)
    {
        $id = $apiData['id'];
        $email = $apiData['email'];
        $first_name = $apiData['first_name'];
        $last_name = $apiData['last_name'];
        $image = $apiData['image'];
        if ($this->internalCandidate($email, 'linkedin')) {
            return false;
        } elseif ($this->existingExternalCandidate($id, $email)) {
            return $this->existingExternalCandidate($id, $email);
        } else {
            // $this->insertCandidateImage($image, $id);
            $data['first_name'] = $first_name;
            $data['last_name'] = $last_name;
            $data['email'] = $first_name.$last_name;
            $data['email'] = $email;
            $data['image'] = $id.'.jpg';
            $data['password'] = makePassword($first_name.$last_name.$email);
            $data['status'] = 1;
            $data['account_type'] = 'linkedin';
            $data['external_id'] = $id;
            $data['created_at'] = date('Y-m-d G:i:s');
            $this->db->insert('candidates', $data);
            return $this->existingExternalCandidate($id, $email);
        }
    }

    private function insertCandidateImage($image, $id)
    {
        $name = $id.'.jpg';
        $full_path = ASSET_ROOT.'/images/candidates/'.$name;
        $content = file_get_contents($image);
        $fp = fopen($full_path, "w");
        fwrite($fp, $content);
        fclose($fp);
        $controllerInstance = & get_instance();
        $controllerInstance->resizeByWidthAndCropByHeight(ASSET_ROOT.'/images/candidates/', $id, 'jpg', 60, 60);
        $controllerInstance->resizeByWidthAndCropByHeight(ASSET_ROOT.'/images/candidates/', $id, 'jpg', 120, 120);
    }

    public function storeAdminCandidate()
    {
        $data = $this->xssCleanInput();
        $data['password'] = makePassword($this->xssCleanInput('password'));
        $data['created_at'] = date('Y-m-d G:i:s');
        $data['candidate_type'] = 'admin';
        $data['candidate_level'] = 'admin';
        unset($data['retype_password']);
        return $this->db->insert('candidates', $data);
    }

    public function storeRememberMeToken($email, $token)
    {
        $this->db->where('email', $email);
        $this->db->update('candidates', array('token' => $token));
    }

    public function getCandidateWithRememberMeToken($token)
    {
        $this->db->where('candidates.token', $token);
        $this->db->select('candidates.*');
        $this->db->from($this->table);
        $result = $this->db->get();
        return ($result->num_rows() == 1) ? objToArr($result->row(0)) : array();
    }
}