  <!-- Content Wrapper Starts -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><i class="fas fa-envelope"></i> <?php echo lang('email_template'); ?></h1>
    </section>

    <!-- Main content -->
  <section class="content">
    
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <div class="row">
              <div class="col-md-12">

              	<a class="btn btn-primary btn-blue btn-flat create-or-edit-email" href="<?php echo 'email-templates/create-or-edit';?>"> <i class="fa fa-plus"></i> New Template</a>                            
                
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">            
          <table class="table table-bordered table-striped" id="emailtemplate_datatable">
              <thead>
              <tr>
				<th><input type="checkbox" class="minimal all-check"></th>
                <th><?php echo lang('Name'); ?></th>
               
                <th><?php echo lang('user_access'); ?></th>
                <th><?php echo lang('created_on'); ?></th>
                <th><?php echo lang('status'); ?></th>
                <th><?php echo lang('actions'); ?></th>
              </tr>
              </thead>
              <tbody>
			  <!--
				<?php foreach($templates as $template):?>
				<tr>
					<td><?php echo $template['name'];?></td>
					<td><?php echo $template['template'];?></td>
					<td><?php echo $template['user_access']==1?'Yes':'NO';?></td>
					<td><?php echo $template['created_at'];?></td>
					<td><?php echo $template['status']==1?'Active':'Inactive';?></td>
					<td>
						<a href="<?php echo base_url()?>admin/email-templates/create-or-edit/<?php echo $template['id']?>"><button type="button" class="btn btn-primary btn-xs create-or-edit-blog" data-id="<?php echo $template['id'];?>"><i class="far fa-edit"></i></button></a>
            
						<a href="<?php echo base_url()?>admin/email-templates/delete/<?php echo $template['id']?>"><button type="button" class="btn btn-danger btn-xs delete-template" data-id="<?php echo $template['id'];?>"><i class="far fa-trash-alt"></i></button></a>
					</td>
				</tr>
				<?php endforeach;?>
				-->
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

</div>

<?php include(VIEW_ROOT.'/admin/layout/footer.php'); ?>

<!-- page script -->
<script src="<?php echo base_url(); ?>assets/admin/js/cf/email_template.js"></script>

</body>
</html>