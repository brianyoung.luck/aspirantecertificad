

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><i class="fa fa-briefcase"></i> <?php echo lang('email_template'); ?><small></small></h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>admin/dashboard"><i class="fas fa-tachometer-alt"></i> <?php echo lang('home'); ?></a></li>
      <li class="active"><i class="fa fa-briefcase"></i> <?php echo lang('email_template'); ?></li>
      <li class="active"><?php echo lang('create'); ?></li>
    </ol>
  </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Email Template</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form"  id="admin_email_template_create_update_form" >
				<?php if(isset($template) && !empty($template)):?>
				<input type="hidden" name="id" value="<?php echo $template['id']?>">
				<input type="hidden" id = "admin_email_template_create_update_form_csrf_token" name="csrf_token" value="">
				<?php endif;?>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-sm-12">
                    <div class="form-group">
                      <label><?php echo lang('Name'); ?></label>
                      <input type="text" class="form-control" placeholder="Enter Name" name="name" value="<?php echo $template['name']?$template['name']:''?>" required>
                    </div>
					<div class="form-group">
                      <label><?php echo lang('status'); ?></label>
                      <select class="form-control change-email-status" name="status">
                        <option value="1" <?php sel($template['status'], 1); ?>><?php echo lang('active'); ?></option>
                        <option value="0" <?php sel($template['status'], 0); ?>><?php echo lang('inactive'); ?></option>
                      </select>
                    </div>
					<div class="form-group">
                      <label><?php echo lang('user_access'); ?></label>
                      <select class="form-control change-user-access-status" name="user_access">
                        <option value="1" <?php sel($template['user_access'], 1); ?>><?php echo lang('active'); ?></option>
                        <option value="0" <?php sel($template['user_access'], 0); ?>><?php echo lang('inactive'); ?></option>
                      </select>
                    </div>
                  </div>                  
                  
                  <div class="col-md-12">
                    <div class="form-group">
                      <label><?php echo lang('Template'); ?></label>
                      <textarea id="template" name="template" rows="10" cols="80" required><?php echo esc_output($template['template'], 'raw'); ?></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary" id="admin_blog_create_update_form_button"><?php echo lang('save'); ?></button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php include(VIEW_ROOT.'/admin/layout/footer.php'); ?>

<!-- page script -->
<script src="<?php echo base_url(); ?>assets/admin/js/cf/email_template.js"></script>
<script>
 var getFormData = function(formId) {
       let formData = {};
       let inputs = $('#'+formId).serializeArray();
       $.each(inputs, function (i, input) {
          formData[input.name] = input.value;
      });
      return formData;
   };
$('#admin_email_template_create_update_form').on('submit',function(e){
	e.preventDefault();
		
		for(var instanceName in CKEDITOR.instances)
                CKEDITOR.instances[instanceName].updateElement();
		var fData = getFormData('admin_email_template_create_update_form');
		
		$.ajax({
			 url: "<?=base_url();?>admin/email-templates/save",
			 method:'POST',
			data: fData,
			//processData: false,
			//contentType: false,
		}).done(function(response){
				var result = JSON.parse(response);
                if (result.success === 'true') {
                } else {
                }
                //application.hideLoader('admin_blog_create_update_form_button');
                application.showMessages(result.messages, 'admin_email_template_create_update_form');
		});
		//return false;
	
});
	function test(e){
		
	}
	$(document).ready(function(){
		$("#admin_email_template_create_update_form_csrf_token").val(document.head.querySelector("[property~=token][content]").content);
		
	});
</script>
</body>
</html>

