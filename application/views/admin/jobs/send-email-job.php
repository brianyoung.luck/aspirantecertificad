<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><i class="fa fa-briefcase"></i> Send email for invitation<small></small></h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>admin/dashboard"><i class="fas fa-tachometer-alt"></i> <?php echo lang('home'); ?></a></li>
      <li class="active"><i class="fa fa-briefcase"></i> <?php echo lang('job'); ?></li>
      <li class="active"><?php echo lang('create'); ?></li>
    </ol>
  </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <!-- form start -->
            <form role="form" id="admin_job_send_email_form">
              <div class="box-body">
                  <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                          <input type="hidden" name="job_id" value="<?php echo esc_output($job['job_id']); ?>" />
                        </div>
                    </div>
                    
                    <div class="col-md-7 col-sm-7">
                      <div class="form-group">
                        <label>
                          <?php echo lang('label_user_field'); ?>
                        </label>
                        <select class="form-control selectpicker" id="users" name="user_id[]" multiple data-live-search="true" style="height: 250px !important;">
                          <option value=""><?php echo lang('none'); ?></option>
                          <?php foreach ($users as $user) { ?>
                            <option value="<?php echo esc_output($user['user_id']); ?>"><?php echo esc_output($user['username'], 'html'); ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="input-group" style="width: 255px;">
                            To Email<input type="text" name="email" id="email" class="form-control" placeholder="example@demo.com" aria-label="Email" aria-describedby="basic-addon1" style="width: 220px;">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope" style=" font-size: 22px; padding: 5px;border: solid;border-width: 1px;border-color: #CCC;"></i></span>
                            </div>
                        </div>
                    </div>
                    
                  </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary" id="send_email_job_update_form_button">
                  <?php echo lang('send'); ?>
                </button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php include(VIEW_ROOT.'/admin/layout/footer.php'); ?>
<script src="<?php echo base_url(); ?>assets/admin/js/cf/send_email_job.js"></script>
<!-- page script -->

</body>
</html>

