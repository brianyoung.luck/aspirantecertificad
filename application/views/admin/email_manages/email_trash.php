<!-- Content Wrapper Starts -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><i class="fas fa-envelope"></i> <?php echo lang('email_trash'); ?></h1>
    </section>

    <!-- Main content -->
  <section class="content">
    
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <div class="row">
              <div class="col-md-12">

              	                      
                
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">  
                <div class="col-md-9 col-lg-9 col-sm-12">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <section class="edit-resume-section" id="process-tab">
                                <div class="col-xs-12">
                                    <!-- Nav tabs -->
                                    <div class="edit-resume-content">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12 col-sm-12">
                                                <div class="account-box">
                                                    <p class="account-box-heading">
                                                        <span class="account-box-heading-text"><?php echo lang('email_trash'); ?></span>
                                                        <span class="account-box-heading-line"></span>
                                                    </p>
                                                    <div class="container">
                                                        <!--mail inbox start-->
                                                        <div class="mail-box">
                                                            <aside class="lg-side">
                                                                <div class="inbox-head">
                                                                    <form class="pull-right position" action="#">
                                                                        <div class="input-append">
                                                                            <input type="text" placeholder="Search Mail" class="sr-input">
                                                                            <button type="button" class="btn sr-btn" data-original-title="" title=""><i class="fa fa-search"></i></button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                                <div class="inbox-body">
                                                                    <div class="mail-option d-flex justify-content-between align-items-center">
                                                                        <div class="chk-all">
                                                                            <input type="checkbox" class="mail-checkbox mail-group-checkbox">
                                                                            <div class="btn-group">
                                                                                <a class="btn mini all" href="#" data-toggle="dropdown" data-original-title="" title="">
                                                                                    All
                                                                                    <i class="fa fa-angle-down "></i>
                                                                                </a>
                                                                                <ul class="dropdown-menu">
                                                                                    <li><a href="#"> None</a></li>
                                                                                    <li><a href="#"> Read</a></li>
                                                                                    <li><a href="#"> Unread</a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="btn-group">
                                                                            <a class="btn mini tooltips" href="#" data-toggle="dropdown" data-placement="top" data-original-title="Refresh">
                                                                                <i class=" fa fa-refresh"></i>
                                                                            </a>
                                                                        </div>
                                                                        <div class="btn-group hidden-phone">
                                                                            <a class="btn mini blue" href="#" data-toggle="dropdown" data-original-title="" title="">
                                                                                More
                                                                                <i class="fa fa-angle-down "></i>
                                                                            </a>
                                                                            <ul class="dropdown-menu">
                                                                                <li><a href="#"><i class="fa fa-pencil"></i> Mark as Read</a></li>
                                                                                <li><a href="#"><i class="fa fa-ban"></i> Spam</a></li>
                                                                                <li class="divider"></li>
                                                                                <li><a href="#"><i class="fa fa-trash"></i> Delete</a></li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="btn-group">
                                                                            <a class="btn mini blue" href="#" data-toggle="dropdown" data-original-title="" title="">
                                                                                Move to
                                                                                <i class="fa fa-angle-down "></i>
                                                                            </a>
                                                                            <ul class="dropdown-menu">
                                                                                <li><a href="#"><i class="fa fa-pencil"></i> Mark as Read</a></li>
                                                                                <li><a href="#"><i class="fa fa-ban"></i> Spam</a></li>
                                                                                <li class="divider"></li>
                                                                                <li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                                                            </ul>
                                                                        </div>
                                                                        <ul class="unstyled inbox-pagination">
                                                                            <li><span>1-50 of 234</span></li>
                                                                            <li>
                                                                                <a href="#" class="np-btn"><i class="fa fa-angle-left  pagination-left"></i></a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" class="np-btn"><i class="fa fa-angle-right pagination-right"></i></a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <table class="table table-inbox table-hover">
                                                                        <tbody>
                                                                        <tr class="unread">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                                                            <td class="view-message  dont-show">Arjuna Admin</td>
                                                                            <td class="view-message ">Lorem ipsum dolor imit set.</td>
                                                                            <td class="view-message  inbox-small-cells"><i class="fa fa-paperclip"></i></td>
                                                                            <td class="view-message  text-right">9:27 AM</td>
                                                                        </tr>
                                                                        <tr class="unread">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                                                            <td class="view-message dont-show">John Doe</td>
                                                                            <td class="view-message">Hi Bro, How are you?</td>
                                                                            <td class="view-message inbox-small-cells"></td>
                                                                            <td class="view-message text-right">March 15</td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                                                            <td class="view-message dont-show">Jabuk Doe</td>
                                                                            <td class="view-message">Lorem ipsum dolor sit amet</td>
                                                                            <td class="view-message inbox-small-cells"></td>
                                                                            <td class="view-message text-right">June 15</td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                                                            <td class="view-message dont-show">Facebook</td>
                                                                            <td class="view-message">Dolor sit amet, consectetuer adipiscing</td>
                                                                            <td class="view-message inbox-small-cells"></td>
                                                                            <td class="view-message text-right">April 01</td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star inbox-started"></i></td>
                                                                            <td class="view-message dont-show">Cendy Andrianto</td>
                                                                            <td class="view-message">Lorem ipsum dolor sit amet</td>
                                                                            <td class="view-message inbox-small-cells"></td>
                                                                            <td class="view-message text-right">May 23</td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star inbox-started"></i></td>
                                                                            <td class="view-message dont-show">Facebook</td>
                                                                            <td class="view-message">Dolor sit amet, consectetuer adipiscing</td>
                                                                            <td class="view-message inbox-small-cells"><i class="fa fa-paperclip"></i></td>
                                                                            <td class="view-message text-right">March 14</td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star inbox-started"></i></td>
                                                                            <td class="view-message dont-show">Rosid nur</td>
                                                                            <td class="view-message">Lorem ipsum dolor sit amet</td>
                                                                            <td class="view-message inbox-small-cells"><i class="fa fa-paperclip"></i></td>
                                                                            <td class="view-message text-right">January 19</td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                                                            <td class="view-message dont-show">Facebook</td>
                                                                            <td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td>
                                                                            <td class="view-message inbox-small-cells"></td>
                                                                            <td class="view-message text-right">March 04</td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                                                            <td class="view-message dont-show">Cendy Andrianto</td>
                                                                            <td class="view-message view-message">Lorem ipsum dolor sit amet</td>
                                                                            <td class="view-message inbox-small-cells"></td>
                                                                            <td class="view-message text-right">June 13</td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                                                            <td class="view-message dont-show">Facebook </td>
                                                                            <td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td>
                                                                            <td class="view-message inbox-small-cells"></td>
                                                                            <td class="view-message text-right">March 24</td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star inbox-started"></i></td>
                                                                            <td class="view-message dont-show">Fikriandro</td>
                                                                            <td class="view-message">Lorem ipsum dolor sit amet</td>
                                                                            <td class="view-message inbox-small-cells"></td>
                                                                            <td class="view-message text-right">March 09</td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star inbox-started"></i></td>
                                                                            <td class="dont-show">Facebook</td>
                                                                            <td class="view-message">Dolor sit amet, consectetuer adipiscing</td>
                                                                            <td class="view-message inbox-small-cells"><i class="fa fa-paperclip"></i></td>
                                                                            <td class="view-message text-right">May 14</td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                                                            <td class="view-message dont-show">Badrasianto</td>
                                                                            <td class="view-message">Lorem ipsum dolor sit amet</td>
                                                                            <td class="view-message inbox-small-cells"><i class="fa fa-paperclip"></i></td>
                                                                            <td class="view-message text-right">February 25</td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                                                            <td class="dont-show">Facebook</td>
                                                                            <td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td>
                                                                            <td class="view-message inbox-small-cells"></td>
                                                                            <td class="view-message text-right">March 14</td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                                                            <td class="view-message dont-show">Daemon emon</td>
                                                                            <td class="view-message">Lorem ipsum dolor sit amet</td>
                                                                            <td class="view-message inbox-small-cells"></td>
                                                                            <td class="view-message text-right">April 07</td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                                                            <td class="view-message dont-show">Twitter</td>
                                                                            <td class="view-message">Dolor sit amet, consectetuer adipiscing</td>
                                                                            <td class="view-message inbox-small-cells"></td>
                                                                            <td class="view-message text-right">July 14</td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star inbox-started"></i></td>
                                                                            <td class="view-message dont-show">Sumomonosuke</td>
                                                                            <td class="view-message">Lorem ipsum dolor sit amet</td>
                                                                            <td class="view-message inbox-small-cells"></td>
                                                                            <td class="view-message text-right">August 10</td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                                                            <td class="view-message dont-show">Facebook</td>
                                                                            <td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td>
                                                                            <td class="view-message inbox-small-cells"><i class="fa fa-paperclip"></i></td>
                                                                            <td class="view-message text-right">April 14</td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                                                            <td class="view-message dont-show">Daemon nikiwa</td>
                                                                            <td class="view-message">Lorem ipsum dolor sit amet</td>
                                                                            <td class="view-message inbox-small-cells"><i class="fa fa-paperclip"></i></td>
                                                                            <td class="view-message text-right">June 16</td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star inbox-started"></i></td>
                                                                            <td class="view-message dont-show">Sumomonosuke</td>
                                                                            <td class="view-message">Lorem ipsum dolor sit amet</td>
                                                                            <td class="view-message inbox-small-cells"></td>
                                                                            <td class="view-message text-right">August 10</td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td class="inbox-small-cells">
                                                                                <input type="checkbox" class="mail-checkbox">
                                                                            </td>
                                                                            <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                                                            <td class="view-message dont-show">Facebook</td>
                                                                            <td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td>
                                                                            <td class="view-message inbox-small-cells"><i class="fa fa-paperclip"></i></td>
                                                                            <td class="view-message text-right">April 14</td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </aside>
                                                        </div>
                                                        <!--mail inbox end-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>

            
            </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->



<?php include(VIEW_ROOT.'/admin/layout/footer.php'); ?>