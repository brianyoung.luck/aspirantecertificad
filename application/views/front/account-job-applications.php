  <!--==========================
    Intro Section
  ============================-->
  <section id="intro" class="clearfix front-intro-section">
    <div class="container">
      <div class="intro-img">
      </div>
      <div class="intro-info">
        <h2><span><?php echo lang('account'); ?> > <?php echo lang('job_applications'); ?></span></h2>
      </div>
    </div>
  </section><!-- #intro -->

  <main id="main">
    
    
    <!--==========================
      Account Area Setion
    ============================-->
    <section id="about">
      <div class="container">
        
        <div class="row mt-10">
          <div class="col-lg-3">
            <div class="account-area-left">
              <ul>
                <?php include(VIEW_ROOT.'/front/partials/account-sidebar.php'); ?>
              </ul>
            </div>
          </div>
          <div class="col-md-12 col-lg-9 col-sm-12">
            <?php
            $this->db->where('candidates.candidate_id', candidateSession());
            $this->db->select('*');
            $this->db->from('candidates');
            $result = $this->db->get();
            $candidateViewData = ($result->num_rows() == 1) ? objToArr($result->row(0)) : array();
            if ($jobs) { ?>
            <?php foreach ($jobs as $job) { ?>
            <div class="row">
              <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="job-listing">
                  <p class="job-listing-heading">
                    <span class="job-listing-heading-text">
                      <a href="<?php echo base_url(); ?>job/<?php echo encode($job['job_id']); ?>"><?php echo esc_output($job['title'], 'html'); ?></a>
                    </span>
                    <span class="job-listing-heading-line"></span>
                  </p>
                  <div class="job-listing-job-info" style="display: flow-root;">
                    <span class="job-listing-job-info-date" style="margin-top: 3px;"><i class="fa fa-clock-o"></i> <?php echo lang('applied_on'); ?> : <?php echo timeFormat($job['applied_on']); ?></span>
                    <span class="job-listing-job-info-date" style="margin-top: 3px;"><i class="fa fa-bookmark"></i> <?php echo esc_output($job['department'], 'html'); ?></span>
                    <?php if (($job['job_status']=="shortlisted") || ($job['job_status']=="interviewed") || ($job['job_status']=="hired") || ($job['job_status']=="rejected")){?>
                      <span class="job-listing-job-info-date upload_document"  title="Documents Upload" data-id="<?php echo encode($job['job_id']); ?>" style="margin-top: 3px; cursor:pointer; font-size: 18px; background: red; border: none;  padding: 0px;  padding-left: 10px; padding-right: 10px;"><i class="fa fa-book"></i> Subir documentos</span>
                      <input type="hidden" class="existDoc" value="1">
                    <?php }else{
                        echo '<input type="hidden" class="existDoc" value="0">';
                    }?>
                    
                    <!--<span class="job-listing-job-info-date refer-job"  style="margin-top: 3px;" title="Refer this job" data-id="<?php echo encode($job['job_id']); ?>"><i class="fa fa-user-plus"></i></span>-->
                    <?php if (isset($candidateViewData["video_interview"]) && $candidateViewData["video_interview"]!=""){?>
                    <span class="job-listing-job-info-date video_view video"  data-toggle="modal" data-target="#videoModal" style="margin-top: 3px; cursor:pointer; font-size: 18px; background: #f39c12; border: none;  padding: 0px;  padding-left: 10px; padding-right: 10px;" title="Video View" data-id="<?php echo encode($job['job_id']); ?>"><i class="fa fa-film"> Ver</i></span>
                    <!--<a class="btn btn-warning btn-xs video" data-video="" data-toggle="modal" data-target="#videoModal"><i class="fa fa-video"></i> <?php echo "Interviewed Video"; ?></a>-->
                    <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body" style="margin: auto;">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <video controls style="width: 100%;">
                                        <source src="<?php echo $candidateViewData['video_interview']?>" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <?php } ?>
                    <?php 
                        if(($job['job_status']!="shortlisted") && ($job['job_status']!="interviewed")){
                    ?>
                    <span class="job-listing-job-info-date record_video" style="margin-top: 3px; font-size: 18px; background: red; border: none;  padding: 0px;  padding-left: 10px; padding-right: 10px;" title="Record Video" data-id="<?php echo encode($job['job_id']); ?>"><i class="fa fa-video"></i> Grabar Video</span>
                    <?php
                        }
                    ?>
                  </div>
                  <div class="job-listing-job-description">
                    <?php echo trimString($job['description'], 280); ?>
                    <a href="<?php echo base_url(); ?>job/<?php echo encode($job['job_id']); ?>"><?php echo lang('read_more'); ?></a>
                  </div>
                  <div class="container">
                    <div class="row job-listing-items-container">
                      <?php if ($job['fields']) { ?>
                        <?php foreach ($job['fields'] as $key => $value) { ?>
                          <?php if ($value['label']) { ?>
                          <div class="col-md-4 col-sm-6 job-listing-items">
                            <span class="job-listing-items-title" title="<?php echo esc_output($value['label'], 'html'); ?>">
                              <?php echo trimString(esc_output($value['label'], 'html')); ?>
                            </span>
                            <span class="job-listing-items-value" title="<?php echo esc_output($value['value'], 'html'); ?>">
                              <?php echo trimString(esc_output($value['value'], 'html')); ?>
                            </span>
                          </div>                          
                          <?php } ?>
                        <?php } ?>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="container">
                    <div class="row job-application-progress">
                        <div class="col-xs-3 job-application-progress-step <?php jobStatus($job['job_status'], 1); ?>">
                          <div class="text-center job-application-progress-stepnum"><?php echo lang('applied'); ?></div>
                          <div class="progress"><div class="progress-bar"></div></div>
                          <a href="#" class="job-application-progress-dot"></a>
                        </div>
                        <div class="col-xs-3 job-application-progress-step <?php jobStatus($job['job_status'], 2); ?>">
                          <div class="text-center job-application-progress-stepnum"><?php echo lang('shortlisted'); ?></div>
                          <div class="progress"><div class="progress-bar"></div></div>
                          <a href="#" class="job-application-progress-dot"></a>
                        </div>
                        <div class="col-xs-3 job-application-progress-step <?php jobStatus($job['job_status'], 3); ?>">
                          <div class="text-center job-application-progress-stepnum"><?php echo lang('interviewed'); ?></div>
                          <div class="progress"><div class="progress-bar"></div></div>
                          <a href="#" class="job-application-progress-dot"></a>
                        </div>
                        <div class="col-xs-3 job-application-progress-step <?php jobStatus($job['job_status'], 4); ?>">
                          <div class="text-center job-application-progress-stepnum"><?php echo lang('hired'); ?></div>
                          <div class="progress"><div class="progress-bar"></div></div>
                          <a href="#" class="job-application-progress-dot"></a>
                        </div>
                    </div>
                    
                  </div>                  
                </div>
              </div>
            </div>
            <?php } ?>
            <?php } else { ?>
            <div class="row">
              <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="job-detail account-no-content-box">
                  <?php echo lang('no_jobs_found'); ?>
                </div>
              </div>
            </div>
            <?php } ?>
            <div class="row">
              <div class="col-md-12 col-lg-12 col-sm-12">
                <?php echo esc_output($pagination, 'raw'); ?>
              </div>
            </div>             
          </div>
        </div>

      </div>

    </section><!-- #account area section ends -->

  </main>
  
  <div class="modal fade" id="jobpagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="max-width:700px;">
      <div class="modal-content">
        <div class="modal-body">
            <br/>
            Instrucciones<br/>
            <br/>
            ⦁	Parte de tu proceso de reclutamiento es un video presentación  cómo 5 min máximo.<br/>
            ⦁	Si tuviste problemas técnicos.. reportalo en por la ventan ade whatsApp.<br/>
            ⦁	Explica quién eres y porque quieres este empleo.<br/>
            ⦁	Después de grabar el video, te daremos el seguimiento para tu próximo empleo<br/>
        </div>
        <button type="button" class="btn btn-primary" data-dismiss="modal" style="width: 150px; margin: auto; margin-bottom: 10px;">OK</button>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  
  <div class="modal fade" id="jobpageDocmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
            <br/>
            Instrucciones<br/>
            <br/>
            Da clic en Subir documentos para seguir con el proceso<br/>
        </div>
        <button type="button" class="btn btn-primary" data-dismiss="modal" style="width: 150px; margin: auto; margin-bottom: 10px;">OK</button>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  
  <div class="modal fade" id="jobpageDocmodal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
            <br/>
            Gracias,<br/>
            <br/>
            en breve nos comunicaremos para el proceso de tu contratación<br/>
        </div>
        <button type="button" class="btn btn-primary" data-dismiss="modal" style="width: 150px; margin: auto; margin-bottom: 10px;">OK</button>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  
  
  <style>
    #jobpagemodal{
        top:20% !important;
        outline: none;
    }
    #jobpageDocmodal{
        top:20% !important;
        outline: none;
    }
    /*#resume-history, #resume-history, #resume-qualification, #resume-language, #resume-achievement, #resume-references{*/
    /*    display:none;*/
    /*}*/
  </style>
  <script>
      $(document).ready(function(){
          if($(".existDoc").val()!="1"){
            setTimeout(function() {
                $('#jobpagemodal').modal('show');
            }, 1000); 
          }else{
            setTimeout(function() {
                $('#jobpageDocmodal').modal('show');
            }, 1000); 
          }
      });
  </script>
  <script type="text/javascript">
  
    // $( document ).ready(function() {
    //     setTimeout(function() {
    //         $('#jobpagemodal').modal('show');
    //     }, 1000); 
    // });
  
</script>
    
  <?php include(VIEW_ROOT.'/front/layout/footer.php'); ?>
  
  