<style type="text/css">
  body {
    padding: 0;
    margin: 0;
  }

  svg:not(:root) {
    display: block;
  }

  .playable-code {
    background-color: #f4f7f8;
    border: none;
    border-left: 6px solid #558abb;
    border-width: medium medium medium 6px;
    color: #4d4e53;
    height: 100px;
    width: 90%;
    padding: 10px 10px 0;
  }

  .playable-canvas {
    border: 1px solid #4d4e53;
    border-radius: 2px;
  }

  .playable-buttons {
    text-align: right;
    width: 90%;
    padding: 5px 10px 5px 26px;
  }

  video {
    margin-top: 2px;
    border: 1px solid black;
  }

  .button {
    cursor: pointer;
    display: block;
    width: 160px;
    border: 1px solid black;
    font-size: 16px;
    text-align: center;
    padding-top: 2px;
    padding-bottom: 4px;
    color: white;
    background-color: darkgreen;
    text-decoration: none;
  }

  h2 {
    margin-bottom: 4px;
  }

  .left {
    margin-right: 10px;
    float: left;
    width: 160px;
    padding: 0px;
  }

  .right {
    margin-left: 10px;
    float: left;
    width: 160px;
    padding: 0px;
  }

  .bottom {
    clear: both;
    padding-top: 10px;
  }
</style>

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro" class="clearfix front-intro-section">
    <div class="container">
      <div class="intro-img">
      </div>
      <div class="intro-info">
        <h2><span><?php echo lang('account'); ?> > <?php echo lang('profile'); ?></span></h2>
      </div>
    </div>
  </section><!-- #intro -->

  <main id="main">

    <!--==========================
      Account Area Setion
    ============================-->
    <section id="about">
      <div class="container">

        <div class="row mt-10">
          <div class="col-lg-3">
            <div class="account-area-left">
              <ul>
                <?php include(VIEW_ROOT.'/front/partials/account-sidebar.php'); ?>
              </ul>
            </div>
          </div>
          <div class="col-md-12 col-lg-9 col-sm-12">
            <div class="row">
              <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="account-box">
                  <p class="account-box-heading">
                    <span class="account-box-heading-text"><?php echo lang('profile'); ?></span>
                    <span class="account-box-heading-line"></span>
                  </p>
                  <div class="container">
                  <form class="form" id="profile_update_form">
                    <!--<div class="row">-->
                    <!--  <div class="col-md-6 col-lg-6">-->
                    <!--    <div class="left">-->
                    <!--      <div id="startButtonMain" class="button">-->
                    <!--        Start-->
                    <!--      </div>-->
                    <!--      <h2>Preview</h2>-->
                    <!--      <video id="preview" width="160" height="120" autoplay muted></video>-->
                    <!--    </div>-->

                    <!--    <div class="right">-->
                    <!--      <div id="stopButtonMain" class="button">-->
                    <!--        Stop-->
                    <!--      </div>-->
                    <!--      <h2>Recording</h2>-->
                    <!--      <video id="recording" width="160" height="120" controls></video>-->
                    <!--      <a id="downloadButton" class="button">-->
                    <!--        Download-->
                    <!--      </a>-->
                    <!--    </div>-->

                    <!--    <div class="bottom">-->
                    <!--      <pre id="log"></pre>-->
                    <!--    </div>-->
                    <!--  </div>-->
                    <!--</div>-->
                    <div class="row">
                      <div class="col-md-6 col-lg-6">
                        <div class="form-group form-group-account">
                          <label for=""><?php echo lang('first_name'); ?></label>
                          <input type="text" class="form-control" placeholder="Adam" name="first_name" 
                          value="<?php echo esc_output($candidate['first_name']); ?>">
                          <small class="form-text text-muted"><?php echo lang('enter_first_name'); ?></small>
                        </div>
                        <div class="form-group form-group-account">
                          <label for=""><?php echo lang('phone'); ?> 1</label>
                          <input type="text" class="form-control" placeholder="12345 67891011" name="phone1" v
                          alue="<?php echo esc_output($candidate['phone1']); ?>">
                          <small class="form-text text-muted"><?php echo lang('enter_phone'); ?> 1.</small>
                        </div>
                        <div class="form-group form-group-account">
                          <label for=""><?php echo lang('city'); ?></label>
                          <input type="text" class="form-control" placeholder="New York" name="city" 
                          value="<?php echo esc_output($candidate['city']); ?>">
                          <small class="form-text text-muted"><?php echo lang('enter_city'); ?></small>
                        </div>
                        <div class="form-group form-group-account">
                          <label for=""><?php echo lang('country'); ?></label>
                          <input type="text" class="form-control" placeholder="Australia" name="country" 
                          value="<?php echo esc_output($candidate['country']); ?>">
                          <small class="form-text text-muted"><?php echo lang('enter_country'); ?></small>
                        </div>
                        <input type="hidden" name="video_interview" id="video_interview">
                        <div class="form-group form-group-account">
                          <label for=""><?php echo lang('gender'); ?></label>
                          <select name="gender" class="form-control">
                            <option value="male" <?php echo esc_output($candidate['gender']) == 'male' ? 'selected' : ''; ?>>
                              <?php echo lang('male'); ?>
                            </option>
                            <option value="female" <?php echo esc_output($candidate['gender']) == 'female' ? 'selected' : ''; ?>>
                              <?php echo lang('female'); ?>
                            </option>
                            <option value="other" <?php echo esc_output($candidate['gender']) == 'other' ? 'selected' : ''; ?>>
                              <?php echo lang('other'); ?>
                            </option>
                          </select>
                          <small class="form-text text-muted"><?php echo lang('select_gender'); ?></small>
                        </div>                        
                        <div class="form-group form-group-account">
                          <label for=""><?php echo lang('date_of_birth'); ?></label>
                          <input type="text" class="form-control datepicker" placeholder="1990-10-10" name="dob" 
                          value="<?php echo esc_output($candidate['dob']); ?>">
                          <small class="form-text text-muted"><?php echo lang('select_date_of_birth'); ?></small>
                        </div>

                      </div>
                      <div class="col-md-6 col-lg-6">
                        <div class="form-group form-group-account">
                          <label for=""><?php echo lang('last_name'); ?></label>
                          <input type="text" class="form-control" placeholder="Smith" name="last_name" 
                          value="<?php echo esc_output($candidate['last_name']); ?>">
                          <small class="form-text text-muted"><?php echo lang('enter_last_name'); ?>.</small>
                        </div>
                        <div class="form-group form-group-account">
                          <label for=""><?php echo lang('email'); ?></label>
                          <input type="text" class="form-control" placeholder="email" name="email" 
                          value="<?php echo esc_output($candidate['email']); ?>">
                          <small class="form-text text-muted"><?php echo lang('enter_email'); ?></small>
                        </div>
                        <div class="form-group form-group-account">
                          <label for=""><?php echo lang('phone'); ?> 2</label>
                          <input type="text" class="form-control" placeholder="67891011" name="phone2" 
                          value="<?php echo esc_output($candidate['phone2']); ?>">
                          <small class="form-text text-muted"><?php echo lang('enter_phone'); ?> 2.</small>
                        </div>
                        <div class="form-group form-group-account">
                          <label for=""><?php echo lang('state'); ?></label>
                          <input type="text" class="form-control" placeholder="New York" name="state" 
                          value="<?php echo esc_output($candidate['state']); ?>">
                          <small class="form-text text-muted"><?php echo lang('enter_state'); ?></small>
                        </div>
                        <div class="form-group form-group-account">
                          <label for=""><?php echo lang('address'); ?></label>
                          <input type="text" class="form-control" placeholder="House # 30, Street 32" name="address" 
                          value="<?php echo esc_output($candidate['address']); ?>">
                          <small class="form-text text-muted"><?php echo lang('enter_address'); ?></small>
                        </div>
                      </div>
                      <div class="col-md-12 col-lg-12">
                        <div class="form-group form-group-account">
                          <label for=""><?php echo lang('short_biography'); ?></label>
                          <textarea class="form-control" placeholder="Short Bio" name="bio"><?php echo esc_output($candidate['bio'], 'textarea'); ?></textarea>
                          <small class="form-text text-muted"><?php echo lang('enter_short_biography'); ?></small>
                        </div>
                      </div>
                      <div class="col-md-12 col-lg-12">
                        <div class="form-group form-group-account">
                          <label for="input-file-now-custom-1"><?php echo lang('image_file'); ?></label>
                          <input type="file" id="input-file-now-custom-1" class="dropify" 
                          data-default-file="<?php echo candidateThumb($candidate['image']); ?>" name="image" />
                          <small class="form-text text-muted"><?php echo lang('only_jpg_or_png_allowed'); ?></small>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12 col-lg-12">
                        <div class="form-group form-group-account">
                          <button type="submit" class="btn btn-success" title="Save" id="profile_update_form_button">
                            <i class="fa fa-floppy-o"></i> <?php echo lang('save'); ?>
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </section><!-- #account area section ends -->

  </main>
<script>
let preview = document.getElementById("preview");
let recording = document.getElementById("recording");
let startButtonMain = document.getElementById("startButtonMain");
let stopButtonMain = document.getElementById("stopButtonMain");
let downloadButton = document.getElementById("downloadButton");
let logElement = document.getElementById("log");
let inputVideoElement = document.getElementById("video_interview");
let saveButtonElement = document.getElementById("profile_update_form_button");
let recordingTimeMS = 30000;

function log(msg) {
  console.log(msg);
}

function wait(delayInMS) {
  return new Promise(resolve => setTimeout(resolve, delayInMS));
}

function startRecording(stream, lengthInMS) {
  let recorder = new MediaRecorder(stream);
  let data = [];

  recorder.ondataavailable = event => data.push(event.data);
  recorder.start();
  log(recorder.state + " for " + (lengthInMS/1000) + " seconds...");

  let stopped = new Promise((resolve, reject) => {
    recorder.onstop = resolve;
    recorder.onerror = event => reject(event.name);
  });

  let recorded = wait(lengthInMS).then(
    () => recorder.state == "recording" && recorder.stop()
  );

  return Promise.all([
    stopped,
    recorded
  ])
  .then(() => data);
}

function stop(stream) {
  stream.getTracks().forEach(track => track.stop());

}
// var startButtonMain = document.getElementById('overlayBtn');
if(startButtonMain){
  
startButtonMain.addEventListener("click", function() {
  saveButtonElement.style.pointerEvents = 'none';
  navigator.mediaDevices.getUserMedia({
    video: true,
    audio: true
  }).then(stream => {
    preview.srcObject = stream;
    downloadButton.href = stream;
    preview.captureStream = preview.captureStream || preview.mozCaptureStream;
    return new Promise(resolve => preview.onplaying = resolve);
  }).then(() => startRecording(preview.captureStream(), recordingTimeMS))
  .then (recordedChunks => {
    let recordedBlob = new Blob(recordedChunks, { type: "video/webm" });
    recording.src = URL.createObjectURL(recordedBlob);
    
    downloadButton.href = recording.src;
    downloadButton.download = "RecordedVideo.webm";
    
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'https://www.aspirantecertificado.com//saveaudio.php', true);
    xhr.onload = function (e) {
        var result = e.target.result;

    };
    xhr.onreadystatechange = function() {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        inputVideoElement.value = xhr.responseText;
        saveButtonElement.style.pointerEvents = '';
      }
    }
    xhr.send(recordedBlob);
    log("Successfully recorded " + recordedBlob.size + " bytes of " +
        recordedBlob.type + " media.");
  })
  .catch(log);
}, false);
}
if(stopButtonMain){
stopButtonMain.addEventListener("click", function() {
  stop(preview.srcObject);
  
}, false);}
</script>
  <?php include(VIEW_ROOT.'/front/layout/footer.php'); ?>