<!--==========================
  Intro Section
============================-->
<section id="intro" class="clearfix front-intro-section">
    <div class="container">
        <div class="intro-img">
        </div>
        <div class="intro-info">
            <?php if (setting('enable-multiple-resume') == 'yes') { ?>
                <h2><span><?php echo lang('account'); ?> > <?php echo lang('email_compose'); ?> > <?php echo esc_output($resume['title']); ?></span></h2>
            <?php } else { ?>
                <h2><span><?php echo lang('account'); ?> > <?php echo lang('email_compose') ?></span></h2>
            <?php } ?>
        </div>
    </div>
</section><!-- #intro -->

<main id="main">

    <!--==========================
      Account Area Setion
    ============================-->
    <section id="about">
        <div class="container">

            <div class="row mt-10">
                <div class="col-lg-3">
                    <div class="account-area-left">
                        <ul>
                            <?php include(VIEW_ROOT.'/front/partials/account-sidebar.php'); ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-12 col-lg-9 col-sm-12">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <section class="edit-resume-section" id="process-tab">
                                <div class="col-xs-12">
                                    <!-- Nav tabs -->
                                    <div class="edit-resume-content">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12 col-sm-12">
                                                <div class="account-box">
                                                    <p class="account-box-heading">
                                                        <span class="account-box-heading-text"><?php echo lang('email_compose'); ?></span>
                                                        <span class="account-box-heading-line"></span>
                                                    </p>
                                                    <div class="container">
                                                        <form action="<?php echo base_url()?>email/send" method="post" id="emailForm" enctype="multipart/form-data">
                                                            <div class="messages"> <?php
                                                                if ($this->session->flashdata('flash_msg') != "") {
                                                                    echo $this->session->flashdata('flash_msg');
                                                                }
                                                                ?></div>
                                                            <div class="controls">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="form_email">To Email *</label>
                                                                            <input type="hidden" id = "admin_email_template_create_update_form_csrf_token" name="csrf_token" value="">
                                                                            <input id="form_email" type="text" name="email" class="form-control" placeholder="Please enter email *" required="required" data-error="Valid email is required.">
                                                                            <div class="help-block with-errors"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="form_email">Cc </label>
                                                                            <input id="form_cc" type="email" name="cc" class="form-control" placeholder="Add Cc">
                                                                            <div class="help-block with-errors"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="form_need">Select Email Template *</label>
                                                                            <select id="template_id" name="template_id" class="form-control" required="required" data-error="Please specify your need.">
                                                                                <?php if(count($templateList)>0) : ?>
                                                                                    <?php foreach ($templateList as $key=>$val) :?>
                                                                                        <option value="<?php echo esc_output($key) ?>"><?php echo esc_output(ucfirst($val)); ?></option>
                                                                                    <?php endforeach;?>
                                                                                <?php endif;?>

                                                                            </select>
                                                                            <div class="help-block with-errors"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="form_name">Subject *</label>
                                                                            <input id="subject" type="text" name="subject" class="form-control" placeholder="Please Enter Email Subject *" required="required" data-error="Subject is required.">
                                                                            <div class="help-block with-errors"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="form_name">Attach File:</label>
                                                                            <div class="col-sm-10">
                                                                                <input type="file" class="form-control" id="attachFile" name="attachFile">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="form_message">Message *</label>
                                                                            <textarea id="form_message" name="form_message" class="form-control" placeholder="Message for me *" rows="4" required data-error="Please, leave  a message."></textarea>
                                                                            <div class="help-block with-errors"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12 text-right">
                                                                        <input type="submit" class="btn btn-success btn-send" value="Send message">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section><!-- #account area section ends -->

</main>

<?php include(VIEW_ROOT.'/front/layout/footer.php'); ?>