<ul style="letter-spacing: -0.2px;">
	<li>
		<a href="<?php echo base_url(); ?>account" <?php echo acActive($page, 'resumes'); ?>>
			<i class="fa fa-file"></i> <?php echo lang('my_resumes'); ?>
		</a>
	</li>
	<li>
		<a href="<?php echo base_url(); ?>account/job-applications" <?php echo acActive($page, 'applications'); ?>>
			<i class="fa fa-check"></i> <?php echo lang('job_applications'); ?>
		</a>
	</li>
	<li class="d-none">
		<a href="<?php echo base_url(); ?>account/job-favorites" <?php echo acActive($page, 'favorites'); ?>>
			<i class="fa fa-heart"></i> <?php echo lang('favorite_jobs'); ?>
		</a>
	</li>
	<!--<li>-->
	<!--	<a href="<?php echo base_url(); ?>account/job-referred" <?php echo acActive($page, 'referred'); ?>>-->
	<!--		<i class="fa fa-user-plus"></i> <?php echo lang('referred_jobs'); ?>-->
	<!--	</a>-->
	<!--</li>-->
	<li class="d-none">
		<a href="<?php echo base_url(); ?>account/quizes" <?php echo acActive($page, 'quizes'); ?>>
			<i class="fa fa-list"></i> <?php echo lang('quizes'); ?>
		</a>
	</li>
	<li>
		<a href="<?php echo base_url(); ?>account/profile" <?php echo acActive($page, 'profile'); ?>>
			<i class="fa fa-user"></i> <?php echo lang('profile'); ?>
		</a>
	</li>
	<li>
		<a href="<?php echo base_url(); ?>account/password" <?php echo acActive($page, 'password'); ?>>
			<i class="fa fa-key"></i> <?php echo lang('password'); ?>
		</a>
	</li>
	<!--<li>-->
	<!--	<a href="<?php echo base_url(); ?>blogs">-->
	<!--		<i class="fas fa-blog"></i> <?php echo lang('news_announcements'); ?>-->
	<!--	</a>-->
	<!--</li>-->
	<li class="d-none">
		<a href="<?php echo base_url(); ?>jobs">
			<i class="fa fa-briefcase"></i> <?php echo lang('jobs'); ?>
		</a>
	</li>
  <!-- <li class="nav-item">
    <a  class="nav-link collapsed text-truncate" href="#submenu1" data-toggle="collapse" data-target="#submenu1">
      <i class="fa fa-briefcase"></i> <?php echo lang('email_manage'); ?> <i class="fas fa-chevron-down"></i>
    </a>


    <div class="collapse" id="submenu1" aria-expanded="false">
      <ul class="flex-column pl-2 nav">
        <li class="nav-item">
          <a href="<?php echo base_url(); ?>email/compose" <?php echo acActive($page, 'email_compose'); ?> class="nav-link py-0" > <i class="fa fa-plus-square"></i> <?php echo lang('email_compose'); ?> </a>
        </li>
        <li class="nav-item">
          <a href="<?php echo base_url(); ?>email/inbox" <?php echo acActive($page, 'email_inbox'); ?> class="nav-link py-0" > <i class="fa fa-inbox"></i> <?php echo lang('email_inbox'); ?></a>
        </li>
        <li class="nav-item">
          <a href="<?php echo base_url(); ?>email/send" <?php echo acActive($page, 'email_send'); ?> class="nav-link py-0" ><i class="fa fa-paper-plane"></i> <?php echo lang('email_send'); ?></a>
        </li>
        <li class="nav-item">
          <a href="<?php echo base_url(); ?>email/trash" <?php echo acActive($page, 'email_trash'); ?> class="nav-link py-0" ><i class="fa fa-trash"></i> <?php echo lang('email_trash'); ?></a>
        </li>

      </ul>
    </div>
  </li> -->
  <li>
    <!--<a href="<?php echo base_url(); ?>email/details" <?php echo acActive($page, 'email_details'); ?> > <i class="fa fa-plus-square"></i> <?php echo lang('email_details'); ?> </a>-->
  </li>
	<li>
		<a href="<?php echo base_url(); ?>logout">
			<i class="fas fa-sign-out-alt"></i><?php echo lang('logout'); ?>
		</a>
	</li>
</ul>


