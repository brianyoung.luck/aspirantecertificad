<style type="text/css">
  body {
    padding: 0;
    margin: 0;
  }

  svg:not(:root) {
    display: block;
  }

  .playable-code {
    background-color: #f4f7f8;
    border: none;
    border-left: 6px solid #558abb;
    border-width: medium medium medium 6px;
    color: #4d4e53;
    height: 100px;
    width: 90%;
    padding: 10px 10px 0;
  }

  .playable-canvas {
    border: 1px solid #4d4e53;
    border-radius: 2px;
  }

  .playable-buttons {
    text-align: right;
    width: 90%;
    padding: 5px 10px 5px 26px;
  }

  video {
    margin-top: 2px;
    border: 1px solid black;
  }
  
  .label {
    background-color: #121212;
  }
  
  .label {
        font-family: 'Orbitron', sans-serif;
        color: #66ff99;
        font-size: 18px;
        padding: 4px;
        padding-left: 10px;
        font-weight: 700;
        width: 100%;
        float: left;
    }

    
  .button {
    cursor: pointer;
    display: block;
    width: 160px;
    font-size: 16px;
    text-align: center;
    padding-top: 2px;
    padding-bottom: 4px;
    color: white;
    background-color: #28a745;
    text-decoration: none;
    height: 40px;
    padding-top: 4px;
    font-family: auto;
    font-size: 20px;
    font-weight: bolder;
    border-radius: 3px;
  }
  #record_video_btn{
      border:none;
  }

  h2 {
    margin-bottom: 4px;
  }

  .left {
    margin-right: 10px;
    float: left;
    width: 160px;
    padding: 0px;
  }

  .right {
    margin-left: 10px;
    float: left;
    width: 160px;
    padding: 0px;
  }

  .bottom {
    clear: both;
    padding-top: 10px;
  }
  
</style>
        
    <div class="container" style="margin-top:30px">
      <form class="form" id="record_video_form">
        <div class="row">
            
            <?php
                $this->db->where('candidates.candidate_id', candidateSession());
                $this->db->select('*');
                $this->db->from('candidates');
                $result = $this->db->get();
                $candidateViewData = ($result->num_rows() == 1) ? objToArr($result->row(0)) : array();
            ?>
            
            <h3 style="width: 90%; text-align: center; margin: auto; margin-bottom: 19px;">Completa el proceso de reclutamiento y graba en 5 minutos máximo</h3>
            <span style="width: 100%; margin-left: 15px;">-	Platica sobre tu experiencia en puestos similares</span>
            <span style="width: 100%; margin-left: 15px;">-	Hábilidades</span>
            <span style="width: 100%; margin-left: 15px;">-	¿Qué sabes de la empresa que estás aplicando?</span>
            <span style="width: 100%; margin-left: 15px; margin-bottom:20px">-	¿Cómo te ves en los próximos años?</span>
            
            <div class="col-md-12 col-lg-12">
                <video id="video" autoplay style="width:100%"></video>
                <label class="label"><span>Grabar: </span><span id="record_time">00:00</span></label>
            </div>
            <div class="bottom">
                <pre id="log"></pre>
            </div>
        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-6 col-lg-6 col-sm-6" style="margin-top:10px">
                <div id="startButtonMain" class="button" style="width:100%; ">Inicio</div>  
            </div>
            
            <div class="col-md-6 col-lg-6 col-sm-6" style="margin-top:10px">
                <button type="button" class="btn btn-success" title="Save" id="record_video_btn" style="pointer-events: none; width:100%">
                    <i class="fa fa-floppy-o"></i> <?php echo "Parar"; ?>
                </button>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="form-group form-group-account" style="margin-left: 0px; margin-right: 0px;">
                    
                </div>
            </div>
        </div>
        
        <div class="bottom">
            <pre id="log"></pre>
        </div>
        
        <input type="hidden" name="video_interview" id="video_interview">
        
      </form>
    </div>

    <script>
        const constraints = { "video": { width: { max: 1200 } }, "audio" : true };
        let inputVideoElement = document.getElementById("video_interview");
        var theStream;
        var theRecorder;
        var recordedChunks = [];
        
        var timeoutHandle=0;
            var min="00";
            
        function currentTime(sec, min) {
            if (sec==60){
                sec=0;
                min=min+1
            }
            
            minstr = updateTime(min);
            secstr = updateTime(sec);
            
            document.getElementById("record_time").innerText =  minstr + ":" + secstr;
            current_time = document.getElementById("record_time").textContent
            if(current_time == "04:58"){
                download();
            }
            timeoutHandle = setTimeout(function(){ currentTime(parseInt(sec)+1, min) }, 1000);
        }
        
        function updateTime(k) {
            if (k < 10) {
              return "0" + k;
            }
            else {
              return k;
            }
        }
        function startFunction() {
           
          navigator.mediaDevices.getUserMedia(constraints)
              .then(gotMedia)
              .catch(e => { console.error('getUserMedia() failed: ' + e); });
        }
        
        function gotMedia(stream) {
          currentTime(0,0);
          theStream = stream;
          var video = document.getElementById("video");
          video.srcObject = stream;
          try {
            recorder = new MediaRecorder(stream, {mimeType : "video/webm"});
          } catch (e) {
              alert("Requested device not found. Please check your devices.")
            // console.error('Exception while creating MediaRecorder: ' + e);
            return;
          }
          
          theRecorder = recorder;
          recorder.ondataavailable = 
              (event) => { recordedChunks.push(event.data); };
          recorder.start(100);
        }
        
        function download() {
          
          theStream.getTracks().forEach(track => { track.stop(); });
          
          let recordedBlob = new Blob(recordedChunks, { type: "video/webm" }); 
          clearTimeout(timeoutHandle);
          timeoutHandle = null;
          theRecorder.stop();
          
            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'https://www.aspirantecertificado.com/saveaudio.php', true);
            xhr.onload = function (e) {
                var result = e.target.result;
            };
        
            xhr.onreadystatechange = function() {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    inputVideoElement.value = xhr.responseText;
                    console.log(inputVideoElement.value)
                    window.location.href="https://www.aspirantecertificado.com/account/job-applications?video_interview="+inputVideoElement.value;
                    alert("Gracias por subir tu video, nos mantendremos en contacto sobre el progreso de tu aplicación.")
                    $('#modal-default > div > div > div.modal-header.resume-modal-header > button').trigger("click");
                }
            }
            xhr.send(recordedBlob);
        
        }

        let startButtonMain = document.getElementById("startButtonMain");
        let stopButtonMain = document.getElementById("record_video_btn");
        stopButtonMain.style.pointerEvents = 'none';
        stopButtonMain.style.background = '#aaaaaa';
        
        
        startButtonMain.addEventListener("click", function() {
            stopButtonMain.style.pointerEvents = 'auto';
            stopButtonMain.style.background = '#28a745';
            startButtonMain.style.pointerEvents = 'none';
            startButtonMain.style.background = '#aaaaaa';
            startFunction();
        }, false);
        
        stopButtonMain.addEventListener("click", function() {
            download()
            stopButtonMain.style.pointerEvents = 'none';
            stopButtonMain.style.background = '#aaaaaa';
        }, false);
    </script>
    