    <style type="text/css">
      body {
        padding: 0;
        margin: 0;
      }
    
      .playable-code {
        background-color: #f4f7f8;
        border: none;
        border-left: 6px solid #558abb;
        border-width: medium medium medium 6px;
        color: #4d4e53;
        height: 100px;
        width: 90%;
        padding: 10px 10px 0;
      }
    
      .label {
        background-color: #121212;
      }
      
      .label {
            font-family: 'Orbitron', sans-serif;
            color: #66ff99;
            font-size: 18px;
            padding: 4px;
            padding-left: 10px;
            font-weight: 700;
            width: 100%;
            float: left;
       }
    
        
      .button {
        cursor: pointer;
        display: block;
        width: 160px;
        font-size: 16px;
        text-align: center;
        padding-top: 2px;
        padding-bottom: 4px;
        color: white;
        background-color: #28a745;
        text-decoration: none;
        height: 40px;
        padding-top: 4px;
        font-family: auto;
        font-size: 20px;
        font-weight: bolder;
        border-radius: 3px;
      }
    
      h3 {
        margin-bottom: 4px;
      }
    
      .bottom {
        clear: both;
        padding-top: 10px;
      }
    </style>
        
    <div class="container" style="margin-top:30px">
      <form class="form" id="upload_document_form">
        <div class="row">
            <h3 style="width: 90%; text-align: center; margin: auto; margin-bottom: 19px;">Sube tu documento para seguir el proceso</h3>
            <span style="width: 100%; margin-left: 15px;">	“Comprobante de domicilio”</span>
            <span style="width: 100%; margin-left: 15px; margin-bottom:20px">	“Identificación oficial  (INE o pasarporte)”</span>
            <div class="col-md-6 col-lg-6">
                Comprobante de domicilio
                <input type="file" id="input-file-now-custom-11" class="dropify" data-default-file="" name="paddress" style="font-size:12px; width: 218px;"/>
                <small class="form-text text-muted"><?php echo "Sólo se aceptan archivos pdf,jpg o png"; ?></small>
            </div>
            <div class="col-md-6 col-lg-6">
                Identificación oficial
                <input type="file" id="input-file-now-custom-22" class="dropify" data-default-file="" name="identification" style="font-size:12px; width: 218px;"/>
                <small class="form-text text-muted"><?php echo "Sólo se aceptan archivos pdf,jpg o png"; ?></small>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="form-group form-group-account" style="margin-left: 0px; margin-right: 0px;">
                    <button type="submit" class="btn btn-success" title="Save" id="upload_document_btn" style="width:100%">
                        <i class="fa fa-floppy-o"></i> <?php echo lang('save'); ?>
                    </button>
                </div>
            </div>
        </div>
      </form>
    </div>
    
    <!--<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>-->
    <script>
        $(document).ready(function(){
            $("#upload_document_btn").css("pointer-events", "none")
            $("#upload_document_btn").css("background", "#7f8a81")
            $("#upload_document_btn").css("border", "none")
            
            
        })
        $("#input-file-now-custom-11").on("change", function(e){
            if (($(this).val()!="")&&($("#input-file-now-custom-22").val()!="")){
                $("#upload_document_btn").css("pointer-events","auto")
                $("#upload_document_btn").css("background", "#0d6efd")
            }else{
                 $("#upload_document_btn").css("pointer-events", "none")
                 $("#upload_document_btn").css("background", "#7f8a81")
            }
            fileName = $(this).val();
            fileExtension = fileName.substr((fileName.lastIndexOf('.') + 1));
            var validFileExtensions = ['png', 'pdf', 'jpg'];
            if ($.inArray(fileExtension, validFileExtensions) == -1) {
                alert("Failed!! Please upload png, pdf, jpg file only.")
                $(this).val("")
            }
            
        });
        $("#input-file-now-custom-22").on("change", function(e){
            if (($(this).val()!="")&&($("#input-file-now-custom-11").val()!="")){
                $("#upload_document_btn").css("pointer-events","auto")
                $("#upload_document_btn").css("background", "#0d6efd")
            }else{
                 $("#upload_document_btn").css("pointer-events", "none")
                 $("#upload_document_btn").css("background", "#7f8a81")
            }
            fileName = $(this).val();
            fileExtension = fileName.substr((fileName.lastIndexOf('.') + 1));
            var validFileExtensions = ['png', 'pdf', 'jpg'];
            if ($.inArray(fileExtension, validFileExtensions) == -1) {
                alert("Failed!! Please upload png, pdf, jpg file only.")
                $(this).val("")
            }
        });
    </script>
    
    
    