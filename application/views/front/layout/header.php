
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?php echo esc_output($page); ?></title>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="<?php echo esc_output(setting('site-keywords')); ?>" name="keywords">
  <meta content="<?php echo esc_output(setting('site-description')); ?>" name="description">
  <meta property="route" content="<?php echo base_url(); ?>">
  <meta property="token" content="<?php echo esc_output($this->security->get_csrf_hash()); ?>">

  <!-- Favicon -->
  <link href="<?php echo base_url(); ?>assets/front/images/<?php echo setting('site-favicon'); ?>" rel="icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- CSS Libraries (For External components/plugins) -->
  <link href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/front/css/font-awesome-all.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/front/css/dropify.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/front/css/jquery-ui.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/front/css/bootstrap-social.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/front/css/bar-rating-pill.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/front/plugins/iCheck/square/blue.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet">
  <!-- Internal Style files -->
  <link href="<?php echo base_url(); ?>assets/front/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/front/css/custom-style.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/front/css/candidatefinder.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/js/jquery-ui.min.js"></script>
  <style>
      .menu_link{
          color:#000;
          padding: 5px;
          padding-left: 12px;
      }
      @media(min-width:768px) {
        	.container {
        		width: 720px;
        		max-width: 100%
        	}
        }
        
        @media(min-width:992px) {
        	.container {
        		width: 960px;
        		max-width: 100%
        	}
        }
        #menulinks{
            background:#fff;
        }

  </style>
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">
    <?php 
    if((current_url() == "https://www.aspirantecertificado.com/") || (current_url()=="https://www.aspirantecertificado.com")){
        echo '<style>
            #headerbar{
                display:block;
            }
            #header{
                height:76px;
            }
            .navbar-brand img{
                padding-top: 8px;
            }
            #menulinks{
                margin-top:-59px
            }
            #accountBtn{
                padding-left: 15px;
                padding-top: 10px;
                padding-right: 15px;
            }
            @media(max-width:768px) {
            	#menulinks {
            	    margin-top:0px;
            	}
            }
        </style>';
    } 
    ?>
    <nav class="navbar navbar-expand-md navbar-light" id="headerbar">
        <a href="https://www.aspirantecertificado.com/" class="navbar-brand logo" >
            <img src="<?php echo base_url(); ?>assets/front/images/<?php echo setting('site-logo'); ?>" height="50" alt="CoolBrand">
        </a>
        <button type="button" class="navbar-toggler" style="width:55px;     float: right;" data-toggle="collapse" data-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-nav">
                
            </div>
            <div class="navbar-nav ml-auto" id="menulinks">
                <a class="menu_link <?php //echo $pagedata=="home"?"active":""; ?>" href="<?php echo base_url(); ?>"><?php echo lang('start'); ?></a>
                <a class="menu_link <?php //echo $pagedata=="aboutus"?"active":"";?>" href="<?php echo base_url(); ?>aboutus"><?php echo lang('about_us'); ?></a>
                <a class="menu_link <?php //echo $pagedata=="business"?"active":"";?>" href="<?php echo base_url(); ?>business"><?php echo lang('business'); ?></a>
                <a class="menu_link <?php //echo $pagedata=="candidate"?"active":"";?>" href="<?php echo base_url(); ?>candidate"><?php echo lang('candidate'); ?></a>
                
                <?php if (candidateSession()) { ?>
                <a class="btn btn-primary btn-sm" id="accountBtn" href="<?php echo base_url(); ?>account"
                  title="<?php echo esc_output($this->session->userdata('candidate')['first_name']); ?>">
                  Hola, <?php echo trimString($this->session->userdata('candidate')['first_name'], 7); ?>
                </a>
                <?php } else { ?>
                <a class="btn btn-primary btn-sm" id="accountBtn" href="<?php echo base_url(); ?>account"><?php echo lang('account'); ?></a>
                <?php } ?>
            </div>
        </div>
    </nav>
    </div>
  </header><!-- #header -->