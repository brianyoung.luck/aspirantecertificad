  <!-- Top Modal -->
  <div class="modal fade in" id="modal-default" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header resume-modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title resume-modal-title">Refer Job</h4>
      </div>
      <div class="modal-body-container">
      </div>
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
  </div>
  </div> 
  
  <!--==========================
    Footer
  ============================-->
  <?php 
    if((current_url() == "https://www.aspirantecertificado.com/") || (current_url()=="https://www.aspirantecertificado.com")){
        echo '<style>
            #footer .footer-top {
                background: #eb7338 !important;
            }
        </style>';
    } 
?>
  <?php $footer = footerColumns(); ?>
  <footer id="footer">
      <div class="floating-wpp"></div>

    <?php if ($footer['columns']) { ?>
    <div class="footer-top">
      <div class="container">
        <div class="row">
          <?php foreach ($footer['columns'] as $column) { ?>
          <div class="col-md-<?php echo esc_output($footer['column_count']); ?> col-sm-12">
            <?php echo esc_output($column['content'], 'raw'); ?>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
    <?php } ?>
  </footer><!-- #footer -->
<style>
    .footer_facebook_icon{
        width: 36px !important;
        border: solid !important;
        border-color: #444 !important;
        border-radius: 5px !important;
        margin-right: 4px !important;
    }
    .footer_linkedin_icon{
        width: 46px !important;
    }
    .footer_instagram_icon{
        width: 48px !important;
    }
</style>
  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets\front\css\floating.css">
  <script src="<?php echo base_url(); ?>assets\front\js\floating.min.js"></script>
  <script>
      $(function () {
          $('.floating-wpp').floatingWhatsApp({
            phone: '+5215625915555',
            showPopup: true,
            zindex:999999,
            message: '¡Hola! '
          });
        });
  </script>
    <style>
        .floating-wpp-popup{
            display:none !important;
        }
        .floating-wpp .floating-wpp-button{
            background-image: url('https://www.jqueryscript.net/demo/Floating-WhatsApp-Message-Button-jQuery/whatsapp.svg');
        }
        .floating-wpp{
            z-index: 99999;
        }
    </style>
  <!-- JavaScript Libraries (For External components/plugins) -->

  <script src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/js/dropify.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/js/bar-rating.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/plugins/iCheck/iCheck.js"></script>

  <!-- JS Language Variables file -->
  <script src="<?php echo base_url(); ?>assets/front/js/lang.js"></script>

  <!-- Files For Functionalities -->
  <script src="<?php echo base_url(); ?>assets/front/js/app.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/js/main.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/js/account.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/js/general.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/js/dot_menu.js"></script>

  </body>
</html>