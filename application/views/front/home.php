<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel='dns-prefetch' href='//js.hsforms.net' />
    <link rel='dns-prefetch' href='//maxcdn.bootstrapcdn.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 .07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
        }
        .Clients__map.hidden-sm-down svg:nth-child(2) {
        display: none !important;
        } 
    </style>
    <link rel='stylesheet' id='wp-block-library-css' href='https://www.aspirantecertificado.com/assets/front/css/homepage/dist/block-library/style.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='contact-form-7-css' href='https://www.aspirantecertificado.com/assets/front/css/homepage/plugins/contact-form-7/includes/css/styles.css' type='text/css' media='all' />
     <link rel='stylesheet' id='olark-wp-css' href='https://www.aspirantecertificado.com/assets/front/css/homepage/plugins/olark-live-chat/public/css/olark-wp-public.css' type='text/css' media='all' /> 
    <link rel='stylesheet' id='acf-fonticonpicker-icons-css' href='https://www.aspirantecertificado.com/assets/front/css/homepage/plugins/acf-fonticonpicker-master/icons/css/iconmoon.css' type='text/css' media='all' />
    <link rel='stylesheet' id='sage/font-awesome.css-css' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='sage/main.css-css' href='https://www.aspirantecertificado.com/assets/front/css/homepage/themes/evaluar/dist/styles/main_40e37c92.css' type='text/css' media='all' />
    <link rel="stylesheet" type="text/css" href="https://www.aspirantecertificado.com/assets/front/css/homepage/plugins/smart-slider-3/Public/SmartSlider3/Application/Frontend/Assets/dist/smartslider.min.css?ver=843132f5" media="all" />
    <style type="text/css">
        @font-face {
        font-family: customFont;
        src: url('https://www.aspirantecertificado.com/assets/front/wp-content/font/sansation_light.woff');
        }
        @font-face {
        font-family: icomoon;
        src: url('https://www.aspirantecertificado.com/assets/front/wp-content/font/icomoon_d699321e.ttf');
        }
        .n2-ss-spinner-simple-white-container {
        position: absolute;
        top: 50%;
        left: 50%;
        margin: -20px;
        background: #fff;
        width: 20px;
        height: 20px;
        padding: 10px;
        border-radius: 50%;
        z-index: 1000;
        }
        .n2-ss-spinner-simple-white {
        outline: 1px solid RGBA(0,0,0,0);
        width:100%;
        height: 100%;
        }
        .n2-ss-spinner-simple-white:before {
        position: absolute;
        top: 50%;
        left: 50%;
        width: 20px;
        height: 20px;
        margin-top: -11px;
        margin-left: -11px;
        }
        .n2-ss-spinner-simple-white:not(:required):before {
        content: '';
        border-radius: 50%;
        border-top: 2px solid #333;
        border-right: 2px solid transparent;
        animation: n2SimpleWhite .6s linear infinite;
        }
        @keyframes n2SimpleWhite {
        to {transform: rotate(360deg);}
        }
    </style>
    <script type='text/javascript' src='https://www.aspirantecertificado.com/assets/front/js/homepage/jquery/jquery.js' id='jquery-core-js'></script>
    <script type='text/javascript' id='olark-wp-js-extra'>
        /* <![CDATA[ */
        var olark_vars = {"site_ID":"2442-599-10-3604","expand":"0","float":"0","override_lang":"1","lang":"es-ES","api":"","mobile":"1","woocommerce":"","woocommerce_version":null,"enable_cartsaver":"0"};
        /* ]]> */
    </script>
    <!-- <script type='text/javascript' src='https://www.aspirantecertificado.com/assets/front/js/homepage/plugins/olark-live-chat/public/js/olark-wp-public.js?ver=1.0.0' id='olark-wp-js'></script> -->
    <script type='text/javascript' src='//js.hsforms.net/forms/v2.js?ver=5.5.3' id='hsforms-js'></script>
    <!--[if lt IE 8]>
    <script type='text/javascript' src='//js.hsforms.net/forms/v2-legacy.js?ver=5.5.3' id='hsformsIE-js'></script>
    <![endif]-->
    <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
    <script type="text/javascript">(function(){var N=this;N.N2_=N.N2_||{r:[],d:[]},N.N2R=N.N2R||function(){N.N2_.r.push(arguments)},N.N2D=N.N2D||function(){N.N2_.d.push(arguments)}}).call(window);if(!window.n2jQuery){window.n2jQuery={ready:function(cb){console.error('n2jQuery will be deprecated!');N2R(['$'],cb)}}}window.nextend={jQueryFallback:'https://www.aspirantecertificado.com/assets/front/wp-includes/js/jquery/jquery.js',localization:{},ready:function(cb){console.error('nextend.ready will be deprecated!');N2R('documentReady',function($){cb.call(window,$)})}};</script><script type="text/javascript" src="https://www.aspirantecertificado.com/assets/front/wp-content/plugins/smart-slider-3/Public/SmartSlider3/Application/Frontend/Assets/dist/n2.min.js?ver=843132f5"></script>
    <script type="text/javascript" src="https://www.aspirantecertificado.com/assets/front/wp-content/plugins/smart-slider-3/Public/SmartSlider3/Application/Frontend/Assets/dist/smartslider-frontend.min.js"></script>
    <script type="text/javascript" src="https://www.aspirantecertificado.com/assets/front/wp-content/plugins/smart-slider-3/Public/SmartSlider3/Slider/SliderType/Simple/Assets/dist/smartslider-simple-type-frontend.min.js"></script>
    <script type="text/javascript">N2R('documentReady',function($){N2R(["documentReady","smartslider-frontend","smartslider-simple-type-frontend"],function(){new N2Classes.SmartSliderSimple('#n2-ss-2',{"admin":false,"callbacks":"","background.video.mobile":1,"alias":{"id":0,"smoothScroll":0,"slideSwitch":0,"scroll":1,"scrollSpeed":400},"align":"normal","isDelayed":0,"load":{"fade":1,"scroll":0},"playWhenVisible":1,"playWhenVisibleAt":0.5,"responsive":{"hideOn":{"desktopLandscape":false,"desktopPortrait":false,"tabletLandscape":false,"tabletPortrait":false,"mobileLandscape":false,"mobilePortrait":false},"onResizeEnabled":true,"type":"auto","downscale":1,"upscale":1,"minimumHeight":0,"maximumSlideWidth":{"desktopLandscape":3000,"desktopPortrait":3000,"tabletLandscape":3000,"tabletPortrait":3000,"mobileLandscape":3000,"mobilePortrait":3000},"forceFull":0,"forceFullOverflowX":"body","forceFullHorizontalSelector":"","constrainRatio":1,"sliderHeightBasedOn":"real","decreaseSliderHeight":0,"focusUser":1,"focusEdge":"auto","breakpoints":[{"device":"tabletPortrait","type":"max-screen-width","portraitWidth":1199,"landscapeWidth":1199},{"device":"mobilePortrait","type":"max-screen-width","portraitWidth":700,"landscapeWidth":900}],"enabledDevices":{"desktopLandscape":0,"desktopPortrait":1,"tabletLandscape":0,"tabletPortrait":1,"mobileLandscape":0,"mobilePortrait":1},"sizes":{"desktopPortrait":{"width":1200,"height":600,"max":3000,"min":1200},"tabletPortrait":{"width":701,"height":350,"max":1199,"min":701},"mobilePortrait":{"width":320,"height":160,"max":900,"min":320}},"normalizedDeviceModes":{"unknown":"desktopPortrait","desktopPortrait":"desktopPortrait","desktopLandscape":"desktopPortrait","tabletLandscape":"desktopPortrait","tabletPortrait":"tabletPortrait","mobileLandscape":"tabletPortrait","mobilePortrait":"mobilePortrait"},"overflowHiddenPage":0,"focus":{"offsetTop":"#wpadminbar","offsetBottom":""}},"controls":{"mousewheel":0,"touch":"horizontal","keyboard":1,"blockCarouselInteraction":1},"lazyLoad":0,"lazyLoadNeighbor":0,"blockrightclick":0,"maintainSession":0,"autoplay":{"enabled":0,"start":1,"duration":8000,"autoplayLoop":1,"allowReStart":0,"pause":{"click":1,"mouse":"0","mediaStarted":1},"resume":{"click":0,"mouse":0,"mediaEnded":1,"slidechanged":0},"interval":1,"intervalModifier":"loop","intervalSlide":"current"},"perspective":1500,"layerMode":{"playOnce":0,"playFirstLayer":1,"mode":"skippable","inAnimation":"mainInEnd"},"bgAnimationsColor":"RGBA(51,51,51,1)","bgAnimations":0,"mainanimation":{"type":"horizontal","duration":800,"delay":0,"ease":"easeOutQuad","parallax":0,"shiftedBackgroundAnimation":0},"carousel":1,"dynamicHeight":0,"initCallbacks":function($){new N2Classes.FrontendItemVimeo(this,"n2-ss-2item1","n2-ss-2",{"vimeourl":"298181653","image":"$upload$\/2019\/04\/Kruger-thumbnail-caso-exito.jpg","aspect-ratio":"fill","autoplay":"0","ended":"","title":"1","byline":"1","portrait":"0","color":"00adef","loop":0,"start":"0","scroll-pause":"partly-visible","volume":"1","quality":"-1","privateurl":"0","vimeocode":"298181653","privacy-enhanced":0},1,0);N2D("SmartSliderWidgetShadow","SmartSliderWidget",function(t,e){"use strict";function r(t){this.key="shadow",N2Classes.SmartSliderWidget.prototype.constructor.call(this,t)}return((r.prototype=Object.create(N2Classes.SmartSliderWidget.prototype)).constructor=r).prototype.onStart=function(){this.$widget=this.slider.sliderElement.find(".nextend-shadow")},r});new N2Classes.SmartSliderWidgetShadow(this)}})})});</script>
</head>
<body class="home page-template-default page page-id-10 wp-custom-logo app-data index-data singular-data page-data page-10-data page-home-data front-page-data" data-new-gr-c-s-check-loaded="14.991.0" data-gr-ext-installed="">
    <div role="document">
        <div class="content">
            <main class="main">
                <section id="hero" class="Hero wrap">
                    <div class="Hero__overlay"></div>
                    <video autoplay="" loop="" muted="" poster="https://www.aspirantecertificado.com/assets/front/images/homepage/5b2dee64094cd28e4496d2decc600873-1.jpg" class="Hero__video_bg wrap hidden-sm-down">
                        <source src="https://www.aspirantecertificado.com/assets/front/images/homepage/4bd1b9c9da1a3f47235fca640d877fa6-1.mp4" type="video/mp4">
                        <source src="https://www.aspirantecertificado.com/assets/front/images/homepage/7bcfaf8df172d093f6eb839dda3c2144-1.webm" type="video/webm">
                        <source src="https://www.aspirantecertificado.com/assets/front/images/homepage/3219ec37042898bf69845858244b7081-1.ogv" type="video/ogg">
                    </video>
                    <div class="ev-container Hero__content row">
                        <div class="col-12 col-md-6 wow fadeIn" data-wow-delay="600m" style="visibility: visible;">
                            <div class="Hero__content--section">
                                <h1 class="h2">Tu proceso de reclutamiento y selección<br>
                                    simple, inteligente y moderno.
                                </h1>
                                <p><strong>Aspirantecertificado.com</strong>&nbsp;es la herramienta ideal para automatizar todo tu proceso de reclutamiento y selección del personal. Usando inteligencia artificial podrás evaluar y filtrar todos tus candidatos para eliminar hasta el 80% del trabajo operativo, reducir drásticamente los tiempos de selección y contratar solo a los mejores.<strong><br>
                                    Incluye pruebas psicométricas, evaluación de competencias laborales, filtrado de CVs y hasta video-entrevistas.&nbsp;</strong>
                                </p>
                                <a id="requiestModalbtn" style="color:#ff7b36; cursor:pointer;" class="ev__button ev__button--white js-modal-hubspot-btn">Solicitar DEMO</a>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-5 offset-lg-1">
                            <div class="row Hero__benefits">
                                <a class="col Hero__benefits--box Hero__benefits--box-1 wow fadeIn" data-wow-delay="200ms" style="visibility: visible; animation-delay: 200ms;">
                                    <span class="icon-stopwatch"></span>
                                    <p><strong>Selección Virtual</strong></p>
                                    <p>Sistema de Adquisición de Talento que automatiza y optimiza el proceso de<br>
                                        reclutamiento y selección.
                                    </p>
                                </a>
                                <a class="col Hero__benefits--box Hero__benefits--box-2 wow fadeIn" data-wow-delay="400ms" style="visibility: visible; animation-delay: 400ms;">
                                    <span class="icon-handshake"></span>
                                    <p><strong>Encuestas de Talento Humano</strong></p>
                                    <p>Conoce el sentir de tus<br>
                                        colaboradores a través de encuestas.<br>
                                        Clima Laboral, Riesgo Psicosocial, Cultura, entre otras.
                                    </p>
                                </a>
                                <div class="w-100"></div>
                                <a class="col Hero__benefits--box Hero__benefits--box-3 wow fadeIn" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms;">
                                    <span class="icon-line-graph"></span>
                                    <p><strong>Desempeño Laboral</strong></p>
                                    <p>Evalúa el desempeño laboral de tus colaboradores basado en la medición de indicadores y competencias, a través de<br>
                                        procesos de 360 °.
                                    </p>
                                </a>
                                <a class="col Hero__benefits--box Hero__benefits--box-4 wow fadeIn" data-wow-delay="800ms" style="visibility: visible; animation-delay: 800ms;">
                                    <span class="icon-brain"></span>
                                    <p><strong>Prime</strong></p>
                                    <p>Lo hacemos todo por ti.<br>
                                        Nosotros los reclutamos, evaluamos y seleccionamos. ¡Tú los contratas!
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade bd-example-modal-lg Hero__modal" id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="modalVideoLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                    </button>
                                    <div class="embed-responsive embed-responsive-16by9">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="brain" class="Brain">
                    <div class="ev-container row align-items-center">
                        <div class="col-12 col-md-6 text-center u-canvas">
                            <div id="particles-js" class="u-img wow fadeInUp" data-wow-delay="500ms" style="visibility: visible; animation-delay: 500ms;">
                                <img src="https://www.aspirantecertificado.com/assets/front/wp-content/themes/evaluar/dist/images/brain_baefb127.png" alt="Brain">
                                <canvas class="particles-js-canvas-el" width="609" height="209" style="width: 100%; height: 100%;"></canvas>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 text-center wow fadeInRight" data-wow-delay="700ms" style="visibility: visible; animation-delay: 700ms;">
                            <div>
                                <h2><b>Mucho más que un ATS</b><br>
                                    <b>(Applicant Tracking System)</b>
                                </h2>
                                <p style="text-align: justify;">Aspirantecertificado.com además de organizar tus procesos de selección e información de los candidatos, también los evalúa y filtra de manera inteligente analizándolos integralmente en 4 dimensiones:</p>
                                <p style="text-align: justify;">Filtro Curricular + Filtro de Conocimientos Técnicos + Filtro de Competencias Laborales (usando Test Psicométricos de avanzada) hasta Video Entrevistas pregrabadas.</p>
                                <p style="text-align: justify;">El resultado de cada proceso es un ranking con los mejores candidatos organizados de acuerdo a su Ajuste al Puesto, además de un informe muy detallado de cada persona evaluada.</p>
                                <p style="text-align: justify;">¡Con Aspirantecertificado.com tienes un Reclutador Virtual que lleva el proceso de manera automática!</p>
                            </div>
                            <a class="ev__button">Conoce Más</a>
                        </div>
                    </div>
                </section>
                <section id="funnel" class="Funnel">
                    <div class="Funnel__container">
                        <div class="ev-container row">
                            <div id="Funnel__wrapper" class="col-12 col-md-6 Funnel__wrapper">
                                <section class="Funnel__section Funnel__section--recruit" id="recruit" data-percentage="">
                                    <div class="Funnel__section--top" data-order="">
                                        <?= setting('percent-1-section');?>
                                    </div>
                                    <div class="Funnel__insights">
                                        <div class="Funnel__knob--widget">
                                            <div class="knob">
                                                <canvas width="100" height="100"></canvas>
                                                <span class="percentage" data-percent="<?= setting('percent-1-value');?>"><?= setting('percent-1-value');?>%</span>
                                            </div>
                                        </div>
                                        <div class="Funnel__knob--description">
                                            <?= setting('percent-1-des');?>
                                        </div>
                                    </div>
                                </section>
                                <section class="Funnel__section Funnel__section--cv" id="cv" data-percentage="">
                                    <div class="Funnel__section--top" data-order="01">
                                            <?= setting('percent-2-section');?>
                                    </div>
                                    <div class="Funnel__insights">
                                        <div class="Funnel__knob--widget">
                                            <div class="knob">
                                                <canvas width="100" height="100"></canvas>
                                                <span class="percentage" data-percent="<?= setting('percent-2-value');?>"><?= setting('percent-2-value');?>%</span>
                                            </div>
                                        </div>
                                        <div class="Funnel__knob--description">
                                            <?= setting('percent-2-des');?>
                                        </div>
                                    </div>
                                </section>
                                <section class="Funnel__section Funnel__section--knowledge" id="knowledge" data-percentage="">
                                    <div class="Funnel__section--top" data-order="02">
                                        <?= setting('percent-3-section');?>
                                    </div>
                                    <div class="Funnel__insights">
                                        <div class="Funnel__knob--widget">
                                            <div class="knob">
                                                <canvas width="100" height="100"></canvas>
                                                <span class="percentage" data-percent="<?= setting('percent-3-value');?>"><?= setting('percent-3-value');?>%</span>
                                            </div>
                                        </div>
                                        <div class="Funnel__knob--description">
                                            <?= setting('percent-3-des');?>
                                        </div>
                                    </div>
                                </section>
                                <section class="Funnel__section Funnel__section--competences" id="competences" data-percentage="">
                                    <div class="Funnel__section--top" data-order="03">
                                        <?= setting('percent-4-section');?>
                                    </div>
                                    <div class="Funnel__insights">
                                        <div class="Funnel__knob--widget">
                                            <div class="knob">
                                                <canvas width="100" height="100"></canvas>
                                                <span class="percentage" data-percent="<?= setting('percent-4-value');?>"><?= setting('percent-4-value');?>%</span>
                                            </div>
                                        </div>
                                        <div class="Funnel__knob--description">
                                            <?= setting('percent-4-des');?>
                                        </div>
                                    </div>
                                </section>
                                <section class="Funnel__section Funnel__section--video" id="video" data-percentage="">
                                    <div class="Funnel__section--top" data-order="04">
                                        <?= setting('percent-5-section');?>
                                    </div>
                                    <div class="Funnel__insights">
                                        <div class="Funnel__knob--widget">
                                            <div class="knob">
                                                <canvas width="100" height="100"></canvas>
                                                <span class="percentage" data-percent="<?= setting('percent-5-value');?>"><?= setting('percent-5-value');?>%</span>
                                            </div>
                                        </div>
                                        <div class="Funnel__knob--description">
                                            <?= setting('percent-5-des');?>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="col-md-5 Funnel__details hidden-sm-down" id="Funnel__details">
                                <div class="Funnel__sources">
                                    <span class="Funnel__sources__icon Funnel__sources__icon--facebook" style="top: 19.999%; transform: matrix(1, 0, 0, 1, 0, 0);"><i class="fa fa-facebook"></i></span>
                                    <span class="Funnel__sources__icon Funnel__sources__icon--linkedin" style="top: 32.9985%; transform: matrix(1, 0, 0, 1, 0, 0);"><i class="fa fa-linkedin"></i></span>
                                    <span class="Funnel__sources__icon Funnel__sources__icon--twitter" style="top: 59.9995%; transform: matrix(0.9, 0, 0, 0.9, 0, 0);"><i class="fa fa-twitter"></i></span>
                                    <span class="Funnel__sources__icon Funnel__sources__icon--multitrabajos" style="top: 27.9974%; transform: matrix(0.75, 0, 0, 0.75, 0, 0);"><i class="icon-multitrabajos"></i></span>
                                    <span class="Funnel__sources__icon Funnel__sources__icon--indeed" style="top: 69.999%; transform: matrix(0.8, 0, 0, 0.8, 0, 0);"><i class="icon-indeed"></i></span>
                                    <span class="Funnel__sources__icon Funnel__sources__icon--mitula" style="top: 50%; transform: matrix(0.85, 0, 0, 0.85, 0, 0);"><i class="icon-mitula"></i></span>
                                    <span class="Funnel__sources__icon Funnel__sources__icon--trovit" style="top: 40.9997%; transform: matrix(0.7, 0, 0, 0.7, 0, 0);"><i class="icon-trovit"></i></span>
                                    <span class="Funnel__sources__icon Funnel__sources__icon--n1"></span>
                                    <span class="Funnel__sources__icon Funnel__sources__icon--n2"></span>
                                    <span class="Funnel__sources__icon Funnel__sources__icon--n3"></span>
                                    <span class="Funnel__sources__icon Funnel__sources__icon--n4"></span>
                                    <span class="Funnel__sources__icon Funnel__sources__icon--n5"></span>
                                    <span class="Funnel__sources__icon Funnel__sources__icon--n6"></span>
                                </div>
                                <div class="Funnel__details--swirl">
                                    <svg class="Funnel__swirl" width="507px" height="350px" viewBox="0 0 507 350" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="swirl" fill-rule="nonzero" fill="#E0E2EB">
                                                <g id="step1">
                                                    <g id="right" transform="translate(142.471295, 51.696559)">
                                                        <path d="M208.691182,72.1169572 C226.936965,74.141933 243.784755,76.6888807 258.836813,79.6830925 C294.53684,86.7842698 315.4014,96.5464225 321.629362,106.972099 C323.280373,109.734323 323.81988,112.571255 323.212442,115.465201 C318.25055,139.12203 236.531926,160.833506 124.489643,168.264971 L124.991739,168.356389 C178.266119,164.822512 227.059044,157.369421 262.78073,148.034873 C280.623852,143.37153 294.817423,138.269771 304.968628,132.872129 C315.394508,127.327038 321.073118,121.23245 322.281103,115.478963 C322.868851,112.675453 322.329343,109.937804 320.748232,107.288624 C311.72133,92.1652006 268.679868,79.8334912 206.935814,73.3240787" id="Fill-112" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M323.826082,48.1549074 C335.987641,53.1269111 343.751427,58.6218697 347.151898,64.3134279 C348.802909,67.0766352 349.343401,69.9135673 348.735963,72.8075133 C343.774071,96.4643422 262.055447,118.175818 150.012179,125.607283 L150.51526,125.698701 C203.788655,122.165807 252.582564,114.711733 288.30425,105.377185 C306.147372,100.713842 320.340944,95.6130659 330.492148,90.2144411 C340.917044,84.6693496 346.596639,78.5747623 347.803639,72.8222582 C348.391387,70.0187481 347.85188,67.2801157 346.271753,64.6309362 C342.874235,58.9383951 334.655609,53.6410191 322.43498,48.9010029" id="Fill-114" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M358.980695,43.6814803 C360.630721,46.4437046 361.172197,49.2806367 360.563775,52.1745827 C355.601883,75.8314116 273.883259,97.5428875 161.840976,104.975335 L162.344056,105.065771 C215.617452,101.532876 264.410376,94.0797857 300.132062,84.7442541 C317.976169,80.0818948 332.16974,74.9801354 342.31996,69.5815106 C352.745841,64.0374021 358.425436,57.9428147 359.632436,52.1893277 C360.220184,49.3858175 359.680676,46.6471852 358.098581,43.9980057 C343.878428,20.1711181 245.224393,3.2753492 122.02179,5.15779029 C90.4901467,5.64044229 59.5147469,7.30760683 29.956046,10.113083 L30.1234114,8.5294733 C59.7185389,5.72104812 92.1096532,3.99195471 123.677724,3.50930272 C190.160188,2.49386582 251.194416,7.44227755 296.188146,16.392474 C331.887188,23.4936512 352.752733,33.256787 358.980695,43.6814803" id="Fill-116" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M184.907375,64.7002376 C213.392968,66.932626 239.274553,70.3564079 261.308702,74.7395958 C297.008729,81.841756 317.874273,91.6039088 324.102235,102.028602 C325.752261,104.790826 326.292753,107.627758 325.685315,110.521704 C320.722438,134.178533 239.004799,155.890992 126.962516,163.322457 L127.463628,163.412893 C180.738992,159.879998 229.531917,152.426908 265.252618,143.092359 C283.09574,138.429017 297.290296,133.327257 307.441501,127.928632 C317.867381,122.384524 323.545991,116.289937 324.752991,110.536449 C325.341724,107.732939 324.801232,104.994307 323.220121,102.345127 C313.039382,85.2871639 259.582869,71.7817549 184.824677,66.1757175" id="Fill-118" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M329.155292,83.6076136 C330.806303,86.3698378 331.346795,89.2067699 330.738372,92.1016989 C325.77648,115.757545 244.057856,137.469021 132.015573,144.901468 L132.518654,144.991904 C185.792049,141.459009 234.584974,134.005919 270.307644,124.670387 C288.150766,120.008028 302.344338,114.906269 312.494558,109.507644 C322.920439,103.963535 328.600033,97.8689479 329.807033,92.1154609 C330.394781,89.3119507 329.855274,86.5733184 328.274163,83.9241389 C314.05401,60.0972514 215.442309,44.2267493 92.239706,46.1101734 C60.7080623,46.5928254 29.7326624,48.2590069 0.173961579,51.0654661 L0.298008885,48.4556065 C29.8931363,45.6471813 62.2842507,43.9180879 93.852321,43.4354359 C160.33577,42.419999 221.369013,47.3684108 266.362743,56.3186072 C302.06277,63.4197845 322.92733,73.1829202 329.155292,83.6076136" id="Fill-124" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M123.364455,0.652317446 C124.634463,0.628725495 125.904471,0.60709954 127.171526,0.588422579 C193.654975,-0.427997312 254.688218,4.52139741 299.681948,13.4715938 C335.381975,20.5727711 356.247519,30.3349238 362.475482,40.7596172 C364.125508,43.5218415 364.666,46.3597566 364.057577,49.2527196 C359.095685,72.9095485 277.378045,94.6220074 165.334778,102.052489 L165.837859,102.144891 C219.112238,98.6110131 267.905163,91.1579226 303.626849,81.823374 C321.469971,77.1600316 335.663543,72.0582722 345.814747,66.6596474 C356.239643,61.1155389 361.919238,55.0209516 363.126238,49.2674645 C363.71497,46.4639544 363.174478,43.725322 361.593368,41.0771255 C347.373215,17.249255 248.718195,0.353486066 125.516577,2.23691016 C123.872458,2.26148511 122.230308,2.28999205 120.590127,2.32144798" id="Fill-126" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M114.178949,27.9713052 C158.208851,28.7773636 198.344061,31.6555816 232.650033,36.3651248 C288.232086,43.995155 321.235561,57.0886878 331.782536,72.5522287 C334.577538,76.6493642 335.67132,80.9637422 335.004812,85.4727539 C329.560514,122.325347 205.849908,163.27311 34.1625296,185.048481 L34.9442245,185.142849 C116.578181,174.788931 190.955371,158.818164 245.047872,141.159588 C272.067541,132.338165 293.431244,123.17859 308.549263,113.937426 C324.074866,104.444614 332.248205,94.5438591 333.573346,85.5808837 C334.218195,81.2134237 333.133273,77.0504274 330.456411,73.1213845 C313.213835,47.816068 229.720154,31.9721069 114.187809,30.2489115" id="Fill-128" style="fill: rgb(224, 226, 235);"></path>
                                                    </g>
                                                    <g id="left">
                                                        <path d="M147.247412,62.0488049 C129.100079,64.5298918 112.374367,67.4975626 97.4611242,70.8682626 C62.09189,78.862002 41.6644492,89.1421947 35.8932959,99.7182697 C34.3643637,102.520797 33.9479191,105.370508 34.680389,108.247743 C40.6691172,131.767935 123.281669,151.42003 235.579923,156.038155 L235.08078,156.142353 C181.685307,153.946335 132.598016,147.721009 96.4923738,139.285904 C78.4572739,135.072774 64.0500653,130.329809 53.6704563,125.18873 C43.010264,119.908065 37.0687918,113.958961 35.6127128,108.238896 C34.9028865,105.451114 35.323269,102.700686 36.7882086,100.014152 C45.1505723,84.670537 87.629883,71.2653938 149.053974,63.2116915" id="Fill-72" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M53.558223,78.228853 C41.6211311,83.5036201 34.0995325,89.1902633 30.9481403,94.9653763 C29.419208,97.7679035 29.0027635,100.616632 29.7362178,103.49485 C35.7239615,127.015042 118.336514,146.667137 230.634767,151.285261 L230.136609,151.389459 C176.741136,149.193442 127.65286,142.967133 91.5472181,134.53301 C73.5131027,130.319881 59.1058942,125.576916 48.7262851,120.435836 C38.0651083,115.154188 32.1246207,109.206068 30.6675571,103.48502 C29.9577308,100.69822 30.3790979,97.9477921 31.843053,95.2602757 C34.9914917,89.4841797 42.9738374,83.9833231 54.980829,78.9395606" id="Fill-74" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M13.3233809,72.7224917 C12.0612488,75.7520914 11.9854421,78.8642629 13.1343564,82.0403293 C22.5235561,108.001305 112.803413,131.912731 232.677413,140.189574 L232.161534,140.288856 C175.163766,136.352933 122.222148,128.122291 82.7859341,117.841115 C63.0870251,112.704951 47.1735278,107.092032 35.4864995,101.158657 C23.483446,95.0640693 16.4068425,88.3698532 14.1227969,82.0570403 C13.0103091,78.9802567 13.1028524,75.9762149 14.3118213,73.0714559 C25.1935267,46.9433702 126.775472,29.6406401 257.488843,31.8415725 C290.942236,32.4048304 324.01561,34.2676115 355.787473,37.3788 L355.201694,34.515327 C323.389467,31.4011894 288.808819,29.4686154 255.316046,28.9043746 C184.779401,27.7169131 121.054528,33.08015 75.0034424,42.8501667 C38.4646194,50.6030716 18.0913262,61.2912084 13.3233809,72.7224917" id="Fill-76" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M70.4014843,24.3242922 C35.0322501,32.3170486 14.6048093,42.5982243 8.83365605,53.1742993 C7.30472378,55.9768265 6.88729475,58.8255546 7.62074906,61.7037726 C13.6084928,85.2239648 96.2220294,104.87606 208.519299,109.494184 L208.021141,109.598382 C154.625667,107.402365 105.537391,101.177039 69.4327339,92.7419332 C51.397634,88.528804 36.9904254,83.7858388 26.6108164,78.6447595 C15.9506241,73.3640944 10.0091519,67.4149908 8.55307286,61.6949257 C7.84324661,58.9071434 8.26362914,56.1557322 9.72856875,53.4691987 C14.6471429,44.4442945 31.3708856,36.0897948 56.8094438,29.1006793 C61.1835881,27.8994558 65.8146875,26.7375522 70.688959,25.6198835" id="Fill-78" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M178.262093,75.3835654 C149.892671,78.3296103 124.175499,82.4011879 102.344158,87.3348546 C66.9739391,95.328594 46.5474828,105.608787 40.7763295,116.184862 C39.2473973,118.987389 38.8309527,121.836117 39.5634225,124.714335 C45.5521508,148.234527 128.163718,167.886622 240.462957,172.504747 L239.963814,172.608945 C186.569325,170.412927 137.481049,164.186618 101.375407,155.752496 C83.3403074,151.539366 68.9330989,146.795418 58.5534898,141.654339 C47.8932976,136.374657 41.9518254,130.425553 40.4957463,124.704505 C39.7859201,121.917706 40.2063026,119.167278 41.6712422,116.479761 C51.10376,99.1740821 103.940036,85.3609947 178.408784,77.8823463" id="Fill-80" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M12.5249494,76.990079 C10.9960172,79.7935892 10.5785881,82.6423173 11.3120425,85.5195523 C17.3007707,109.039744 99.9133228,128.692823 212.211577,133.309964 L211.712434,133.414162 C158.31696,131.218144 109.229669,124.992818 73.1240273,116.557713 C55.0889274,112.344584 40.6817188,107.601618 30.3021098,102.460539 C19.6419175,97.1798741 13.7004453,91.2307704 12.2443663,85.5107053 C11.53454,82.7229231 11.9549225,79.9724948 13.4198622,77.2849784 C26.5954581,53.1120756 124.454999,33.7509478 247.663509,32.5428433 C279.198106,32.2341819 310.226669,33.1237951 339.889727,35.1871078 L339.653447,33.6084131 C309.953962,31.5431343 277.507716,30.6269802 245.937676,30.9366246 C179.450289,31.5883522 118.670063,38.0653258 74.0927777,48.1400719 C38.7235435,56.1338113 18.2961027,66.414004 12.5249494,76.990079" id="Fill-86" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M239.425291,0.0185863792 C238.155283,0.0274333608 236.886259,0.0372633404 235.61822,0.0500423139 C169.130833,0.701769961 108.350607,7.17874352 63.7733217,17.2534896 C28.4040875,25.247229 7.97664668,35.5274217 2.20549346,46.1034967 C0.676561191,48.9060239 0.259132162,51.754752 0.99258647,54.63297 C6.98033023,78.1531622 89.5938669,97.8052574 201.891136,102.423382 L201.392978,102.52758 C147.997504,100.331562 98.9092288,94.1052531 62.8045713,85.6711306 C44.7694714,81.4580013 30.3622629,76.7140532 19.9826538,71.5729739 C9.321477,66.2932918 3.38098936,60.3441882 1.92491027,54.62314 C1.21508402,51.8363408 1.63546655,49.0859125 3.10040617,46.3983961 C16.2750176,22.2254933 114.135543,2.86436547 237.345038,1.65626098 C238.988172,1.63955001 240.630322,1.62677104 242.271488,1.61694106" id="Fill-88" style="fill: rgb(224, 226, 235);"></path>
                                                    </g>
                                                    <g id="dots" transform="translate(3.000000, 35.000000)">
                                                        <path d="M462.336897,67.280159 C461.348457,66.747374 460.979268,65.515678 461.512869,64.5287477 C462.046469,63.5418178 463.28005,63.1731935 464.267506,63.7059784 C465.255947,64.2387633 465.625135,65.47046 465.091535,66.45739 C464.557935,67.44432 463.325338,67.812944 462.336897,67.280159" id="Fill-337" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M50.5874627,45.4209385 C49.6167433,44.8979836 49.2544464,43.6879131 49.7782017,42.7176941 C50.301957,41.7484581 51.5138795,41.3867149 52.4855834,41.9096698 C53.4563028,42.4326247 53.8185997,43.6426952 53.2948444,44.6129142 C52.7710891,45.5831331 51.5581821,45.9448764 50.5874627,45.4209385" id="Fill-387" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M51.5417409,2.2804019 C51.2089791,2.1005133 51.0839473,1.6856882 51.2641112,1.3534349 C51.4432907,1.0201985 51.8597352,0.8963408 52.192497,1.0762294 C52.5262433,1.2561181 52.6502906,1.6709432 52.4701267,2.0031965 C52.2909473,2.3364328 51.8745027,2.4602906 51.5417409,2.2804019" id="Fill-556" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M1.56426282,39.1403679 C1.01983298,38.8474345 0.81702548,38.1681829 1.1104072,37.6245851 C1.40378893,37.0819702 2.0830956,36.8794726 2.62850995,37.172406 C3.17195529,37.4653394 3.37476279,38.143608 3.08138106,38.6872059 C2.78799934,39.2308037 2.10869267,39.4333013 1.56426282,39.1403679" id="Fill-564" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M454.843554,69.531716 C454.624994,69.413756 454.543281,69.141466 454.661421,68.922257 C454.779561,68.704032 455.053253,68.622443 455.271812,68.740402 C455.490372,68.858362 455.572086,69.131636 455.453945,69.349861 C455.335805,69.568087 455.063098,69.649676 454.843554,69.531716" id="Fill-580" style="fill: rgb(224, 226, 235);"></path>
                                                    </g>
                                                </g>
                                                <g id="step2" transform="translate(69.000000, 107.000000)">
                                                    <g id="left" transform="translate(0.922546, 0.665622)" stroke="#E0E2EB" stroke-width="0.1">
                                                        <path d="M106.973827,45.0778591 C93.7899637,46.8803448 81.6388953,49.0363291 70.804568,51.4851102 C45.1091601,57.2924849 30.268821,64.7609459 26.0761338,72.4443625 C24.9653792,74.4803715 24.6628363,76.5506592 25.1949685,78.6409427 C29.5457219,95.7281358 89.5629454,110.005194 171.146545,113.360217 L170.783923,113.435915 C131.992625,111.84053 96.3311814,107.31789 70.1007802,101.189867 C56.9984538,98.1290696 46.5317556,94.6833511 38.9910695,90.9484065 C31.2465425,87.1120542 26.9301202,82.7900877 25.8722928,78.6345155 C25.3566109,76.6092185 25.6620147,74.6110589 26.7262793,72.6593182 C32.8014559,61.5123296 63.662266,51.7736222 108.286276,45.9226851" id="Fill-72" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M38.9095331,56.8325083 C30.2373508,60.6645757 24.7729819,64.7958672 22.4835258,68.9914312 C21.3727712,71.0274402 21.0702283,73.0970137 21.6030758,75.1880114 C25.953114,92.2752045 85.9703374,106.552262 167.553937,109.907285 L167.19203,109.982984 C128.400733,108.387598 92.7385734,103.864245 66.5081722,97.7369361 C53.406561,94.6761383 42.9398628,91.2304198 35.3991768,87.4954752 C27.6539346,83.6584087 23.3382275,79.3371564 22.2796848,75.18087 C21.7640029,73.1562872 22.070122,71.1581276 23.1336713,69.2056728 C25.4209817,65.0093947 31.220079,61.0130753 39.9430427,57.3488304" id="Fill-74" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M9.67930791,52.8321898 C8.76238109,55.0331648 8.70730826,57.2941274 9.54198345,59.6015091 C16.3631466,78.4619082 81.9505932,95.8333284 169.0379,101.846375 L168.663118,101.918503 C127.254789,99.0590922 88.7932132,93.0796102 60.1431838,85.6104351 C45.8321161,81.8790611 34.2711136,77.80133 25.7806001,73.4907897 C17.0604973,69.0631307 11.9194131,64.1998472 10.2600759,59.6136494 C9.45186428,57.3783958 9.51909605,55.1959884 10.3974004,53.0857089 C18.3028545,34.1039063 92.101159,21.5336395 187.063165,23.1325958 C211.366732,23.5417971 235.39422,24.8950897 258.476172,27.1553382 L258.050609,25.0750526 C234.939333,22.8126617 209.816825,21.4086653 185.484649,20.9987498 C134.240455,20.1360704 87.9449486,24.0324103 54.4892785,31.1302334 C27.9441755,36.7626441 13.1431742,44.5274726 9.67930791,52.8321898" id="Fill-76" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M51.1460003,17.671364 C25.4505923,23.4780246 10.6102533,30.9471997 6.41756604,38.6306163 C5.30681147,40.6666253 5.0035533,42.7361988 5.53640079,44.8271965 C9.88643896,61.9143896 69.9043776,76.1914475 151.487262,79.5464704 L151.125355,79.6221691 C112.334058,78.0267836 76.6718984,73.5041441 50.4422124,67.3761212 C37.339886,64.3153234 26.8731878,60.8696049 19.3325018,57.1346603 C11.5879748,53.298308 7.27155248,48.9763415 6.21372505,44.8207693 C5.69804312,42.7954723 6.00344698,40.7965985 7.0677115,38.8448579 C10.6410082,32.2883519 22.7906462,26.2188883 41.2715138,21.1413632 C44.4492874,20.2686859 47.8137366,19.4245742 51.3548479,18.6125986" id="Fill-78" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M129.505694,54.7654342 C108.895582,56.9057075 90.2123041,59.8636693 74.3520449,63.4479307 C48.6559217,69.2553054 33.8162979,76.7237664 29.6236107,84.407183 C28.5128561,86.443192 28.2103132,88.5127655 28.7424454,90.6037632 C33.0931988,107.690956 93.109707,121.968014 174.694022,125.323037 L174.3314,125.398736 C135.540818,123.80335 99.8786582,119.279997 73.6482571,113.152688 C60.5459307,110.09189 50.0792324,106.645457 42.5385464,102.910513 C34.7940194,99.0748747 30.4775971,94.7529082 29.4197697,90.5966218 C28.9040877,88.572039 29.2094916,86.5738794 30.2737561,84.6214246 C37.1263894,72.0490154 75.5114352,62.0139405 129.612263,56.5807744" id="Fill-80" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M9.09925514,55.9325509 C7.98850057,57.969274 7.6852424,60.0388475 8.21808989,62.1291311 C12.5688433,79.2163241 72.5860667,93.4940962 154.169666,96.8484049 L153.807044,96.9241036 C115.015747,95.3287181 79.3543027,90.8060787 53.1239015,84.6780558 C40.0215751,81.6172579 29.5548769,78.1715394 22.0141909,74.4365948 C14.2696639,70.6002425 9.95324158,66.278276 8.89541415,62.1227038 C8.37973222,60.0974069 8.68513608,58.0992472 9.7494006,56.1467924 C19.3213442,38.5854113 90.4153583,24.5197385 179.925154,23.6420622 C202.834735,23.4178227 225.376687,24.0641181 246.926613,25.5630949 L246.754958,24.4161884 C225.178568,22.9157833 201.606683,22.2502062 178.671353,22.4751598 C130.368907,22.9486337 86.2126578,27.6540926 53.8276894,34.9732986 C28.1322814,40.7806733 13.2919424,48.2491342 9.09925514,55.9325509" id="Fill-86" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M173.940168,0.0135028254 C173.017519,0.0199300724 172.095586,0.0270714579 171.174368,0.036355259 C122.871921,0.509829118 78.7156725,5.21528802 46.330704,12.534494 C20.635296,18.3418687 5.79495699,25.8103297 1.60226976,33.4937463 C0.491515189,35.5297553 0.18825702,37.5993288 0.72110451,39.6903265 C5.07114268,56.7775196 65.0890813,71.0545775 146.671966,74.4096004 L146.310059,74.4852991 C107.518761,72.8899136 71.8566021,68.36656 45.6269162,62.2392512 C32.5245898,59.1784534 22.0578915,55.7320208 14.5172055,51.9970761 C6.77196326,48.161438 2.4562562,43.8394715 1.39842877,39.6831851 C0.882746836,37.6586023 1.1881507,35.6604427 2.25241522,33.7079879 C11.8236436,16.1466068 82.9183729,2.08093393 172.428884,1.20325765 C173.622605,1.19111729 174.815611,1.18183349 176.007902,1.17469211" id="Fill-88" style="fill: rgb(224, 226, 235);"></path>
                                                    </g>
                                                    <g id="right" transform="translate(104.426570, 38.222674)" stroke="#E0E2EB" stroke-width="0.1">
                                                        <path d="M151.612134,52.3922748 C164.867519,53.8634002 177.107277,55.7137332 188.042452,57.8889992 C213.978177,63.0479361 229.136079,70.140046 233.660634,77.7141995 C234.860077,79.7209288 235.252024,81.7819327 234.810726,83.8843566 C231.205959,101.070815 171.838166,116.843993 90.4405264,122.242881 L90.8052945,122.309295 C129.508618,119.741967 164.956208,114.327369 190.907669,107.545909 C203.870525,104.158036 214.182018,100.451657 221.556771,96.530322 C229.131072,92.5018665 233.256528,88.0742075 234.134117,83.8943545 C234.56111,81.8576314 234.169163,79.8687555 233.020502,77.9441521 C226.462544,66.9571305 195.193336,57.9982624 150.336876,53.2692369" id="Fill-112" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M235.25653,34.9840765 C244.091785,38.5961892 249.732101,42.5882237 252.202511,46.7230859 C253.401954,48.7305294 253.794616,50.7915333 253.353318,52.8939571 C249.748551,70.0804155 190.380758,85.8535936 108.982403,91.2524811 L109.347886,91.318896 C148.050495,88.752282 183.4988,83.3369694 209.450261,76.5555097 C222.413117,73.1676364 232.72461,69.4619715 240.099363,65.5399226 C247.672949,61.511467 251.79912,57.083808 252.675994,52.9046692 C253.102987,50.8679461 252.71104,48.8783561 251.563094,46.9537527 C249.09483,42.8181763 243.124077,38.9696837 234.245908,35.5261076" id="Fill-114" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M260.796017,31.7341747 C261.994745,33.7409041 262.388123,35.8019079 261.94611,37.9043318 C258.341343,55.0907901 198.97355,70.8639683 117.57591,76.2635699 L117.941393,76.3292706 C156.644002,73.7626567 192.091592,68.3480582 218.043053,61.5658844 C231.006624,58.1787252 241.318117,54.4723462 248.692154,50.5502972 C256.266456,46.5225558 260.392627,42.0948968 261.269501,37.9150439 C261.696494,35.8783207 261.304547,33.8887307 260.15517,31.9641274 C249.824366,14.654123 178.15316,2.37950965 88.6476555,3.74708497 C65.74022,4.097727 43.2368904,5.30890598 21.7627789,7.3470574 L21.8843683,6.1965802 C43.3849433,4.15628636 66.9167759,2.90011665 89.8506749,2.54947462 C138.149545,1.8117695 182.490324,5.40674296 215.177835,11.9089745 C241.112846,17.0679113 256.271463,24.1607354 260.796017,31.7341747" id="Fill-116" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M134.333427,47.0040995 C155.027936,48.6259081 173.830658,51.1132527 189.838255,54.2975965 C215.773981,59.4572475 230.932598,66.5493574 235.457152,74.1227967 C236.65588,76.1295261 237.048542,78.1905299 236.607245,80.2929538 C233.001762,97.4794122 173.634685,113.253304 92.2370449,118.652192 L92.6010978,118.717893 C131.305137,116.151279 166.752727,110.73668 192.703472,103.955221 C205.666329,100.567347 215.978537,96.8609682 223.353289,92.9389193 C230.927591,88.9111778 235.053047,84.4835188 235.92992,80.3036659 C236.357629,78.2669427 235.964967,76.2773527 234.816305,74.3527494 C227.420096,61.9603031 188.584454,52.1487536 134.273348,48.0760214" id="Fill-118" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M239.12815,60.740126 C240.327593,62.7468553 240.720255,64.8078592 240.278242,66.9109972 C236.673475,84.0967414 177.305682,99.8699195 95.9080423,105.269521 L96.2735256,105.335222 C134.976134,102.768608 170.423724,97.3540094 196.3759,90.5718356 C209.338756,87.1846765 219.650249,83.4782974 227.024286,79.5562485 C234.598588,75.5285071 238.724759,71.1008481 239.601633,66.9209951 C240.028626,64.884272 239.636679,62.894682 238.488017,60.9700786 C228.157213,43.6600743 156.516762,32.1303074 67.011258,33.4985969 C44.1038225,33.8492389 21.6004929,35.0597037 0.126381411,37.0985693 L0.216500584,35.2025314 C21.7170756,33.1622376 45.2489082,31.9060679 68.1828072,31.5554259 C116.482393,30.8177207 160.822456,34.4126942 193.509967,40.9149257 C219.445693,46.0738626 234.603595,53.1666867 239.12815,60.740126" id="Fill-124" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M89.6230883,0.473902342 C90.5457369,0.456763016 91.4683856,0.441051968 92.3888886,0.427483336 C140.688474,-0.310935925 185.028537,3.28475167 217.716049,9.78698317 C243.651775,14.9459201 258.810392,22.03803 263.334946,29.6114693 C264.533674,31.6181986 264.926336,33.6799166 264.484323,35.7816264 C260.879556,52.9680847 201.512478,68.741977 120.114124,74.1401503 L120.479607,74.2072793 C159.182931,71.6399512 194.630521,66.2253528 220.581982,59.4438931 C233.544838,56.0560198 243.856331,52.3496407 251.231083,48.4275918 C258.80467,44.3998504 262.93084,39.9721914 263.807714,35.7923385 C264.235423,33.7556153 263.842761,31.7660253 262.694099,29.8421361 C252.363295,12.5314176 180.691373,0.256804223 91.1865844,1.62509368 C89.9921477,1.64294715 88.7991415,1.66365717 87.6075658,1.6865096" id="Fill-126" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M82.9499066,20.3208838 C114.937206,20.9064775 144.09505,22.9974751 169.018008,26.4189129 C209.397835,31.9620563 233.374541,41.4743818 241.036817,52.7084954 C243.067359,55.6850248 243.861981,58.8193789 243.377769,62.0951325 C239.422539,88.8681867 149.547975,118.616342 24.8187487,134.435939 L25.3866425,134.504497 C84.6929258,126.982475 138.727237,115.379866 178.024919,102.551081 C197.654448,96.142402 213.174972,89.488059 224.158068,82.7744424 C235.437269,75.8780065 241.375121,68.685203 242.337823,62.1736877 C242.806299,59.0007701 242.018114,55.9763934 240.0734,53.1219816 C227.546835,34.7379129 166.889479,23.2274277 82.9563437,21.9755429" id="Fill-128" style="fill: rgb(224, 226, 235);"></path>
                                                    </g>
                                                    <g id="dots" transform="translate(2.922546, 15.665622)">
                                                        <path d="M22.6583303,4.061649 C21.6555757,3.5217602 21.2822249,2.2727319 21.8229399,1.2715096 C22.3636549,0.2710015 23.6145949,-0.1024929 24.6173495,0.4373958 C25.6193889,0.9772846 25.993455,2.227027 25.4527399,3.2275351 C24.9113097,4.2280432 23.6603697,4.6015377 22.6583303,4.061649" id="Fill-249" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M336.062774,59.3055504 C335.344681,58.9184873 335.07647,58.0236717 335.464125,57.3066766 C335.851781,56.5896815 336.747966,56.3218795 337.465343,56.7089426 C338.183435,57.0960057 338.451647,57.9908213 338.063992,58.7078164 C337.676336,59.4248115 336.780866,59.6926135 336.062774,59.3055504" id="Fill-337" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M36.9307755,43.4250372 C36.2255572,43.0451155 35.962352,42.166011 36.3428552,41.4611562 C36.7233584,40.7570156 37.6038084,40.4942126 38.3097419,40.8741343 C39.0149602,41.254056 39.2781654,42.1331606 38.8976622,42.8380153 C38.5171591,43.5428701 37.6359938,43.8056731 36.9307755,43.4250372" id="Fill-387" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M37.6240494,12.0838529 C37.3823012,11.9531656 37.2914668,11.6517991 37.4223542,11.4104203 C37.5525263,11.1683273 37.8550692,11.0783459 38.0968175,11.2090332 C38.339281,11.3397206 38.4294002,11.641087 38.2985128,11.8824659 C38.1683406,12.1245588 37.8657977,12.2145403 37.6240494,12.0838529" id="Fill-556" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M1.31589298,38.8622632 C0.92036994,38.6494499 0.77303225,38.1559802 0.98617124,37.7610616 C1.19931024,37.3668571 1.69282,37.2197445 2.08905826,37.4325578 C2.48386607,37.6453711 2.63120377,38.1381267 2.41806477,38.5330453 C2.20492577,38.927964 1.71141602,39.0750765 1.31589298,38.8622632" id="Fill-564" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M330.618932,60.9412848 C330.460151,60.8555881 330.400786,60.6577717 330.486614,60.4985189 C330.572442,60.3399801 330.771276,60.2807066 330.930058,60.3664032 C331.088839,60.4520998 331.148203,60.6506304 331.062376,60.8091691 C330.976548,60.9677079 330.778429,61.0269814 330.618932,60.9412848" id="Fill-580" style="fill: rgb(224, 226, 235);"></path>
                                                    </g>
                                                </g>
                                                <g id="step3" transform="translate(127.000000, 197.000000)">
                                                    <g id="left" transform="translate(0.232645, 0.973392)">
                                                        <path d="M17.0042657,53.8932193 C16.6253776,52.4048984 16.8375468,50.9231356 17.6302632,49.4700894 C20.6438599,43.9474454 31.2827216,38.8703136 48.4122484,34.9988989 C55.8541454,33.3168769 64.1826075,31.8409229 73.1921921,30.6091408 L73.2599215,31.1045322 C64.2640557,32.3344388 55.9496793,33.8078966 48.5224753,35.4865976 C35.9058186,38.3380663 26.84916,41.8434272 21.8244726,45.6505929 C29.9257809,39.6903361 48.5003965,34.5427329 74.0919581,31.1872609 L74.1569599,31.6830177 C43.2140213,35.7401427 22.5546583,42.4431484 18.514245,49.8566679 C17.8082267,51.1514255 17.6187153,52.4547326 17.9524561,53.765471 C18.6784235,56.6173636 21.7106331,59.5105573 26.8012496,62.0322604 C31.9434707,64.5792318 39.0909083,66.9322071 48.0424786,69.0233584 C59.2035732,71.6308459 72.6489802,73.7687039 87.2959194,75.2854394 C96.7455767,76.2639802 106.695341,76.9839876 116.854455,77.4047813 L117.143494,77.8474773 C107.191355,77.4382081 97.7088222,76.7925917 88.8264102,75.9409291 C73.5362136,74.4153807 59.5079868,72.2154275 47.9287337,69.5102486 C38.9427857,67.4110664 31.7607549,65.0467029 26.5793167,62.4803071 C21.3708588,59.9002296 18.2369768,56.9100249 17.4679124,53.8888302 L17.0042657,53.8932193 Z M119.177166,77.4306869 L117.164038,77.3478995 C106.630874,76.9147364 96.621993,76.2143797 87.2959257,75.2854401 C47.3042266,71.3019947 19.8683493,63.1152895 17.4888082,53.769856 C17.1418598,52.4069982 17.3355456,51.0543214 18.0691809,49.7095706 C18.8219684,48.3300305 20.0803124,46.9721293 21.8244726,45.6505929 C20.0743178,46.9382087 18.8129577,48.2637514 18.0752406,49.6173455 C17.3103912,51.0199927 17.1021296,52.4522492 17.4679124,53.8888302 C18.2369768,56.9100249 21.3708588,59.9002296 26.5793167,62.4803071 C31.7607549,65.0467029 38.9427857,67.4110664 47.9287337,69.5102486 C59.5079868,72.2154275 73.5362136,74.4153807 88.8264101,75.9409291 C97.7088222,76.7925917 107.191355,77.4382081 117.143494,77.8474773 L117.102679,77.3529638 C106.695341,76.9839876 96.7455767,76.2639802 87.2959309,75.2854406 L119.177166,77.4306869 Z M117.204853,77.842413 L119.177166,77.4306869 L117.164038,77.3478995 C106.630874,76.9147364 96.621993,76.2143797 87.2959257,75.2854401 C47.3042266,71.3019947 19.8683493,63.1152895 17.4888082,53.769856 C17.1418598,52.4069982 17.3355456,51.0543214 18.0691809,49.7095706 C18.8219684,48.3300305 20.0803124,46.9721293 21.8244726,45.6505929 C20.0743178,46.9382087 18.8129577,48.2637514 18.0752406,49.6173455 C17.3103912,51.0199927 17.1021296,52.4522492 17.4679124,53.8888302 C18.2369768,56.9100249 21.3708588,59.9002296 26.5793167,62.4803071 C31.7607549,65.0467029 38.9427857,67.4110664 47.9287337,69.5102486 C59.5079868,72.2154275 73.5362136,74.4153807 88.8264101,75.9409291 C97.7088222,76.7925917 107.191355,77.4382081 117.143494,77.8474773 L117.102679,77.3529638 C106.695341,76.9839876 96.7455767,76.2639802 87.2959309,75.2854406 L117.204853,77.842413 Z M116.895269,77.8992947 L117.204853,77.842413 L119.177166,77.4306869 L117.164038,77.3478995 C106.630874,76.9147364 96.621993,76.2143797 87.2959257,75.2854401 C47.3042266,71.3019947 19.8683493,63.1152895 17.4888082,53.769856 C17.1418598,52.4069982 17.3355456,51.0543214 18.0691809,49.7095706 C18.8219684,48.3300305 20.0803124,46.9721293 21.8244726,45.6505929 C20.0743178,46.9382087 18.8129577,48.2637514 18.0752406,49.6173455 C17.3103912,51.0199927 17.1021296,52.4522492 17.4679124,53.8888302 C18.2369768,56.9100249 21.3708588,59.9002296 26.5793167,62.4803071 C31.7607549,65.0467029 38.9427857,67.4110664 47.9287337,69.5102486 C59.5079868,72.2154275 73.5362136,74.4153807 88.8264101,75.9409291 C97.7088222,76.7925917 107.191355,77.4382081 117.143494,77.8474773 L117.102679,77.3529638 C106.695341,76.9839876 96.7455767,76.2639802 87.2959309,75.2854406 L116.895269,77.8992947 Z M87.2959309,75.2854406 C96.7455767,76.2639802 106.695341,76.9839876 116.854455,77.4047813 L117.143494,77.8474773 C107.191355,77.4382081 97.7088222,76.7925917 88.8264102,75.9409291 C73.5362136,74.4153807 59.5079868,72.2154275 47.9287337,69.5102486 C38.9427857,67.4110664 31.7607549,65.0467029 26.5793167,62.4803071 C21.3708588,59.9002296 18.2369768,56.9100249 17.4679124,53.8888302 C17.1021296,52.4522492 17.3103912,51.0199927 18.0752406,49.6173455 C18.8129577,48.2637514 20.0743178,46.9382087 21.8244726,45.6505929 C20.0803124,46.9721293 18.8219684,48.3300305 18.0691809,49.7095706 C17.3355456,51.0543214 17.1418598,52.4069982 17.4888082,53.769856 C19.8683493,63.1152895 47.3042266,71.3019947 87.2959193,75.2854394 C96.621993,76.2143797 106.630874,76.9147364 117.164038,77.3478995 L119.177166,77.4306869 L117.204853,77.842413 L116.895269,77.8992947 C107.263147,77.5031505 97.82618,76.8388627 88.8264174,75.9409298 L87.2959309,75.2854406 Z" id="Fill-72" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M114.745727,75.4787814 L116.714689,75.0669418 L114.704815,74.9842882 C104.187085,74.5517598 94.1921063,73.8528206 84.8777159,72.925912 C70.2155629,71.4088983 56.7553401,69.2694979 45.5832491,66.6597456 C36.6319645,64.5685469 29.4846159,62.2155605 24.342516,59.6686492 C19.2513982,57.1464612 16.2196889,54.2537449 15.4932254,51.4013417 C15.1596095,50.0915557 15.3492395,48.7882369 16.055074,47.492472 C17.5658867,44.7207492 21.4301204,42.0111308 27.4387575,39.4870898 L27.2451143,39.02611 C24.2405689,40.2882259 21.7586913,41.5996185 19.8246725,42.9513002 C21.6471854,41.6393306 23.9556157,40.3602205 26.7355196,39.1318355 L26.53343,38.6744955 C20.6248442,41.2853838 16.7904308,44.1388485 15.1710405,47.1064773 C14.3781975,48.5597555 14.1662047,50.0410532 14.5455469,51.529666 C17.0307475,61.2916731 45.2960439,69.6430867 86.4081402,73.5812431 C95.2787424,74.4309642 104.747423,75.0752256 114.684271,75.483866 L114.643359,74.9893728 C104.251447,74.6210055 94.3155831,73.9023971 84.8777232,72.9259127 L114.745727,75.4787814 Z M114.436536,75.5356834 L114.745727,75.4787814 L116.714689,75.0669418 L114.704815,74.9842882 C104.187085,74.5517598 94.1921063,73.8528206 84.8777159,72.925912 C70.2155629,71.4088983 56.7553401,69.2694979 45.5832491,66.6597456 C36.6319645,64.5685469 29.4846159,62.2155605 24.342516,59.6686492 C19.2513982,57.1464612 16.2196889,54.2537449 15.4932254,51.4013417 C15.1596095,50.0915557 15.3492395,48.7882369 16.055074,47.492472 C17.5658867,44.7207492 21.4301204,42.0111308 27.4387575,39.4870898 L27.2451143,39.02611 C24.2405689,40.2882259 21.7586913,41.5996185 19.8246725,42.9513002 C21.6471854,41.6393306 23.9556157,40.3602205 26.7355196,39.1318355 L26.53343,38.6744955 C20.6248442,41.2853838 16.7904308,44.1388485 15.1710405,47.1064773 C14.3781975,48.5597555 14.1662047,50.0410532 14.5455469,51.529666 C17.0307475,61.2916731 45.2960439,69.6430867 86.4081402,73.5812431 C95.2787424,74.4309642 104.747423,75.0752256 114.684271,75.483866 L114.643359,74.9893728 C104.251447,74.6210055 94.3155831,73.9023971 84.8777232,72.9259127 L114.436536,75.5356834 Z M86.4081502,73.5812441 C95.3960177,74.4772071 104.81918,75.1401465 114.436536,75.5356834 L114.745727,75.4787814 L116.714689,75.0669418 L114.704815,74.9842882 C104.187085,74.5517598 94.1921063,73.8528206 84.8777159,72.925912 C70.2155629,71.4088983 56.7553401,69.2694979 45.5832491,66.6597456 C36.6319645,64.5685469 29.4846159,62.2155605 24.342516,59.6686492 C19.2513982,57.1464612 16.2196889,54.2537449 15.4932254,51.4013417 C15.1596095,50.0915557 15.3492395,48.7882369 16.055074,47.492472 C17.5658867,44.7207492 21.4301204,42.0111308 27.4387575,39.4870898 L27.2451143,39.02611 C24.2405689,40.2882259 21.7586913,41.5996185 19.8246725,42.9513002 C21.6471854,41.6393306 23.9556157,40.3602205 26.7355196,39.1318355 L26.53343,38.6744955 C20.6248442,41.2853838 16.7904308,44.1388485 15.1710405,47.1064773 C14.3781975,48.5597555 14.1662047,50.0410532 14.5455469,51.529666 C17.0307475,61.2916731 45.2960439,69.6430867 86.4081402,73.5812431 C95.2787424,74.4309642 104.747423,75.0752256 114.684271,75.483866 L114.643359,74.9893728 C104.251447,74.6210055 94.3155831,73.9023971 84.8777232,72.9259127 L86.4081502,73.5812441 Z" id="Fill-74" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M6.62570998,36.164855 C5.99805238,37.6714733 5.96035376,39.2191545 6.53170822,40.7986105 C11.200952,53.7089896 56.0970752,65.6001283 115.710349,69.7161977 L115.453803,69.765571 C87.1088442,67.8082382 60.7810067,63.7151445 41.1693994,58.6023215 C31.3731428,56.0481098 23.4593694,53.2568086 17.6474167,50.3061442 C11.6783047,47.2753092 8.15911374,43.9462793 7.0232591,40.8069209 C6.47001956,39.2768381 6.51604126,37.7829298 7.11726087,36.3383948 C12.5287269,23.3449121 63.0453721,14.740274 128.04906,15.8347965 C144.685413,16.1149043 161.132784,17.0412643 176.932914,18.5884566 L176.641606,17.1644531 C160.821403,15.6157942 143.624466,14.6547262 126.968529,14.3741296 C91.8906943,13.7836056 60.20035,16.4507403 37.2991706,21.3093643 C19.128434,25.1648796 8.99680653,30.4800841 6.62570998,36.164855" id="Fill-76" stroke="#E0E2EB" stroke-width="0.5" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M35.0106193,12.0964571 C17.4215187,16.0712505 7.2629636,21.1840735 4.39297228,26.4435497 C3.63263511,27.837245 3.42504788,29.2539159 3.7897943,30.6852521 C6.76749597,42.3818307 47.8511622,52.1548068 103.696532,54.4513976 L103.448798,54.5032151 C76.8952589,53.4111368 52.4836866,50.3152856 34.5288603,46.1205123 C25.560015,44.0253254 18.3953182,41.6666514 13.2335443,39.1099955 C7.93223657,36.4839237 4.97754574,33.5254377 4.25343842,30.6808525 C3.90044221,29.29449 4.10949822,27.9262146 4.83801188,26.590203 C7.28401608,22.1021231 15.6007241,17.9474351 28.2513051,14.4717518 C30.4265646,13.874384 32.7296078,13.29657 35.1535803,12.740754" id="Fill-78" stroke="#E0E2EB" stroke-width="0.5" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M88.6496408,37.4882055 C74.5415429,38.953272 61.7524072,40.9780653 50.8956932,43.4315751 C33.306103,47.4068573 23.1480375,52.5191914 20.2780462,57.7786677 C19.517709,59.1723629 19.3106114,60.5890338 19.6748682,62.0203701 C22.6530595,73.7169487 63.7357465,83.4899248 119.582096,85.7865155 L119.333872,85.838333 C92.7808224,84.7462547 68.3692501,81.6499147 50.4139342,77.4556302 C41.4450889,75.3604433 34.2803921,73.0012805 29.1186182,70.4446246 C23.8173105,67.8190417 20.8626197,64.8605557 20.1385123,62.0154816 C19.7855161,60.6296079 19.9945721,59.2618214 20.7230858,57.925321 C25.4138717,49.3192163 51.6893227,42.4499756 88.7225901,38.7308479" id="Fill-80" stroke="#E0E2EB" stroke-width="0.5" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M6.22865045,38.2871238 C5.46831328,39.6813079 5.26072605,41.0979788 5.62547248,42.5288262 C8.60366374,54.2254048 49.6868404,63.9988698 105.5327,66.2949717 L105.284477,66.3467892 C78.7309371,65.2547109 54.3198543,62.1588597 36.3645385,57.9640864 C27.3956932,55.8688995 20.2309964,53.5102255 15.0692225,50.9535696 C9.76791474,48.3274978 6.81322392,45.3690118 6.08911659,42.5244266 C5.73612038,41.1380641 5.94517639,39.7702776 6.67369005,38.4337771 C13.2259067,26.4126059 61.8914026,16.7843277 123.162926,16.183538 C138.845064,16.0300409 154.275552,16.472445 169.026974,17.4985293 L168.909472,16.7134453 C154.139934,15.6863833 138.004434,15.2307804 122.304671,15.3847664 C89.24053,15.7088702 59.0145569,18.9298655 36.8462975,23.9400312 C19.2571968,27.9153134 9.09864177,33.0276476 6.22865045,38.2871238" id="Fill-86" stroke="#E0E2EB" stroke-width="0.5" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M119.066065,0.00924299611 C118.434491,0.0136425952 117.803406,0.0185310386 117.172811,0.0248860151 C84.1086702,0.348989815 53.8826972,3.56998519 31.7144377,8.58015087 C14.1253371,12.5554331 3.966782,17.6677672 1.09679068,22.9272435 C0.336453507,24.3209387 0.128866281,25.7376096 0.493612704,27.1689459 C3.47131437,38.8655245 44.5549806,48.6385006 100.400351,50.9350913 L100.152617,50.9869088 C73.5990773,49.8948305 49.187505,46.7984905 31.2326787,42.604206 C22.2638334,40.5090192 15.0991366,38.1498564 9.93736271,35.5932004 C4.63556537,32.9676175 1.68136414,30.0091315 0.957256815,27.1640574 C0.604260612,25.7781837 0.813316616,24.4103972 1.54183028,23.0738968 C8.0935573,11.0527256 56.7595428,1.42444737 118.031556,0.823657672 C118.848686,0.815347318 119.665327,0.808992342 120.481477,0.804103898" id="Fill-88" stroke="#E0E2EB" stroke-width="0.5" style="fill: rgb(224, 226, 235);"></path>
                                                    </g>
                                                    <g id="right" transform="translate(71.083542, 26.682062)" stroke="#E0E2EB" stroke-width="0.5">
                                                        <path d="M103.78201,35.8637231 C112.855628,36.8707424 121.234024,38.1373381 128.719404,39.626358 C146.473008,43.1577695 156.848942,48.0124827 159.946104,53.1971658 C160.767151,54.5708184 161.035448,55.9816232 160.733369,57.4207809 C158.265823,69.1853089 117.627196,79.9824139 61.9086307,83.6780771 L62.1583229,83.7235396 C88.6516426,81.9661442 112.916337,78.2597264 130.680712,73.6176605 C139.554087,71.298583 146.612542,68.7614808 151.66073,66.0772365 C156.845515,63.3196656 159.669485,60.2888307 160.270215,57.4276247 C160.562501,56.0334407 160.294205,54.6720092 159.507919,53.3545737 C155.018845,45.8337034 133.61435,39.7011512 102.909066,36.4640239" id="Fill-112" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M161.038532,23.9474089 C167.086469,26.4199836 170.947395,29.1526235 172.638448,31.9830322 C173.459494,33.3571737 173.728281,34.7679785 173.426202,36.2071362 C170.958656,47.9716642 130.320029,58.7687692 74.6009739,62.4644324 L74.8511557,62.5098949 C101.343986,60.7529884 125.60917,57.0460817 143.373545,52.4040158 C152.246919,50.0849383 159.305375,47.548325 164.353563,44.8635918 C169.537858,42.1060209 172.362317,39.075186 172.962558,36.2144689 C173.254845,34.8202848 172.986548,33.4583645 172.200752,32.140929 C170.511168,29.3100314 166.42405,26.6756492 160.346738,24.3184418" id="Fill-114" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M178.520902,21.7227761 C179.341459,23.0964287 179.610735,24.5072335 179.308166,25.9463912 C176.84062,37.7109192 136.201994,48.5080242 80.4834279,52.2041763 L80.7336097,52.2491499 C107.22644,50.4922434 131.491134,46.7858256 149.255509,42.1432708 C158.129374,39.8246821 165.187829,37.28758 170.235528,34.6028468 C175.420312,31.8457647 178.244772,28.8149298 178.845012,25.9537239 C179.137299,24.5595398 178.869002,23.1976195 178.082227,21.880184 C171.010553,10.0310859 121.949956,1.62882935 60.6813691,2.56496627 C45.0007001,2.80498884 29.5966508,3.63406885 14.8971252,5.0292306 L14.9803559,4.24170237 C29.6979965,2.84507408 45.8060798,1.98519688 61.5048637,1.74517431 C94.5665566,1.2401981 124.918845,3.70104052 147.294202,8.15196827 C165.047316,11.6833798 175.423739,16.5385818 178.520902,21.7227761" id="Fill-116" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M91.9543357,32.1753925 C106.120206,33.285558 118.991103,34.9882028 129.948673,37.1679598 C147.702277,40.6998601 158.078701,45.5545733 161.175863,50.7387676 C161.99642,52.1124202 162.265206,53.523225 161.963128,54.9623827 C159.495092,66.7269107 118.856955,77.5245045 63.1383892,81.2201678 L63.3875917,81.2651414 C89.881401,79.5082349 114.146096,75.8018171 131.909981,71.1597512 C140.783356,68.8406736 147.8423,66.3035715 152.890489,63.6188383 C158.075274,60.8617562 160.899243,57.8309213 161.499484,54.9697154 C161.79226,53.5755313 161.523474,52.213611 160.737188,50.8961755 C155.674312,42.4132596 129.090418,35.6970271 91.91321,32.9091478" id="Fill-118" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M163.688745,41.5780201 C164.509792,42.9516727 164.778578,44.3624775 164.47601,45.8021241 C162.008464,57.5661632 121.369837,68.3632682 65.6512717,72.0594203 L65.9014535,72.104394 C92.3942836,70.3474874 116.658978,66.6410696 134.423843,61.9985149 C143.297217,59.6799262 150.355673,57.142824 155.403371,54.4580909 C160.588156,51.7010088 163.412615,48.6701739 164.012856,45.8089679 C164.305142,44.4147838 164.036846,43.0528635 163.25056,41.735428 C156.178886,29.88633 107.139341,21.993938 45.8707549,22.9305638 C30.1900859,23.1705864 14.7860366,23.9991775 0.0865109972,25.3948281 L0.148199654,24.0969464 C14.8658402,22.7003181 30.9739235,21.8404409 46.6727075,21.6004183 C79.7348899,21.0954421 110.086688,23.5562846 132.462045,28.0072123 C150.215649,31.5386238 160.591583,36.3938259 163.688745,41.5780201" id="Fill-124" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M61.3490754,0.324397106 C61.9806497,0.312664842 62.6122241,0.301910266 63.2423296,0.292622224 C96.3045121,-0.212842827 126.65631,2.24848844 149.031668,6.69941619 C166.785271,10.2308277 177.161695,15.0855409 180.258857,20.2697352 C181.079414,21.6433878 181.348201,23.0546814 181.045632,24.4933503 C178.578086,36.2578783 137.939949,47.0554721 82.2208939,50.7506465 L82.4710756,50.7965979 C108.964395,49.0392025 133.22909,45.3327846 150.993465,40.6907188 C159.866839,38.3716412 166.925295,35.8345391 171.973483,33.1498059 C177.157778,30.3927238 179.982237,27.3618889 180.582478,24.500683 C180.875254,23.1064989 180.606468,21.7445785 179.820182,20.4276319 C172.748508,8.578045 123.687421,0.175788426 62.4193246,1.11241419 C61.6017051,1.1246353 60.7850648,1.13881178 59.9694037,1.1544548" id="Fill-126" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M56.7811283,13.9101147 C78.6771743,14.310967 98.6363923,15.7423033 115.696733,18.0843565 C143.337658,21.8787663 159.750267,28.390173 164.995272,36.0801833 C166.385225,38.1176866 166.929162,40.2632244 166.597708,42.5055534 C163.890262,60.8323278 102.369046,81.1956278 16.9890071,92.0245077 L17.3777436,92.0714367 C57.9742652,86.9224393 94.9620005,78.9801852 121.862172,70.1985854 C135.299038,65.8116963 145.923196,61.2566447 153.441378,56.661019 C161.162252,51.9402492 165.226849,47.016609 165.885841,42.5593263 C166.206524,40.3873908 165.666993,38.317135 164.335791,36.3632242 C155.761068,23.7789043 114.239706,15.8997112 56.7855346,15.042767" id="Fill-128" style="fill: rgb(224, 226, 235);"></path>
                                                    </g>
                                                    <g id="dots" transform="translate(1.232645, 10.973392)">
                                                        <path d="M15.879197,3.0481393 C15.1927883,2.678573 14.937221,1.8235842 15.307353,1.1382244 C15.6774849,0.4533535 16.5337822,0.1976879 17.2201909,0.5672542 C17.9061101,0.9368206 18.1621669,1.7922982 17.792035,2.4771691 C17.4214135,3.16204 16.5651162,3.4177056 15.879197,3.0481393" id="Fill-249" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M230.411782,40.8638667 C229.920231,40.5989131 229.736634,39.9863911 230.001993,39.4955914 C230.267353,39.0047916 230.880812,38.821475 231.371873,39.0864287 C231.863424,39.3513823 232.047021,39.9639043 231.781662,40.454704 C231.516303,40.9455037 230.903333,41.1288203 230.411782,40.8638667" id="Fill-337" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M25.6490157,29.9932884 C25.1662774,29.7332232 24.9861074,29.1314558 25.2465706,28.6489664 C25.5070338,28.1669659 26.1097222,27.9870712 26.59295,28.2471364 C27.0756882,28.5072016 27.2558583,29.108969 26.9953951,29.5914583 C26.7349319,30.0739477 26.1317539,30.2538424 25.6490157,29.9932884" id="Fill-387" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M26.1235777,8.5395233 C25.9580954,8.4500648 25.8959172,8.2437725 25.9855126,8.0785431 C26.0746184,7.9128249 26.2817161,7.8512305 26.4471983,7.940689 C26.6131702,8.0301475 26.6748589,8.2364398 26.5852634,8.4016692 C26.4961576,8.5673875 26.28906,8.6289818 26.1235777,8.5395233" id="Fill-556" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M1.26980539,26.8699641 C0.99906073,26.7242885 0.89820467,26.386497 1.04410324,26.1161661 C1.19000181,25.846324 1.52782064,25.7456221 1.79905489,25.8912977 C2.06930996,26.0369733 2.17016602,26.3742759 2.02426745,26.6446068 C1.87836888,26.9149378 1.54055005,27.0156397 1.26980539,26.8699641" id="Fill-564" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M226.685347,41.9835647 C226.576657,41.9249033 226.536021,41.7894935 226.594772,41.6804812 C226.653523,41.5719577 226.78963,41.5313836 226.89832,41.590045 C227.007009,41.6487063 227.047645,41.784605 226.988894,41.8931285 C226.930143,42.0016519 226.794526,42.042226 226.685347,41.9835647" id="Fill-580" style="fill: rgb(224, 226, 235);"></path>
                                                    </g>
                                                </g>
                                                <g id="step4" transform="translate(176.000000, 276.000000)">
                                                    <g id="left" transform="translate(0.358315, 0.796682)">
                                                        <path d="M55.2009472,47.1419421 C60.5514046,47.6454142 66.2496539,48.0280389 72.2230384,48.2736868 L72.2405012,47.8490457 L72.1536532,47.4330139 L72.0874392,47.8809979 L72.1049036,47.4563569 C65.7882635,47.1965698 59.6085106,46.7494295 53.7464226,46.1407872 C44.763542,45.2081227 36.5265873,43.8962266 29.6860389,42.2981177 C24.1848543,41.0130014 19.79627,39.5682556 16.6466609,38.0082363 C13.5714737,36.4849023 11.7567974,34.7534224 11.3324911,33.0865768 C11.1440514,32.3464955 11.250753,31.6126876 11.6542227,30.8727696 C14.0849373,26.4127928 26.7782822,22.2943908 45.7626005,19.8052363 L45.6520975,18.9624499 C45.4854233,18.9843036 45.3192307,19.0062795 45.1535223,19.028377 C26.145312,21.5631557 13.5065953,25.6977549 10.9079154,30.4659216 C10.4044328,31.3892518 10.2658557,32.3422737 10.508767,33.2962873 C11.006338,35.2509466 12.9938575,37.1473454 16.269375,38.7699156 C19.4856531,40.3629564 23.9330458,41.8270623 29.4926726,43.1258311 C36.7593243,44.8234875 45.5846663,46.1986092 55.20095,47.1419423 C60.6186708,47.6734071 66.2874433,48.0678184 72.0699748,48.3056389 L75.6802759,47.5651429 L72.257964,47.4244046 C65.7278277,47.155861 59.5234853,46.7194165 53.7464226,46.1407872 C29.2762374,43.689859 12.4721199,38.687913 11.0465881,33.0892648 C10.8499521,32.3168533 10.9593723,31.5526764 11.3797566,30.7821136 C13.1313067,27.5722654 19.5720538,24.4985793 29.9801102,22.1462724 C34.5519877,21.112934 39.6712496,20.2057082 45.2109374,19.4483272 L45.0957974,18.6061616 C39.5327876,19.3667312 34.38958,20.2782006 29.7927244,21.3171847 C19.157889,23.7207454 12.5417698,26.8781235 10.6335965,30.3749956 C10.1127743,31.3296606 9.97193226,32.3132837 10.2228657,33.2989824 C11.8045738,39.5109989 29.4859594,44.7221903 55.2009472,47.1419421 Z" id="Fill-72" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M53.7084955,45.6867251 C59.051995,46.1890838 64.7421433,46.5709326 70.7066081,46.8162137 L70.7240709,46.3915726 L70.6370584,45.9755752 L70.5713108,46.4235248 L70.5887752,45.9988838 C64.2824774,45.7395221 58.1123647,45.2933639 52.2584496,44.6861627 C43.264925,43.7533033 35.0177213,42.4403342 28.1695981,40.8406421 C22.6685983,39.5554988 18.2800675,38.1107458 15.1305325,36.5507632 C12.0550325,35.0271314 10.2406544,33.295936 9.81604824,31.6287525 C9.62769459,30.8892704 9.73446927,30.1554125 10.1378812,29.4148323 C11.0329241,27.772795 13.3710152,26.1333149 17.0244487,24.5986215 L16.6952553,23.8149559 C16.5965581,23.8564155 16.4987556,23.8979655 16.40185,23.9396055 C12.7524255,25.5077515 10.3751319,27.2035579 9.39149682,29.0081257 C8.88814589,29.9321729 8.74949555,30.885111 8.99234559,31.8385475 C9.49023891,33.7934873 11.477428,35.6895709 14.7532324,37.3124355 C17.969448,38.9054453 22.4167832,40.3695574 27.9762374,41.6683569 C35.2493204,43.3673177 44.0833821,44.7433118 53.7084986,45.6867254 C59.1192249,46.2170621 64.7799314,46.6106997 70.5538464,46.8481658 L74.158319,46.1074425 L70.7415337,45.9669315 C64.2221153,45.6988286 58.0274247,45.2633757 52.2584496,44.6861627 C27.7728288,42.2362613 10.9563085,37.2326054 9.5304638,31.6318079 C9.33356781,30.8591494 9.4428828,30.0953119 9.86332629,29.3246405 C10.788181,27.6297893 13.0578725,25.9407475 16.5953865,24.3775894 L16.2518343,23.6001116 C12.5589021,25.2319459 10.1501919,27.0244416 9.11716692,28.9175211 C8.59625752,29.8723461 8.45552595,30.8557068 8.70676245,31.841608 C10.2886792,38.0554655 27.9804922,43.2679571 53.7084955,45.6867251 Z" id="Fill-74" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M78.9660186,9.33926732 C89.0355605,9.50880912 98.992421,10.061883 108.570392,10.9835818 L108.568983,10.9766926 C98.9380057,10.0432658 88.4635188,9.45992294 78.2854228,9.28845617 C71.4555097,9.1734768 64.7760339,9.25886888 58.3747994,9.53111815 C64.8804687,9.28793709 71.7798627,9.21826789 78.9660186,9.33926732 Z M4.47785795,22.4639318 C5.91691312,19.0137563 12.3486383,15.8344379 23.0879931,13.5557329 C32.8628699,11.4819336 45.012631,10.0994212 58.3747983,9.5311182 C28.4305751,10.6504302 6.82767761,15.4457058 3.99638069,22.2439388 C3.99486897,22.247571 4.15536139,22.320902 4.47785795,22.4639318 Z M3.69336225,22.1367216 C5.27793706,18.3376587 11.9125656,15.0580415 22.9115762,12.724242 C37.4372717,9.64252778 57.1527687,8.08257436 78.2997353,8.43857668 C88.6104321,8.61227733 99.224238,9.20770926 108.963881,10.1611348 L109.275999,10.1916884 L109.633917,11.9413054 L109.060685,11.8851729 C99.3301114,10.9323282 89.1996365,10.3616922 78.9517088,10.1891469 C39.1403379,9.51880888 8.01610717,14.8030299 4.7810873,22.5706429 C4.44762141,23.3718529 4.42856302,24.1833914 4.7303967,25.0181664 C5.40982874,26.8960305 7.58662735,28.8704232 11.074316,30.6413078 C14.6276775,32.4453104 19.4802822,34.1568376 25.4934933,35.7246828 C33.0487784,37.6943753 42.0818774,39.3840733 51.866549,40.6692054 C58.0773106,41.4849353 64.5908807,42.1376656 71.2215315,42.5955385 L71.1922533,43.0195288 L71.1119345,42.6021873 L71.3504475,42.9890838 L71.3211726,43.4130743 C64.9022773,42.9698741 58.7610687,42.3819764 52.9791005,41.6700323 C25.4614434,38.2817335 6.0806459,32.083759 3.62799093,25.30221 C3.24101638,24.232455 3.2636935,23.1680908 3.69329316,22.1368874 L4.47792704,22.463766 C4.13039197,23.2979831 4.11239787,24.1425472 4.42731035,25.0130933 C6.69443506,31.2816543 25.3465967,37.2847276 51.866549,40.6692054 C57.9868043,41.4502727 64.5260909,42.0918761 71.3797224,42.5650933 L74.6346254,42.7898319 L71.2725721,43.4368703 C64.949191,43.0144325 58.8374148,42.4146394 52.9791005,41.6700323 C42.6905111,40.3623255 33.1836916,38.6079587 25.2790509,36.5471878 C19.2115587,34.9651897 14.3041775,33.2343427 10.6895101,31.3992158 C7.02743919,29.539788 4.6979932,27.4269416 3.93107457,25.3072756 C3.55903227,24.2783262 3.58094729,23.2527737 3.98959618,22.2603273 C3.9895999,22.2603182 3.89085526,22.2191164 3.69336225,22.1367216 Z" id="Fill-76" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M46.9062309,32.8695624 C52.2557009,33.3728794 57.9527978,33.7553968 63.9249071,34.0009945 L63.9423701,33.5763534 L63.8553576,33.160356 L63.7896101,33.6083056 L63.8070745,33.1836646 C57.4914741,32.9239203 51.3126956,32.4768855 45.4514729,31.8684025 C36.4673222,30.9357126 28.2292726,29.6236981 21.3882123,28.025426 C15.8870251,26.740309 11.4984407,25.2955633 8.34883173,23.735544 C5.27364453,22.21221 3.45896822,20.4807301 3.03466202,18.8138845 C2.84621439,18.0737724 2.95294069,17.339663 3.35639365,16.5997759 C4.7645162,14.0160656 9.69656676,11.4866 17.5331904,9.33352715 C18.8708615,8.96617713 20.285393,8.61139452 21.7717318,8.27057758 L21.5817574,7.4420791 C20.0839047,7.78553617 18.6576857,8.14325018 17.3080497,8.51388597 C9.25665547,10.7259659 4.16695844,13.3362825 2.61008533,16.1929296 C2.10661715,17.1162333 1.96802077,18.0695588 2.21093788,19.0235951 C2.70850886,20.9782543 4.69602826,22.874653 7.97154581,24.4972233 C11.1878238,26.0902641 15.6352166,27.5543699 21.1948435,28.8531388 C28.4620417,30.5509692 37.288582,31.926214 46.9062339,32.8695626 C52.3230074,33.4008676 57.9907274,33.7951719 63.7721457,34.0329466 L67.3766115,33.2922243 L63.9598331,33.1517123 C57.4309012,32.8832159 51.2276359,32.4468783 45.4514729,31.8684025 C20.9796379,29.4175776 4.17411017,24.4154318 2.74876305,18.8165887 C2.55192181,18.0441452 2.66136979,17.2803019 3.08192743,16.5094213 C4.83347873,13.299571 11.2739956,10.2258041 21.6807783,7.87391977 L21.4949111,7.0444888 C10.8604716,9.44766451 4.24402391,12.6052787 2.33576729,16.1023033 C1.81476685,17.0572952 1.67386617,18.0406485 1.9250617,19.0263888 C3.50655008,25.2385636 21.1894527,30.4499407 46.9062309,32.8695624 Z" id="Fill-78" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M56.7224604,52.193696 C62.0659124,52.6960442 67.7560073,53.0778847 73.720419,53.3231615 L73.7378817,52.8985204 L73.6510336,52.4824887 L73.5848198,52.9304726 L73.6022845,52.5058316 C67.2965009,52.2464863 61.126849,51.8003684 55.2733212,51.1932276 C46.2794065,50.260359 38.0318023,48.9473451 31.1834089,47.3475899 C25.6830188,46.0626592 21.2941064,44.6176546 18.1440414,43.0574095 C15.0688247,41.5343445 13.2541815,39.8028962 12.829876,38.1357669 C12.6414347,37.3959407 12.7481376,36.6621242 13.1516346,35.9218855 C15.8646744,30.9443178 31.3930098,26.6512785 54.7514591,24.3054672 L54.6665237,23.4597214 C30.9697337,25.8395111 15.3209142,30.16586 12.4053034,35.5150813 C11.9018081,36.4387724 11.763234,37.3917745 12.0061564,38.3454953 C12.5037084,40.3004158 14.49123,42.1968167 17.7667837,43.8191028 C20.983479,45.4123502 25.4312076,46.8767195 30.9900532,48.1753058 C38.2631738,49.8742755 47.0973148,51.25028 56.7224635,52.1936963 C62.1330673,52.7240207 67.7936235,53.1176506 73.5673551,53.3551136 L77.1776595,52.6146165 L73.7553444,52.4738793 C67.2363199,52.205795 61.041991,51.770383 55.2733211,51.1932276 C30.7870503,48.7433763 13.9700804,43.7396656 12.5439686,38.1387395 C12.3473385,37.3660876 12.456727,36.6021985 12.8771371,35.8315883 C14.6287279,32.6216656 21.0693288,29.5480123 31.4774882,27.1957477 C38.1847591,25.6799709 46.0533839,24.4388899 54.6748376,23.542507 C54.6821855,23.541743 54.6639431,23.2587798 54.6201103,22.6936172 C45.9538336,23.5935735 38.0407814,24.8410734 31.2901169,26.3666566 C20.6551708,28.7701755 14.03919,31.9275254 12.130977,35.4244703 C11.6101367,36.3791687 11.4693273,37.3624786 11.7202377,38.3484235 C13.3024545,44.5624379 30.9943144,49.7749434 56.7224604,52.193696 Z" id="Fill-80" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M4.21384162,23.8125578 C5.96541143,20.6026736 12.4061585,17.5289875 22.8142149,15.1766806 C36.8098448,12.0135806 55.543849,10.1065247 75.4208683,9.91168471 C85.1171929,9.8165815 95.0537766,10.0991696 104.125128,10.7299842 L103.734293,10.3689161 L103.806748,10.8530223 L104.256557,10.3661387 C95.1490848,9.73263821 85.6229744,9.45951201 75.9417688,9.55427188 C38.366808,9.92270807 7.93899809,15.7959486 3.74202798,23.496014 C3.73373428,23.5112292 3.72553959,23.5264525 3.717444,23.5416835 L4.21384162,23.8125578 Z M46.6106382,39.1743201 C52.4637755,39.7813683 58.6331824,40.2274228 64.9390083,40.4867651 L64.9215439,40.9114061 L64.8346959,40.4953743 L65.0746059,40.8794539 L65.0571468,41.3040951 C59.0936083,41.0589043 53.4042755,40.6771676 48.0614581,40.1749395 C22.3324675,37.7563982 4.6392453,32.5436332 3.05700411,26.3295229 C2.8057977,25.3440766 2.94669613,24.3607391 3.46765147,23.4054948 L4.21389127,23.8124668 C3.79330767,24.5836654 3.68385816,25.3475193 3.88069276,26.119673 C5.30682898,31.7206949 22.1243925,36.7245885 46.6106382,39.1743201 C52.3791664,39.7514337 58.5732993,40.1867937 65.092065,40.4548127 L68.5145005,40.5955261 L65.0083919,41.3274379 C59.1309422,41.098613 53.4711946,40.7050964 48.0614614,40.1749398 C38.4349253,39.2315341 29.600079,37.8554493 22.3267773,36.1562393 C16.7671505,34.8574705 12.3197578,33.3933646 9.10347966,31.8003238 C5.82796216,30.1777537 3.8404427,28.2813548 3.34287172,26.3266956 C3.1039707,25.3884322 3.23406313,24.4511318 3.71742791,23.5417138 C3.71743149,23.5417071 3.63418923,23.4962704 3.46770112,23.4054038 C5.37587451,19.9085317 11.9919937,16.7511536 22.6268347,14.3475916 C36.6857829,11.1701812 55.4787622,9.25712188 75.4125342,9.06172557 C85.1309315,8.96640587 95.0899407,9.24963178 104.184094,9.88203197 L104.524431,9.90569877 L104.725754,11.2508291 L104.197575,11.2140899 C95.1128405,10.5821709 85.6091628,10.3096879 75.9500956,10.4042311 C38.7514613,10.7689773 8.46015768,16.615869 4.48835876,23.902819 C4.08485204,24.6430755 3.97815669,25.3769062 4.16659586,26.116985 C4.59090206,27.7838307 6.40557842,29.5153106 9.48076557,31.0386445 C12.6303747,32.5986638 17.018959,34.0434096 22.5201436,35.3285259 C29.3683345,36.9284203 37.6160538,38.241462 46.6106382,39.1743201 Z" id="Fill-86" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M44.8922109,30.7030436 C50.2363139,31.2055021 55.9271593,31.5874209 61.8923834,31.8327354 L61.9098464,31.4080943 L61.8228339,30.9920969 L61.7570863,31.4400465 L61.7745507,31.0154055 C55.4673622,30.7560072 49.2964065,30.30976 43.4417882,29.7024297 C34.4492233,28.7695838 26.2029704,27.456706 19.355678,25.8571644 C13.8552854,24.5722331 9.46637304,23.1272285 6.31630799,21.5669834 C3.24100075,20.0440142 1.42648545,18.3126169 1.00214259,16.6453408 C0.813701305,15.9055146 0.920404149,15.171698 1.32390118,14.4314595 C5.29539537,7.14452396 35.586951,1.2976182 72.7859399,0.932871422 C73.2725158,0.927922889 73.7729188,0.923953939 74.2950126,0.920826789 L74.2899216,0.0708420354 C73.9935257,0.0726173375 73.7040796,0.0746635535 73.4201786,0.0770055687 C73.2028978,0.078798007 72.9888651,0.0807637084 72.7774508,0.0829138287 C35.202277,0.451349176 4.77421941,6.32460268 0.577564025,14.0246661 C0.0740747179,14.9483462 -0.0644994667,15.9013484 0.178422989,16.8550692 C0.676010857,18.8101307 2.66342095,20.706503 5.93906427,22.3286836 C9.15574556,23.9219241 13.6034742,25.3862934 19.1623198,26.6848797 C26.4346562,28.3837125 35.2678993,29.7596331 44.8922109,30.7030436 C50.3035444,31.2334824 55.9649604,31.6271906 61.7396219,31.8646875 L65.3440896,31.1239652 L61.9273094,30.9834532 C55.4069818,30.7153106 49.2114629,30.279769 43.4417882,29.7024297 C18.9575707,27.2524303 2.14202979,22.2489143 0.716239303,16.6483296 C0.51939806,15.875886 0.628846042,15.1120427 1.04940368,14.3411622 C2.80095384,11.1313139 9.24170081,8.05762784 19.6497573,5.705321 C33.6453871,2.54222089 52.3793912,0.635165051 72.2564108,0.440325042 C72.6061066,0.436800912 72.9690414,0.433849018 73.4226424,0.430689195 L73.4167214,-0.419290182 C72.9623123,-0.41612473 72.5985545,-0.413166142 72.2479622,-0.409632962 C52.3143044,-0.214237784 33.5213251,1.69882151 19.4623771,4.87623194 C8.82753605,7.27979397 2.21141691,10.437172 0.303243551,13.9340442 C-0.217756897,14.889036 -0.358657573,15.8723893 -0.107462046,16.8581297 C1.47440561,23.0717942 19.1654113,28.284181 44.8922109,30.7030436 Z" id="Fill-88" style="fill: rgb(224, 226, 235);"></path>
                                                    </g>
                                                    <g id="right" transform="translate(44.047088, 16.649414)">
                                                        <path d="M56.5612345,49.4312315 C50.8212419,50.1525765 44.6497491,50.7429884 38.1465782,51.1743263 L34.720936,51.4015399 L38.2525432,52.0445539 C44.1050431,51.669195 49.7337764,51.1353528 55.1080884,50.4716973 C64.7015835,49.2870291 73.4843763,47.6887162 80.689069,45.8060347 C86.1895102,44.3684814 90.5716431,42.7934649 93.7180529,41.1204424 C96.9235913,39.4155501 98.8298778,37.4680828 99.2433189,35.4989114 C99.4445531,34.5390406 99.2648121,33.5912362 98.7222773,32.6822095 C95.9193111,27.9861974 83.105044,24.1710587 63.9950677,22.1147321 C63.83096,22.0970734 63.666388,22.0795443 63.5013541,22.0621454 L63.4122361,22.9074608 C82.4985328,24.9196477 95.3659282,28.7175277 97.992398,33.1178426 C98.4286917,33.8488605 98.5674124,34.5803578 98.4114304,35.3243794 C98.0600011,36.9981956 96.32399,38.7717091 93.3189541,40.3699624 C90.2390762,42.0076083 85.9158472,43.5614537 80.4741537,44.9836532 C73.6982826,46.7542779 65.5111532,48.2733731 56.5612345,49.4312315 C50.7377391,50.1846219 44.5912947,50.7850722 38.3005432,51.20236 L38.3286731,51.626428 L38.404803,51.2083021 L38.1747055,51.5983945 L38.2028328,52.0224627 C44.1405206,51.6286318 49.7992963,51.1058094 55.1080884,50.4716973 C80.7421777,47.4098166 98.2162335,41.7531611 99.5289316,35.4946067 C99.7371217,34.5027504 99.5538174,33.5243006 98.992334,32.5849105 C96.9353219,29.1414498 90.1869826,26.1526567 79.4551317,24.0179588 C74.8152382,23.0949737 69.634581,22.3129206 64.041958,21.6922332 L63.948198,22.5370462 C69.5175738,23.1551536 74.6743424,23.9336006 79.2893003,24.8516253 C89.796951,26.9417271 96.3702692,29.8530046 98.2626741,33.0209109 C98.7173968,33.781685 98.8600831,34.5433219 98.6970464,35.3200588 C97.5165963,40.9480941 80.9263792,46.3692632 56.5612345,49.4312315 Z" id="Fill-112" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M64.4243466,36.3456775 C58.6739812,37.0691079 52.4900649,37.6611092 45.9730552,38.0933612 L42.5425528,38.3208952 L46.0794662,38.963615 C51.9437259,38.5875395 57.5840954,38.0522818 62.9688229,37.3866535 C72.5486339,36.2024537 81.3193146,34.6056189 88.5158478,32.7250696 C94.0174465,31.2872137 98.3990873,29.7124505 101.54486,28.0394623 C104.750312,26.3344552 106.65672,24.3869443 107.069807,22.4181939 C107.271009,21.4584754 107.091337,20.5104124 106.549116,19.6013445 C105.488757,17.8247119 103.039016,16.190212 99.3224223,14.7146658 C99.2252347,14.6760809 99.1271809,14.6376046 99.028263,14.5992373 L98.7208852,15.3917137 C102.441369,16.8347779 104.851708,18.4158824 105.819169,20.0368652 C106.255189,20.7678799 106.393884,21.4997192 106.237907,22.2437157 C105.886798,23.9170852 104.150666,25.6906477 101.145718,27.2890051 C98.0664978,28.9265995 93.7437682,30.48019 88.3009325,31.9026881 C81.5339246,33.6709967 73.3596913,35.1884421 64.4243466,36.3456775 C58.5898904,37.1013102 52.4309255,37.7033632 46.1273291,38.1213944 L46.1554518,38.5454629 L46.2314374,38.1273108 L46.0011823,38.5174294 L46.0293094,38.9414976 C51.979772,38.5468228 57.6501195,38.0226029 62.9688229,37.3866535 C88.5844336,34.3238333 106.043593,28.6694269 107.35571,22.4136415 C107.56386,21.421977 107.380408,20.4435585 106.818845,19.503701 C105.705501,17.6402361 103.221948,15.9104753 99.4619859,14.373292 L99.1403243,15.160079 C102.745666,16.6340488 105.089525,18.2665144 106.089165,19.9396687 C106.544002,20.7009048 106.686815,21.4625781 106.523825,22.2390935 C105.343963,27.8643267 88.7696932,33.2828869 64.4243466,36.3456775 Z" id="Fill-114" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M110.203801,13.3217286 C110.194541,13.3058283 110.185171,13.28994 110.175689,13.2740635 C105.649006,5.68928057 74.9804234,0.582659097 37.4114465,1.15668419 C27.7310829,1.30486064 18.2212774,1.81670796 9.14585391,2.6780748 L9.60865722,3.14584117 L9.65997977,2.66022781 L9.27748354,3.03865927 C18.3165266,2.18090064 28.2360829,1.64932354 37.9322279,1.50107678 C57.8082758,1.19749489 76.6167564,2.63355854 90.7430747,5.44357948 C101.250172,7.53362888 107.823991,10.445171 109.716457,13.61288 L110.203801,13.3217286 Z M49.8108709,32.6424505 L46.1431298,31.9959202 L49.6003476,31.7665818 C56.1099842,31.334758 62.2872718,30.7435559 68.0320965,30.0211976 C92.3880172,26.9586671 108.970657,21.5386758 110.150848,15.9118775 C110.314014,15.1357816 110.171193,14.3741305 109.716445,13.6128612 L110.446165,13.1769602 C111.007626,14.116874 111.191101,15.0953274 110.982705,16.0865607 C109.670206,22.3441668 92.2011836,28.0001024 66.5731103,31.0624788 C61.2610715,31.6972305 55.5984987,32.2205561 49.6566096,32.6147178 L49.6284786,32.1906498 L49.7036732,31.7723547 L49.7827482,32.218382 L49.7546255,31.7943135 C56.051408,31.3767341 62.2036221,30.7755918 68.0320965,30.0211976 C76.975358,28.8636487 85.1564036,27.345282 91.9279537,25.5756001 C97.3699262,24.153706 101.693133,22.5998686 104.772755,20.9619017 C107.777923,19.3638615 109.514083,17.590346 109.865204,15.9166352 C110.021207,15.172507 109.882397,14.4407508 109.445858,13.7097775 C105.156111,6.52199887 74.6245667,1.43819553 37.4244441,2.00658481 C27.7662389,2.15442208 18.2788671,2.66506195 9.22616819,3.52427194 L8.70873823,3.57338227 L8.85121258,2.22529163 L9.19718366,2.19246075 C18.2590164,1.33253949 28.2009989,0.799760601 37.9192401,0.651176015 C57.851858,0.346730089 76.7192416,1.7872911 90.9089071,4.60991311 C101.64019,6.74455713 108.389027,9.73361447 110.446153,13.1769414 L110.203801,13.3217286 C110.724667,14.2161163 110.894843,15.147908 110.697107,16.0910986 C110.284014,18.0602142 108.37753,20.0077182 105.171869,21.7123732 C102.025737,23.3857151 97.6435819,24.9607397 92.1428528,26.3979859 C84.941002,28.2801229 76.162233,29.8779979 66.5731103,31.0624788 C61.195486,31.7267411 55.5630062,32.2609901 49.8108709,32.6424505 Z" id="Fill-116" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M80.0473028,23.3357035 C90.5550362,25.4261111 97.1286041,28.3374306 99.0209888,31.5050044 C99.4755672,32.2659909 99.6183299,33.0276818 99.4553521,33.8041372 C98.2744491,39.4332146 81.6786285,44.8554426 57.3068482,47.9171729 C51.5704414,48.637815 45.4032508,49.2276869 38.9048839,49.6587062 L35.4572918,49.8873757 L39.0111963,50.5287501 C44.8574169,50.15376 50.4807874,49.6206667 55.8503749,48.9579617 C65.4499731,47.7731963 74.2384486,46.1741754 81.4470756,44.2904139 C86.9481838,42.8526862 91.3303398,41.27764 94.4763869,39.6045058 C97.6819201,37.8999185 99.5882254,35.9524302 100.001337,33.9832231 C100.202831,33.0237226 100.022971,32.0755753 99.4802811,31.166288 C96.2895162,25.8201193 80.0716738,21.6209524 56.7082038,19.8689506 L56.6446416,20.7165708 C79.6899781,22.4447161 95.7699855,26.6081943 98.7503937,31.6019076 C99.1868311,32.3331662 99.3256403,33.0649144 99.1694635,33.8086194 C98.8182903,35.4825804 97.0823118,37.2560623 94.0772874,38.8540262 C90.9977436,40.4917926 86.6745038,42.045663 81.2321575,43.4680331 C74.453161,45.2395233 66.2615827,46.7591973 57.3068482,47.9171729 C51.4870088,48.6697617 45.3448176,49.2695716 39.0585553,49.6864379 L39.0866769,50.1105065 L39.1621575,49.6922629 L38.9330112,50.0827744 L38.9611385,50.5068426 C44.8928451,50.1134084 50.5462119,49.591231 55.8503768,48.9579615 C81.4928682,45.896479 98.9739915,40.2386088 100.287234,33.9787018 C100.495367,32.9871161 100.311986,32.0087103 99.750697,31.0690848 C97.6936373,27.6258692 90.9450777,24.6370488 80.2131465,22.5020394 C73.4000641,21.1467331 65.4368075,20.0985577 56.734987,19.4166044 L56.668577,20.2640062 C65.3446704,20.9442662 73.2716199,21.987837 80.0473028,23.3357035 Z" id="Fill-118" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M58.8864314,42.264498 C53.1415176,42.9868717 46.9641317,43.5780864 40.4543984,44.0099167 L36.9971732,44.2392555 L40.5616045,44.8800119 C46.4171145,44.5043211 52.0496589,43.9700646 57.4273599,43.3057907 C67.0164035,42.121316 75.7951752,40.5234527 82.9972027,38.6413214 C88.4977752,37.2040368 92.8798684,35.629007 96.0259202,33.955708 C99.2315803,32.2510533 101.138064,30.3035494 101.551158,28.3344336 C101.752383,27.3746009 101.572616,26.4264931 101.030101,25.5174984 C96.5449699,18.0023387 66.2037447,13.1349512 28.2787773,13.7147183 C18.5954875,13.8629394 9.08586462,14.3747009 0.013174171,15.23611 L0.477866005,15.6793849 L0.515905068,14.8790724 L0.131534261,15.2819941 C9.17057757,14.4242354 19.090134,13.8926583 28.7862787,13.7444116 C48.662536,13.440831 67.4707366,14.8768793 81.5971254,17.6869143 C92.1043367,19.7769287 98.6780001,22.6884364 100.570507,25.8562147 L101.300203,25.4202763 C99.2429364,21.9768368 92.4942691,18.9878386 81.7629556,16.8532475 C67.573222,14.0306119 48.7061183,12.5900662 28.773291,12.8945109 C19.0550499,13.0430954 9.11306744,13.5758743 0.0512343798,14.4357955 L-0.315640197,14.47061 L-0.394446525,16.1286343 L0.0935163438,16.0823046 C9.14346561,15.2230546 18.6306463,14.7125008 28.2917783,14.5646188 C65.8377682,13.9906453 96.0538016,18.8379493 100.30021,25.9531123 C100.736492,26.6841099 100.875245,27.4159078 100.719255,28.15997 C100.368133,29.8336811 98.631973,31.6071966 95.6268056,33.2052365 C92.547261,34.8431622 88.2241122,36.3970059 82.7823008,37.8189363 C76.0106114,39.5886058 67.8295895,41.106957 58.8864314,42.264498 C53.0578501,43.0189096 46.9055435,43.6200633 40.6086762,44.0376483 L40.6367989,44.4617168 L40.7119933,44.0434217 L40.4825294,44.4339847 L40.5106604,44.8580527 C46.4526152,44.4638866 52.1152566,43.9405527 57.4273599,43.3057907 C83.0552064,40.243429 100.52425,34.5875709 101.836754,28.3302003 C102.045204,27.3383831 101.861822,26.3598925 101.300157,25.4201992 L100.570553,25.8562918 C101.02539,26.6172582 101.168125,27.3788667 101.004892,28.1555417 C99.8247072,33.7820818 83.2420878,39.2019829 58.8864314,42.264498 Z" id="Fill-124" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M69.1450733,29.119938 C63.3884522,29.8446164 57.197052,30.4375454 50.6717285,30.8702916 L47.2675437,31.0960502 L50.7773451,31.7407027 C56.6499157,31.3641736 62.2972642,30.8280102 67.6880101,30.161232 C77.2602783,28.9772438 86.0234633,27.3814192 93.2145174,25.5023017 C98.715354,24.0646451 103.097328,22.4896093 106.243529,20.8163929 C109.448988,19.1116844 111.355399,17.1641719 111.76848,15.1951103 C111.96998,14.2355792 111.790099,13.2874009 111.247388,12.3784171 C106.720699,4.7933119 76.0519089,-0.31332922 38.4831182,0.260997371 C38.2685942,0.264203891 38.0514266,0.267669294 37.8309927,0.271397901 C37.5499902,0.276151017 37.2636797,0.281331851 36.970771,0.286949351 L36.9870696,1.13679308 C37.5085925,1.12679114 38.0090718,1.11817795 38.4959664,1.11090025 C75.6960558,0.542210142 106.227796,5.62603106 110.517529,12.814084 C110.953967,13.5450725 111.092784,14.2767964 110.936606,15.0205065 C110.585465,16.694316 108.849364,18.4678464 105.844415,20.0659212 C102.764728,21.7037639 98.4416732,23.2576221 92.9996021,24.6799202 C86.2384693,26.4466936 78.0722108,27.9630305 69.1450733,29.119938 C63.3042791,29.8768725 57.1377721,30.4799443 50.8259915,30.8986271 L50.8541214,31.3226951 L50.9308977,30.9046875 L50.6998519,31.2943601 L50.7279753,31.7184286 C56.6860756,31.3232995 62.3633589,30.7982711 67.6880101,30.161232 C93.2920019,27.0979811 110.742293,21.4447739 112.054078,15.1905724 C112.262523,14.1994433 112.079175,13.2206745 111.51784,12.2809719 C109.460748,8.83770364 102.712078,5.84891373 91.9802781,3.71422581 C77.7896237,0.891407069 58.9223229,-0.549233105 38.9906072,-0.244510763 C38.6187281,-0.239029159 38.2323979,-0.232521623 37.8217735,-0.224893773 L37.8375605,0.624959607 C38.2471457,0.617351063 38.6323999,0.610861652 39.003368,0.605393413 C58.8787468,0.30153243 77.6871384,1.73767451 91.8144479,4.54789261 C102.322054,6.63798568 108.895718,9.54927038 110.788132,12.7168917 C111.242711,13.4778791 111.385439,14.2398087 111.222227,15.0158608 C110.042675,20.6396145 93.4779825,26.0567634 69.1450733,29.119938 Z" id="Fill-126" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M71.2842492,11.5724038 C87.7047225,13.8265254 98.1360385,17.716926 101.389891,22.4875904 C102.191597,23.6627951 102.496227,24.8792443 102.308677,26.1480433 C100.988285,35.085806 75.7315255,45.0375399 38.1890717,52.0191001 C29.5383376,53.6278287 20.235284,55.0788535 10.4224528,56.3234297 L7.01611225,56.7554611 L10.6646971,57.1959265 C19.7226791,56.0600001 28.4714723,54.693161 36.7992674,53.1450296 C51.275452,50.4539167 64.4795159,47.2149969 75.2757362,43.6905552 C83.5887702,40.9765029 90.1693307,38.1554111 94.8381814,35.3014912 C99.5595137,32.4147296 102.268998,29.2922714 102.71058,26.305507 C102.922592,24.8695863 102.572716,23.4854804 101.685574,22.1833528 C96.344134,14.3442205 71.0419464,9.38811258 35.0219901,8.85086542 L35.0093135,9.70077088 C70.710339,10.2332611 95.8727887,15.1619977 100.983125,22.6619605 C101.753579,23.7928173 102.049509,24.9635104 101.869708,26.1812704 C101.472272,28.869444 98.9223578,31.8080105 94.3948234,34.5762784 C89.7902712,37.3908946 83.2672498,40.1873194 75.0119415,42.8825252 C64.6374462,46.2692942 52.0238052,49.3933514 38.1890717,52.0191001 C29.4177845,53.6838373 20.1556645,55.1482735 10.6621584,56.3523677 L10.7156342,56.77399 L10.7665713,56.3520535 L10.4759278,56.7450521 L10.5294028,57.1666745 C19.7794221,55.9934805 28.5742038,54.6407255 36.7992674,53.1450296 C75.6093128,46.0875732 101.734965,35.847631 103.149546,26.272302 C103.369239,24.7860436 103.011042,23.3556897 102.092086,22.0086098 C98.6573785,16.9727835 88.0467422,13.0155046 71.3998511,10.7303015 C60.8025351,9.275496 48.4633236,8.39855693 35.020714,8.15246218 L35.0051556,9.00231978 C48.4248064,9.24846465 60.7259041,10.1229482 71.2842492,11.5724038 Z" id="Fill-128" style="fill: rgb(224, 226, 235);"></path>
                                                    </g>
                                                    <g id="dots" transform="translate(1.358315, 6.796682)">
                                                        <path d="M9.4082156,2.04587128 C8.98495555,1.81798568 8.82736514,1.29077415 9.0555995,0.86816091 C9.2838339,0.4458491 9.8118523,0.28819808 10.2351124,0.51608368 C10.6580705,0.74396928 10.8159628,1.27148225 10.5877284,1.69379406 C10.3591922,2.11610587 9.8311738,2.27375689 9.4082156,2.04587128" id="Fill-249" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M141.695397,25.364175 C141.392292,25.2007967 141.279081,24.8230975 141.442709,24.5204557 C141.606337,24.217814 141.984615,24.1047755 142.287418,24.2681538 C142.590523,24.4315321 142.703734,24.8092314 142.540106,25.1118731 C142.376477,25.4145148 141.998502,25.5275533 141.695397,25.364175" id="Fill-337" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M15.4325767,18.6610527 C15.134906,18.5006888 15.0238077,18.1296211 15.1844171,17.8321038 C15.3450265,17.5348879 15.7166621,17.4239594 16.0146348,17.5843234 C16.3123055,17.7446873 16.4234037,18.115755 16.2627944,18.4132723 C16.102185,18.7107896 15.7302475,18.8217181 15.4325767,18.6610527" id="Fill-387" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M15.7252058,5.4320222 C15.6231645,5.3768594 15.5848235,5.2496535 15.6400708,5.1477681 C15.6950161,5.0455813 15.8227186,5.0076004 15.9247599,5.0627632 C16.0271031,5.1179259 16.0651422,5.2451319 16.009895,5.3470173 C15.9549497,5.4492041 15.8272471,5.487185 15.7252058,5.4320222" id="Fill-556" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M0.3996296,16.7351179 C0.23268038,16.64529 0.17048953,16.4369977 0.26045493,16.2703036 C0.35042034,16.103911 0.55872949,16.0418152 0.72598061,16.1316431 C0.89262793,16.221471 0.95481878,16.4294618 0.86485338,16.5961559 C0.77488798,16.76285 0.56657882,16.8249459 0.3996296,16.7351179" id="Fill-564" style="fill: rgb(224, 226, 235);"></path>
                                                        <path d="M139.397566,26.0546142 C139.330545,26.0184418 139.305487,25.9349441 139.341715,25.8677238 C139.377943,25.8008051 139.46187,25.7757859 139.528891,25.8119582 C139.595912,25.8481305 139.62097,25.9319297 139.584742,25.9988485 C139.548515,26.0657673 139.464889,26.0907865 139.397566,26.0546142" id="Fill-580" style="fill: rgb(224, 226, 235);"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    <div class="Funnel__canvas--wrapper">
                                        <canvas id="funnel__canvas" class="Funnel__canvas" width="1366" height="250"></canvas>
                                    </div>
                                    <img class="Funnel__canvas--etalent" src="https://www.aspirantecertificado.com/assets/front/wp-content/themes/evaluar/dist/images/etalent-logo_e1ed12df.png" alt="E-talent">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="ranking" class="Ranking">
                    <div class="ev-container row Ranking__top">
                        <!--div class="col-12 col-md-6 Ranking__video">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe width="420" height="315" class="embed-responsive-item" src="https://www.youtube.com/embed/I9eW8F0-2rA" frameborder="0" allowfullscreen=""></iframe>
                            </div>
                        </div-->
                        <div class="col-12 col-md-6 Ranking__description wow fadeIn" style="visibility: visible;">
                            <h3><strong>RANKING DE FINALISTAS</strong></h3>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                    <div class="ev-container row Ranking__middle">
                        <div class="col-12 col-md-6 Ranking__list">
                            <h3>% Ajuste al Puesto</h3>
                            <p>&nbsp;</p>
                            <p>Es el resultado del análisis integral de cada candidato en comparación con el perfil ideal de la vacante.</p>
                            <p>Obtén una calificación objetiva de tus postulantes basado en los 4 criterios de evaluación.</p>
                        </div>
                        <div class="col-12 col-md-6 Ranking__lasted">
                            <div class="ev-container row align-items-end">
                                <?php foreach ($candidates as $candidate) { ?> 
                                <div class="col-4 Candidate text-center wow fadeInUp" data-wow-delay="200ms" style="visibility: visible; animation-delay: 200ms;">
                                    <figure class="Candidate__figure">
                                        <div class="Candidate__percent"><?php echo $candidate['experience']?><span class="Candidate__sign">%</span></div>
                                        <img src="https://www.aspirantecertificado.com/assets/images/candidates/<?php echo $candidate['image'] ?>" alt="dibujo-sin-titulo-11" class="Candidate__image">
                                    </figure>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="ev-container row Ranking__bottom">
                        <div class="col-12 col-md-7 Ranking__image wow fadeIn text-center" style="visibility: visible;">
                            <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/07/Dibujo-sin-título-1-1.png" alt="dibujo-sin-titulo-1-2">
                        </div>
                        <div class="col-12 col-md-5 Ranking__descriptionLiveReport wow fadeIn" style="visibility: visible;">
                            <h3><strong>LIVE REPORT</strong></h3>
                            <p>&nbsp;</p>
                            <p style="text-align: justify;">Promovemos la evaluación colaborativa, olvídate de esas largas horas redactando informes para los jefes inmediatos o clientes internos, ahora nuestro Live Report lo hace todo por ti.</p>
                            <p style="text-align: justify;">Recibirás un reporte completo de cada persona con el CV + resultados de las competencias y los videos de las entrevistas grabadas.</p>
                        </div>
                    </div>
                </section>
                <section id="demo" class="Demo" style="">
                    <div class="ev-container row align-items-center">
                        <div class="col-12 hidden-sm-down">
                            <div class="row">
                                <div class="col Demo__advantage wow fadeInUp" data-wow-delay="200ms" style="visibility: visible; animation-delay: 200ms;">
                                    <span class="Demo__icon"><i class="icon-gears"></i></span>
                                    <h5>Automatiza</h5>
                                </div>
                                <div class="col Demo__advantage wow fadeInUp" data-wow-delay="400ms" style="visibility: visible; animation-delay: 400ms;">
                                    <span class="Demo__icon"><i class="icon-piggy-bank"></i></span>
                                    <h5>Ahorra</h5>
                                </div>
                                <div class="col Demo__advantage wow fadeInUp" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms;">
                                    <span class="Demo__icon"><i class="icon-exam"></i></span>
                                    <h5>Califica</h5>
                                </div>
                                <div class="col Demo__advantage wow fadeInUp" data-wow-delay="800ms" style="visibility: visible; animation-delay: 800ms;">
                                    <span class="Demo__icon"><i class="icon-share"></i></span>
                                    <h5>Comparte</h5>
                                </div>
                                <div class="col Demo__advantage wow fadeInUp" data-wow-delay="1000ms" style="visibility: visible; animation-delay: 1000ms;">
                                    <span class="Demo__icon"><i class="icon-handshake"></i></span>
                                    <h5>Contrata</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 Demo__description">
                            <h2 class="p1"><strong><span class="s1">Solicita un Demo hoy.</span></strong></h2>
                            <p class="p1"><span style="font-weight: 400;">Nuestro equipo estará gustoso en ayudarte, prueba todo lo que Aspirantecertificado.com puede hacer por ti y tu equipo de trabajo.</span></p>
                        </div>
                        <div class="col-12 col-md-6 text-center Demo__figure" style="background-image: url(https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/07/demo_figure.png)">
                            <a id="requiestModalbtn" style="color:#ff7b36; cursor:pointer;" class="ev__button ev__button--white js-modal-hubspot-btn">Solicitar DEMO</a>

                            <!--<button id="requiestModalbtn">Open Modal</button>-->
                            <div id="requiestModal" class="requiestModal">
                                <!-- Modal content -->
                                <div class="requiestModal-content">
                                    <div class="requiestModal-header">
                                        <h2 style="font-size: 30px; font-weight: 800; color: #555; margin-top: 20px; ">Solicitar DEMO</h2>
                                    </div>
                                    <div class="requiestModal-body">
                                      <form class="modal-content" action="email/sendadmin" method="POST">
									  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                                        <label for="Nombre"><b>Nombre</b></label>
                                        <input type="text" placeholder="Nombre" name="firstname" required>
                                        
                                        <label for="Apellido"><b>Apellido</b></label>
                                        <input type="text" placeholder="Apellido" name="lastname" required>
                                        
                                        <label for="email"><b>Email</b></label>
                                        <input type="text" placeholder="Enter Email" name="email" required>
                                        
                                        <label for="Teléfono"><b>Teléfono</b></label>
                                        <input type="text" placeholder="Teléfono" name="phone" required>
                                    
                                        <label for="Pais"><b>Pais</b></label>
                                        <select id="countrySelect" required="" class="hs-input invalid error" name="pais">
                                            <option value="" disabled="" selected="">- Por favor, elige -</option>
                                            <option value="Argentina">Argentina</option>
                                            <option value="Brasil">Brasil</option>
                                            <option value="Chile">Chile</option>
                                            <option value="Colombia">Colombia</option>
                                            <option value="Costa Rica">Costa Rica</option>
                                            <option value="Cuba">Cuba</option>
                                            <option value="Ecuador">Ecuador</option>
                                            <option value="Bolivia">Bolivia</option>
                                            <option value="España">España</option>
                                            <option value="El Salvador">El Salvador</option>
                                            <option value="Guatemala">Guatemala</option>
                                            <option value="Honduras">Honduras</option>
                                            <option value="México">México</option>
                                            <option value="Nicaragua">Nicaragua</option>
                                            <option value="Panamá">Panamá</option>
                                            <option value="Paraguay">Paraguay</option>
                                            <option value="Perú">Perú</option>
                                            <option value="República Dominicana" >República Dominicana</option>
                                            <option value="Uruguay" >Uruguay</option>
                                            <option value="USA" >USA</option>
                                            <option value="Venezuela" >Venezuela</option>
                                            <option value="Otro" >Otro</option>
                                        </select>
                                        
                                        <label for="Ciudad"><b>Ciudad</b></label>
                                        <input type="text" placeholder="Ciudad" name="city" required>
                                        
                                        <label for="Empresa"><b>Empresa</b></label>
                                        <input type="text" placeholder="Empresa" name="company" required>
                                        
                                        <label for="numberOfcompany"><b>Número de colaboradores</b></label>
                                        <select id="numbersCompany" required="" class="hs-input invalid error" name="numemployees">
                                            <option value="" disabled="" selected="">- Por favor, elige -</option>
                                            <option value="1-50">1-50</option>
                                            <option value="50-100">50-100</option>
                                            <option value="100-250">100-250</option>
                                            <option value="250-500">250-500</option>
                                            <option value="500-1000">500-1000</option>
                                            <option value="1000-3000">1000-3000</option>
                                            <option value="3000+">3000+</option>
                                        </select>
                                        
                                        <label for="lookingjob"><b>¿Estás buscando empleo?</b></label>
                                        <select id="lookingforjob" required="" class="hs-input invalid error" name="lookingforjob">
                                            <option value="" disabled="" selected="">- Por favor, elige -</option>
                                            <option value="YES">YES</option>
                                            <option value="NO">NO</option>
                                        </select>
                                        
                                        <label for="describfunctions"><b>¿Cómo describes tus funciones?</b></label>
                                        <select id="describfunctions" required="" class="hs-input invalid error" name="describfunctions">
                                            <option value="" disabled="" selected="" >- Por favor, elige -</option>
                                            <option value="persona_10" >Analista de RRHH</option>
                                            <option value="persona_20" >Busco capacitación en Talento Humano</option>
                                            <option value="persona_5" >Consultor de RRHH</option>
                                            <option value="persona_12" >Dueño de negocio</option>
                                            <option value="persona_18" >Estoy buscando trabajo</option>
                                            <option value="persona_13" >Estudiante</option>
                                            <option value="persona_8" >Evaluado(a) / Candidato(a)</option>
                                            <option value="persona_4" >Gerente General</option>
                                            <option value="persona_11" >Gerente de RRHH</option>
                                            <option value="persona_22" >Gerente de otro departamento</option>
                                            <option value="persona_21" >Jefe de RRHH</option>
                                            <option value="persona_15" >Jefe de otro departamento</option>
                                            <option value="persona_9" >Otro</option>
                                            <option value="persona_19" >Pasante</option>
                                        </select>
                                        
                                        <label for="productinterest"><b>Producto de Interés</b></label>
                                        <select id="productinterest" required="" class="hs-input invalid error" name="productinterest">
                                            <option value="" disabled="" selected="">- Por favor, elige -</option>
                                            <option value="Evaluar en General">Evaluar en General</option>
                                            <option value="Sistema de Automatización del Proceso de Selección">Sistema de Automatización del Proceso de Selección</option>
                                            <option value="Pruebas Psicométricas y Competencias Laborales">Pruebas Psicométricas y Competencias Laborales</option>
                                            <option value="Selección y Reclutamiento Digital">Selección y Reclutamiento Digital</option>
                                            <option value="Selección y Reclutamiento">Selección y Reclutamiento</option>
                                            <option value="Evaluaciones de Desempeño">Evaluaciones de Desempeño</option>
                                            <option value="Encuestas de Clima Laboral">Encuestas de Clima Laboral</option>
                                            <option value="Riesgos Psicosociales">Riesgos Psicosociales</option>
                                            <option value="Encuestas de Talento Humano">Encuestas de Talento Humano</option>
                                            <option value="Evaluaciones de Integridad y Honestidad Laboral">Evaluaciones de Integridad y Honestidad Laboral</option>
                                        </select>
                                        
                                        <label for="findabouthere"><b>¿Cómo te enteraste de Aspirantecertificado.com?</b></label>
                                        <select id="findabouthere" required="" class="hs-input invalid error" name="findabouthere">
                                            <option value="" disabled="" selected="">Seleccionar</option>
                                            <option value="Facebook">Facebook</option>
                                            <option value="Instagram">Instagram</option>
                                            <option value="LinkedIn">LinkedIn</option>
                                            <option value="Anuncio de Google">Anuncio de Google</option>
                                            <option value="Correo Electrónico">Correo Electrónico</option>
                                            <option value="Blog">Blog</option>
                                            <option value="Revistas / Vallas">Revistas / Vallas</option>
                                            <option value="Busqué en internet">Busqué en internet</option>
                                            <option value="Me recomendaron">Me recomendaron</option>
                                            <option value="Ya lo conocía">Ya lo conocía</option>
                                        </select>
                                        
                                        <div class="clearfix">
                                            <button type="button" onclick="document.getElementById('requiestModal').style.display='none'" class="cancelbtn">Cancelar</button>
                                            <button type="submit" class="signupbtn">Enviar</button>
                                        </div>
                                      </form>
                                    </div>
                                </div>
                            </div>
                            <style>
                                
/* The Modal (background) */
.requiestModal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 99999999999999; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

input[type=text],select{
  width: 100%;
  padding: 5px;
  margin: 5px 0 13px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
  font-size:15px;
}
.requiestModalbtn{
    color:red;
}
.requiestModalbtn:hover{
    cursor:pointer;
}

/* Add a background color when the inputs get focus */
input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

.requiestModal-body label{
    font-size:14px;
    font-weight:100;
    margin-bottom:0px;
    
}

/* Set a style for all buttons */
button {
  background-color: #4CAF50;
  color: white;
  padding: 7px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 49%;
  opacity: 0.9;
}
.cancelbtn{
    padding: 7px 20px !important;
}
button:hover {
  opacity:1;
}

/* Extra styles for the cancel button */
.cancelbtn {
  padding: 14px 20px;
  background-color: #f44336;
}

/* Modal Content */
.requiestModal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  border: 1px solid #888;
  width:500px;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
  -webkit-animation-name: animatetop;
  -webkit-animation-duration: 0.4s;
  animation-name: animatetop;
  animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
  from {top:-300px; opacity:0} 
  to {top:0; opacity:1}
}

@keyframes animatetop {
  from {top:-300px; opacity:0}
  to {top:0; opacity:1}
}

/* The Close Button */
.requiestModal-header .requiestModalclose {
  color: white;
  float: right;
  font-size: 28px;
  font-weight: bold;
  
}

.requiestModalclose:hover,
.requiestModalclose:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

/*.requiestModal-header {*/
/*  padding: 7px 16px;*/
/*  background-color: #e0e0e0;*/
/*  color: white;*/
/*}*/

.requiestModal-body {
    
    text-align:left;
    color: #444;
}
.requiestModal-body form{
    padding: 40px;
    padding-top: 20px;
    padding-bottom:20px;

}

.requiestModal-footer {
  padding: 2px 16px;
  background-color: #e0e0e0;
  color: white;
}
                            </style>
                            <script>
                            // Get the modal
                                var modal = document.getElementById("requiestModal");

                                // Get the button that opens the modal
                                var btn = document.getElementById("requiestModalbtn");

                                // Get the <span> element that closes the modal
                                var cancel = document.getElementsByClassName("cancelbtn")[0];

                                // When the user clicks the button, open the modal 
                                btn.onclick = function() {
                                    modal.style.display = "block";
                                }

                                // When the user clicks on <span> (x), close the modal
                                
                                cancel.onclick = function(){
                                    modal.style.display = "none";
                                }

                                // When the user clicks anywhere outside of the modal, close it
                                window.onclick = function(event) {
                                    if (event.target == modal) {
                                        modal.style.display = "none";
                                    }
                                }
                            </script>
                        </div>
                    </div>
                </section>
                <section id="about" class="About">
                    <div class="ev-container row align-items-center About__description">
                        <div class="col-12 col-md-6">
                            <h4></h4>
                            <h3></h3>
                            <h3></h3>
                            <h2><strong>¿Quiénes somos?</strong></h2>
                            <p>&nbsp;</p>
                            <p class="p1"><span style="font-weight: 400;">Nacimos en el año 2010 como una empresa de Grupo Céntrico orientada al desarrollo tecnológico web de varias herramientas.<br>
                                </span>
                            </p>
                            <p class="p1"><span style="font-weight: 400;">En Aspirantecertificado.com somos especialistas en combinar investigación y desarrollo con la más avanzada tecnología de análisis de datos para la evaluación de talento humano.</span></p>
                        </div>
                        <div class="col-12 col-md-6 About__stadistics" style="background-image: url(https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/07/whoIsEvaluar_background.png)">
                            <div class="u-stadistics text-center">
                                <?php if (setting('about-us-section')) { ?>
                                <?php echo setting('about-us-section'); ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </section>
                <!--section id="videos" class="Videos-success" style="background-color: #F8F9FB">
                    <div class="ev-container row align-items-center justify-content-center">
                    <div class="col-12 text-center">
                    <h2>
                    Historias de Éxito
                    </h2>
                    </div>
                    <div class="col-12 col-md-6 text-center">
                    <div class="n2-section-smartslider fitvidsignore" role="region" aria-label="Slider"><style>div#n2-ss-2{width:1200px;}div#n2-ss-2 .n2-ss-slider-1{position:relative;}div#n2-ss-2 .n2-ss-slider-background-video-container{position:absolute;left:0;top:0;width:100%;height:100%;overflow:hidden;}div#n2-ss-2 .n2-ss-slider-2{position:relative;overflow:hidden;padding:0px 0px 0px 0px;height:600px;border:0px solid RGBA(62,62,62,1);border-radius:0px;background-clip:padding-box;background-repeat:repeat;background-position:50% 50%;background-size:cover;background-attachment:scroll;z-index:1;}div#n2-ss-2.n2-ss-mobileLandscape .n2-ss-slider-2,div#n2-ss-2.n2-ss-mobilePortrait .n2-ss-slider-2{background-attachment:scroll;}div#n2-ss-2 .n2-ss-slider-3{position:relative;width:100%;height:100%;overflow:hidden;outline:1px solid rgba(0,0,0,0);z-index:10;}div#n2-ss-2 .n2-ss-slide-backgrounds,div#n2-ss-2 .n2-ss-slider-3 > .n-particles-js-canvas-el,div#n2-ss-2 .n2-ss-slider-3 > .n2-ss-divider{position:absolute;left:0;top:0;width:100%;height:100%;}div#n2-ss-2 .n2-ss-slide-backgrounds{z-index:10;}div#n2-ss-2 .n2-ss-slider-3 > .n-particles-js-canvas-el{z-index:12;}div#n2-ss-2 .n2-ss-slide-backgrounds > *{overflow:hidden;}div#n2-ss-2 .n2-ss-slide{position:absolute;top:0;left:0;width:100%;height:100%;z-index:20;display:block;-webkit-backface-visibility:hidden;}div#n2-ss-2 .n2-ss-layers-container{position:relative;width:1200px;height:600px;}div#n2-ss-2 .n2-ss-parallax-clip > .n2-ss-layers-container{position:absolute;right:0;}div#n2-ss-2 .n2-ss-slide{perspective:1500px;}div#n2-ss-2[data-ie] .n2-ss-slide{perspective:none;transform:perspective(1500px);}div#n2-ss-2 .n2-ss-slide-active{z-index:21;}div#n2-ss-2 .nextend-shadow{width:100%;z-index:0;}div#n2-ss-2 .nextend-shadow img{max-width:none;width:100%;}</style><div id="n2-ss-2-align" class="n2-ss-align n2-ss-align-visible"><div class="n2-padding" style="padding-left: 0px; padding-right: 0px;"><div id="n2-ss-2" data-creator="Smart Slider 3" class="n2-ss-slider n2-ow n2-has-hover  n2-ss-load-fade n2-ss-desktopPortrait n2-ss-loaded" style="font-size: 8.12px; width: 609px;" data-fontsize="16" data-device-mode="desktopPortrait">
                    <div class="n2-ss-slider-1 n2_ss__touch_element n2-ow" style="touch-action: pan-y;">
                    <div class="n2-ss-slider-2 n2-ow" style="height: 304px;">
                    <div class="n2-ss-slider-3 n2-ow" style="">
                    <div class="n2-ss-slide-backgrounds"><div class="n2-ss-slide-background n2-ow" data-mode="fill" style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);"><div class="n2-ss-slide-background-wrap n2-ow"></div></div></div><div data-first="1" data-slide-duration="0" data-id="4" style="width: 609px; height: 304px; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);" class="n2-ss-slide n2-ss-canvas n2-ow  n2-ss-slide-4 n2-ss-slide-active"><div tabindex="-1" class="n2-ss-slide--focus"></div><div class="n2-ss-layers-container n2-ow" style="width: 609px; margin: 0px; font-size: 8.12px; height: 304px;"><div class="n2-ss-layer n2-ow" style="padding:0px 0px 0px 0px;" data-desktopportraitpadding="0|*|0|*|0|*|0" data-sstype="slide" data-csstextalign="center" data-pm="default"><div class="n2-ss-layer n2-ow n2-ss-layer--need-width" style="left: 0px; top: 0px; width: 609px; height: 304px; overflow: visible; font-size: 1rem; right: auto; bottom: auto;" data-pm="absolute" data-responsiveposition="1" data-desktopportraitleft="0" data-desktopportraittop="0" data-responsivesize="1" data-desktopportraitwidth="1200" data-desktopportraitheight="600" data-desktopportraitalign="center" data-desktopportraitvalign="middle" data-parentid="" data-desktopportraitparentalign="center" data-desktopportraitparentvalign="middle" data-sstype="layer" data-rotation="0" data-desktopportrait="1" data-desktoplandscape="1" data-tabletportrait="1" data-tabletlandscape="1" data-mobileportrait="1" data-mobilelandscape="1" data-adaptivefont="1" data-desktopportraitfontsize="100" data-plugin="rendered"><div id="n2-ss-2item1" class="n2_ss_video_player n2-ss-item-content n2-ow-all" data-aspect-ratio="fill"><div class="n2_ss_video_player__placeholder"></div><div class="n2_ss_video_player__cover" style="cursor:pointer; background: URL(//www.evaluar.com/wp-content/uploads/2019/04/Kruger-thumbnail-caso-exito.jpg) no-repeat 50% 50%; background-size: cover"><img style="width:48px;height:48px;margin-left:-24px;margin-top:-24px;" class="" data-no-lazy="1" data-hack="data-lazy-src" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA0OCA0OCI+CiAgICA8Y2lyY2xlIGN4PSIyNCIgY3k9IjI0IiByPSIyNCIgZmlsbD0iIzAwMCIgb3BhY2l0eT0iLjYiLz4KICAgIDxwYXRoIGZpbGw9IiNGRkYiCiAgICAgICAgICBkPSJNMTkuOCAzMmMtLjEyNCAwLS4yNDctLjAyOC0uMzYtLjA4LS4yNjQtLjExNi0uNDM2LS4zNzUtLjQ0LS42NjRWMTYuNzQ0Yy4wMDUtLjI5LjE3Ni0uNTUuNDQtLjY2Ni4yNzMtLjEyNi41OTItLjEuODQuMDdsMTAuNCA3LjI1N2MuMi4xMzIuMzIuMzU1LjMyLjU5NXMtLjEyLjQ2My0uMzIuNTk1bC0xMC40IDcuMjU2Yy0uMTQuMS0uMzEuMTUtLjQ4LjE1eiIvPgo8L3N2Zz4=" alt="Play"></div></div></div></div></div></div> </div>
                    </div>
                    </div>
                    <div data-position="below" class="n2-ss-widget n2-ss-widget-hide-mobileportrait nextend-shadow n2-ow" style="margin-top:0px;width:100%;"><img style="display: block; width:100%;max-width:none;" class="n2-ow nextend-shadow-image" data-no-lazy="1" data-hack="data-lazy-src" src="https://www.aspirantecertificado.com/assets/front/wp-content/plugins/smart-slider-3/Public/SmartSlider3/Widget/Shadow/ShadowImage/Assets/shadow/float.png" alt="Shadow"></div>
                    </div></div></div><div class="n2_clear"></div></div>
                    </div>
                    <div class="col-12 text-center">
                    <p>¿Por qué empresas como la tuya usan Aspirantecertificado.com? <br> Mira las historias de éxito de nuestros clientes.</p>
                    </div>
                    </div>
                    </section-->
                <section id="clients" class="Clients wrap" data-clients="[{&quot;term_id&quot;:8,&quot;name&quot;:&quot;Ecuador&quot;,&quot;slug&quot;:&quot;ecuador&quot;,&quot;term_group&quot;:0,&quot;term_taxonomy_id&quot;:8,&quot;taxonomy&quot;:&quot;clients_categories&quot;,&quot;description&quot;:&quot;Con 8 a\u00f1os de experiencia en el mercado, contamos con oficinas en Quito y Guayaquil para brindar el mejor soporte a nuestros clientes.\r\n\r\nTenemos m\u00e1s de 400 clientes en Ecuador.&quot;,&quot;parent&quot;:0,&quot;count&quot;:18,&quot;filter&quot;:&quot;raw&quot;,&quot;countries_code&quot;:&quot;ec&quot;,&quot;countries_logo&quot;:{&quot;ID&quot;:203,&quot;id&quot;:203,&quot;title&quot;:&quot;Ecuador&quot;,&quot;filename&quot;:&quot;flag-ecuador.png&quot;,&quot;filesize&quot;:2733,&quot;url&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001027\/flag-ecuador.png&quot;,&quot;link&quot;:&quot;https:\/\/www.evaluar.com\/flag-ecuador&quot;,&quot;alt&quot;:&quot;Ecuador&quot;,&quot;author&quot;:&quot;1&quot;,&quot;description&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;name&quot;:&quot;flag-ecuador&quot;,&quot;status&quot;:&quot;inherit&quot;,&quot;uploaded_to&quot;:0,&quot;date&quot;:&quot;2017-08-25 00:27:37&quot;,&quot;modified&quot;:&quot;2018-10-24 17:58:18&quot;,&quot;menu_order&quot;:0,&quot;mime_type&quot;:&quot;image\/png&quot;,&quot;type&quot;:&quot;image&quot;,&quot;subtype&quot;:&quot;png&quot;,&quot;icon&quot;:&quot;https:\/\/www.evaluar.com\/wp-includes\/images\/media\/default.png&quot;,&quot;width&quot;:101,&quot;height&quot;:69,&quot;sizes&quot;:{&quot;thumbnail&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001027\/flag-ecuador.png&quot;,&quot;thumbnail-width&quot;:101,&quot;thumbnail-height&quot;:69,&quot;medium&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001027\/flag-ecuador.png&quot;,&quot;medium-width&quot;:101,&quot;medium-height&quot;:69,&quot;medium_large&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001027\/flag-ecuador.png&quot;,&quot;medium_large-width&quot;:101,&quot;medium_large-height&quot;:69,&quot;large&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001027\/flag-ecuador.png&quot;,&quot;large-width&quot;:101,&quot;large-height&quot;:69,&quot;1536x1536&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001027\/flag-ecuador.png&quot;,&quot;1536x1536-width&quot;:101,&quot;1536x1536-height&quot;:69,&quot;2048x2048&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001027\/flag-ecuador.png&quot;,&quot;2048x2048-width&quot;:101,&quot;2048x2048-height&quot;:69,&quot;testimonial-thumb&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001027\/flag-ecuador.png&quot;,&quot;testimonial-thumb-width&quot;:101,&quot;testimonial-thumb-height&quot;:69}}},{&quot;term_id&quot;:9,&quot;name&quot;:&quot;Per\u00fa&quot;,&quot;slug&quot;:&quot;peru&quot;,&quot;term_group&quot;:0,&quot;term_taxonomy_id&quot;:9,&quot;taxonomy&quot;:&quot;clients_categories&quot;,&quot;description&quot;:&quot;Contamos con m\u00e1s de 250 clientes en Per\u00fa.\r\n\r\nNuestra oficina en Lima cuenta con un equipo de 17 personas para atender de la mejor manera el exigente mercado peruano.&quot;,&quot;parent&quot;:0,&quot;count&quot;:18,&quot;filter&quot;:&quot;raw&quot;,&quot;countries_code&quot;:&quot;pe&quot;,&quot;countries_logo&quot;:{&quot;ID&quot;:204,&quot;id&quot;:204,&quot;title&quot;:&quot;Peru&quot;,&quot;filename&quot;:&quot;flag-peru-3.png&quot;,&quot;filesize&quot;:335,&quot;url&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001028\/flag-peru-3.png&quot;,&quot;link&quot;:&quot;https:\/\/www.evaluar.com\/flag-peru-2&quot;,&quot;alt&quot;:&quot;Peru&quot;,&quot;author&quot;:&quot;1&quot;,&quot;description&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;name&quot;:&quot;flag-peru-2&quot;,&quot;status&quot;:&quot;inherit&quot;,&quot;uploaded_to&quot;:0,&quot;date&quot;:&quot;2017-08-25 00:31:42&quot;,&quot;modified&quot;:&quot;2018-10-24 17:58:23&quot;,&quot;menu_order&quot;:0,&quot;mime_type&quot;:&quot;image\/png&quot;,&quot;type&quot;:&quot;image&quot;,&quot;subtype&quot;:&quot;png&quot;,&quot;icon&quot;:&quot;https:\/\/www.evaluar.com\/wp-includes\/images\/media\/default.png&quot;,&quot;width&quot;:101,&quot;height&quot;:69,&quot;sizes&quot;:{&quot;thumbnail&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001028\/flag-peru-3.png&quot;,&quot;thumbnail-width&quot;:101,&quot;thumbnail-height&quot;:69,&quot;medium&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001028\/flag-peru-3.png&quot;,&quot;medium-width&quot;:101,&quot;medium-height&quot;:69,&quot;medium_large&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001028\/flag-peru-3.png&quot;,&quot;medium_large-width&quot;:101,&quot;medium_large-height&quot;:69,&quot;large&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001028\/flag-peru-3.png&quot;,&quot;large-width&quot;:101,&quot;large-height&quot;:69,&quot;1536x1536&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001028\/flag-peru-3.png&quot;,&quot;1536x1536-width&quot;:101,&quot;1536x1536-height&quot;:69,&quot;2048x2048&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001028\/flag-peru-3.png&quot;,&quot;2048x2048-width&quot;:101,&quot;2048x2048-height&quot;:69,&quot;testimonial-thumb&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001028\/flag-peru-3.png&quot;,&quot;testimonial-thumb-width&quot;:101,&quot;testimonial-thumb-height&quot;:69}}},{&quot;term_id&quot;:10,&quot;name&quot;:&quot;Chile&quot;,&quot;slug&quot;:&quot;chile&quot;,&quot;term_group&quot;:0,&quot;term_taxonomy_id&quot;:10,&quot;taxonomy&quot;:&quot;clients_categories&quot;,&quot;description&quot;:&quot;Contamos con oficinas en Santiago de Chile.\r\n\r\nDurante estos 3 a\u00f1os en el mercado, consolidamos clientes de gran importancia.&quot;,&quot;parent&quot;:0,&quot;count&quot;:12,&quot;filter&quot;:&quot;raw&quot;,&quot;countries_code&quot;:&quot;cl&quot;,&quot;countries_logo&quot;:{&quot;ID&quot;:223,&quot;id&quot;:223,&quot;title&quot;:&quot;flag-chile&quot;,&quot;filename&quot;:&quot;flag-chile.png&quot;,&quot;filesize&quot;:624,&quot;url&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001040\/flag-chile.png&quot;,&quot;link&quot;:&quot;https:\/\/www.evaluar.com\/flag-chile&quot;,&quot;alt&quot;:&quot;&quot;,&quot;author&quot;:&quot;1&quot;,&quot;description&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;name&quot;:&quot;flag-chile&quot;,&quot;status&quot;:&quot;inherit&quot;,&quot;uploaded_to&quot;:0,&quot;date&quot;:&quot;2017-08-25 00:32:42&quot;,&quot;modified&quot;:&quot;2017-08-25 00:32:42&quot;,&quot;menu_order&quot;:0,&quot;mime_type&quot;:&quot;image\/png&quot;,&quot;type&quot;:&quot;image&quot;,&quot;subtype&quot;:&quot;png&quot;,&quot;icon&quot;:&quot;https:\/\/www.evaluar.com\/wp-includes\/images\/media\/default.png&quot;,&quot;width&quot;:101,&quot;height&quot;:69,&quot;sizes&quot;:{&quot;thumbnail&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001040\/flag-chile.png&quot;,&quot;thumbnail-width&quot;:101,&quot;thumbnail-height&quot;:69,&quot;medium&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001040\/flag-chile.png&quot;,&quot;medium-width&quot;:101,&quot;medium-height&quot;:69,&quot;medium_large&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001040\/flag-chile.png&quot;,&quot;medium_large-width&quot;:101,&quot;medium_large-height&quot;:69,&quot;large&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001040\/flag-chile.png&quot;,&quot;large-width&quot;:101,&quot;large-height&quot;:69,&quot;1536x1536&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001040\/flag-chile.png&quot;,&quot;1536x1536-width&quot;:101,&quot;1536x1536-height&quot;:69,&quot;2048x2048&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001040\/flag-chile.png&quot;,&quot;2048x2048-width&quot;:101,&quot;2048x2048-height&quot;:69,&quot;testimonial-thumb&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001040\/flag-chile.png&quot;,&quot;testimonial-thumb-width&quot;:101,&quot;testimonial-thumb-height&quot;:69}}},{&quot;term_id&quot;:11,&quot;name&quot;:&quot;Bolivia&quot;,&quot;slug&quot;:&quot;bo&quot;,&quot;term_group&quot;:0,&quot;term_taxonomy_id&quot;:11,&quot;taxonomy&quot;:&quot;clients_categories&quot;,&quot;description&quot;:&quot;Las empresas m\u00e1s grandes de Bolivia conf\u00edan en Evaluar.\r\n\r\nEn nuestro primer a\u00f1o en el mercado boliviano, la Banca y otras industrias nos ven como sus aliados estrat\u00e9gicos.&quot;,&quot;parent&quot;:0,&quot;count&quot;:12,&quot;filter&quot;:&quot;raw&quot;,&quot;countries_code&quot;:&quot;bo&quot;,&quot;countries_logo&quot;:{&quot;ID&quot;:237,&quot;id&quot;:237,&quot;title&quot;:&quot;Bolivia&quot;,&quot;filename&quot;:&quot;flag-bolivia.png&quot;,&quot;filesize&quot;:396,&quot;url&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001048\/flag-bolivia.png&quot;,&quot;link&quot;:&quot;https:\/\/www.evaluar.com\/flag-bolivia&quot;,&quot;alt&quot;:&quot;&quot;,&quot;author&quot;:&quot;1&quot;,&quot;description&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;name&quot;:&quot;flag-bolivia&quot;,&quot;status&quot;:&quot;inherit&quot;,&quot;uploaded_to&quot;:0,&quot;date&quot;:&quot;2017-08-25 00:33:45&quot;,&quot;modified&quot;:&quot;2017-09-18 18:11:10&quot;,&quot;menu_order&quot;:0,&quot;mime_type&quot;:&quot;image\/png&quot;,&quot;type&quot;:&quot;image&quot;,&quot;subtype&quot;:&quot;png&quot;,&quot;icon&quot;:&quot;https:\/\/www.evaluar.com\/wp-includes\/images\/media\/default.png&quot;,&quot;width&quot;:101,&quot;height&quot;:69,&quot;sizes&quot;:{&quot;thumbnail&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001048\/flag-bolivia.png&quot;,&quot;thumbnail-width&quot;:101,&quot;thumbnail-height&quot;:69,&quot;medium&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001048\/flag-bolivia.png&quot;,&quot;medium-width&quot;:101,&quot;medium-height&quot;:69,&quot;medium_large&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001048\/flag-bolivia.png&quot;,&quot;medium_large-width&quot;:101,&quot;medium_large-height&quot;:69,&quot;large&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001048\/flag-bolivia.png&quot;,&quot;large-width&quot;:101,&quot;large-height&quot;:69,&quot;1536x1536&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001048\/flag-bolivia.png&quot;,&quot;1536x1536-width&quot;:101,&quot;1536x1536-height&quot;:69,&quot;2048x2048&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001048\/flag-bolivia.png&quot;,&quot;2048x2048-width&quot;:101,&quot;2048x2048-height&quot;:69,&quot;testimonial-thumb&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/08\/03001048\/flag-bolivia.png&quot;,&quot;testimonial-thumb-width&quot;:101,&quot;testimonial-thumb-height&quot;:69}}},{&quot;term_id&quot;:13,&quot;name&quot;:&quot;Uruguay&quot;,&quot;slug&quot;:&quot;uruguay&quot;,&quot;term_group&quot;:0,&quot;term_taxonomy_id&quot;:13,&quot;taxonomy&quot;:&quot;clients_categories&quot;,&quot;description&quot;:&quot;\u00a1Gracias Uruguay por su bienvenida!&quot;,&quot;parent&quot;:0,&quot;count&quot;:6,&quot;filter&quot;:&quot;raw&quot;,&quot;countries_code&quot;:&quot;uy&quot;,&quot;countries_logo&quot;:{&quot;ID&quot;:1184,&quot;id&quot;:1184,&quot;title&quot;:&quot;flag-uruguay&quot;,&quot;filename&quot;:&quot;flag-uruguay.png&quot;,&quot;filesize&quot;:1500,&quot;url&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/12\/03001243\/flag-uruguay.png&quot;,&quot;link&quot;:&quot;https:\/\/www.evaluar.com\/flag-uruguay&quot;,&quot;alt&quot;:&quot;Uruguay&quot;,&quot;author&quot;:&quot;1&quot;,&quot;description&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;name&quot;:&quot;flag-uruguay&quot;,&quot;status&quot;:&quot;inherit&quot;,&quot;uploaded_to&quot;:0,&quot;date&quot;:&quot;2017-12-29 15:42:33&quot;,&quot;modified&quot;:&quot;2018-10-24 17:53:37&quot;,&quot;menu_order&quot;:0,&quot;mime_type&quot;:&quot;image\/png&quot;,&quot;type&quot;:&quot;image&quot;,&quot;subtype&quot;:&quot;png&quot;,&quot;icon&quot;:&quot;https:\/\/www.evaluar.com\/wp-includes\/images\/media\/default.png&quot;,&quot;width&quot;:101,&quot;height&quot;:69,&quot;sizes&quot;:{&quot;thumbnail&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/12\/03001243\/flag-uruguay.png&quot;,&quot;thumbnail-width&quot;:101,&quot;thumbnail-height&quot;:69,&quot;medium&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/12\/03001243\/flag-uruguay.png&quot;,&quot;medium-width&quot;:101,&quot;medium-height&quot;:69,&quot;medium_large&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/12\/03001243\/flag-uruguay.png&quot;,&quot;medium_large-width&quot;:101,&quot;medium_large-height&quot;:69,&quot;large&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/12\/03001243\/flag-uruguay.png&quot;,&quot;large-width&quot;:101,&quot;large-height&quot;:69,&quot;1536x1536&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/12\/03001243\/flag-uruguay.png&quot;,&quot;1536x1536-width&quot;:101,&quot;1536x1536-height&quot;:69,&quot;2048x2048&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/12\/03001243\/flag-uruguay.png&quot;,&quot;2048x2048-width&quot;:101,&quot;2048x2048-height&quot;:69,&quot;testimonial-thumb&quot;:&quot;https:\/\/cdn.evaluar.com\/evaluar-web\/wp-content\/uploads\/2017\/12\/03001243\/flag-uruguay.png&quot;,&quot;testimonial-thumb-width&quot;:101,&quot;testimonial-thumb-height&quot;:69}}}]">
                    <div class="ev-container row">
                        <div class="col-12 col-md-5">
                            <div class="Clients__map hidden-sm-down" id="map" style="position: relative; overflow: hidden; background-color: rgb(248, 249, 251);">
                                <svg width="502" height="475">
                                    <g transform="scale(0.5284210526315789) translate(5.3786103503752606e-14, 174.45219123505981)">
                                        <path d="m 534.552,493.51923 -6.16906,-0.68024 -6.14561,4.12835 4.45674,4.83204 7.85793,-8.28015 0,0 m 6.14561,-3.09626 -2.04072,6.54437 -5.81721,5.16043 0.35184,1.71233 9.92211,-3.79996 4.10489,-5.16043 -6.52091,-4.45674 0,0 z" stroke="#818181" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-opacity="0.25" fill="#223A4F" original="#223A4F" id="jqvmap1_fk" class="jqvmap-region" style="fill: rgb(202, 202, 202);"></path>
                                        <path d="m 352.17758,89.293073 -11.09493,6.896216 -0.79752,10.227041 -2.22837,3.35428 6.99004,6.70857 -3.02589,3.30737 0.70369,8.44434 12.50233,2.97898 18.92941,-22.40097 -0.0469,-7.81102 -9.07767,-0.58642 -12.85417,-11.118387 0,0 z" stroke="#818181" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-opacity="0.25" fill="#223A4F" original="#223A4F" id="jqvmap1_ec" class="jqvmap-region"></path>
                                        <path d="m 407.37077,4.6854166 -4.83205,-0.4925868 -31.94777,26.3416682 -3.37774,9.265325 -4.36291,0.492586 1.94689,20.47754 -11.14185,27.326842 12.10356,10.250498 15.50476,0.985174 10.64926,15.622037 15.4813,0.49259 -0.49258,11.7048 h 5.79376 l 6.28634,-21.46271 -5.81721,-7.318434 1.4543,-13.651693 12.10356,-0.985174 -1.4543,-31.713211 -27.11574,-8.772737 -6.28634,-17.076345 15.50476,-21.4861694 0,0 z" stroke="#818181" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-opacity="0.25" fill="#223A4F" original="#223A4F" id="jqvmap1_co" class="jqvmap-region" style="fill: rgb(202, 202, 202);"></path>
                                        <path d="m 399.70048,19.087718 1.03209,6.075238 7.62337,2.416021 1.73578,-11.188758 8.04559,-8.3270639 8.04558,9.4295199 18.50719,5.043151 15.66896,-3.283912 10.67271,13.159106 8.04559,5.043151 -8.81965,13.440584 2.95552,10.180128 -5.04315,6.239434 -5.23081,4.386369 -11.32949,-5.699934 -2.60368,2.62713 v 8.115955 l 8.28015,3.940695 -6.09869,6.591281 -6.09869,6.591281 -8.04559,-0.656782 -8.0925,-8.89002 -1.71232,-33.448993 -27.63178,-9.42952 -5.0197,-14.707236 5.11352,-7.646825 0,0 z" stroke="#818181" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-opacity="0.25" fill="#223A4F" original="#223A4F" id="jqvmap1_ve" class="jqvmap-region" style="fill: rgb(202, 202, 202);"></path>
                                        <path d="m 480.83658,38.345519 16.9356,15.340562 -6.73202,7.787563 -0.5395,4.620934 8.84311,9.124585 -0.21111,8.772738 -15.38747,5.864129 -9.21842,-12.455411 1.97035,-14.965258 -3.94069,-11.141845 8.28015,-12.947997 0,0 z" stroke="#818181" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-opacity="0.25" fill="#223A4F" original="#223A4F" id="jqvmap1_gy" class="jqvmap-region" style="fill: rgb(202, 202, 202);"></path>
                                        <path d="m 499.78944,54.788537 4.78513,4.386369 7.41226,-4.597478 6.75548,0.211109 -0.86789,2.62713 -2.83824,5.911042 -0.44567,14.707236 -13.4875,5.488825 0.65678,-9.42952 -8.70237,-8.115954 0.44568,-4.17526 6.28634,-7.013499 0,0 z" stroke="#818181" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-opacity="0.25" fill="#223A4F" original="#223A4F" id="jqvmap1_sr" class="jqvmap-region" style="fill: rgb(202, 202, 202);"></path>
                                        <path d="m 520.90031,56.336667 13.72206,8.561629 -7.17769,14.261562 -2.60368,3.283912 -7.62337,-4.386368 0.21111,-15.364019 3.47157,-6.356716 0,0 z" stroke="#818181" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-opacity="0.25" fill="#223A4F" original="#223A4F" id="jqvmap1_gf" class="jqvmap-region" style="fill: rgb(202, 202, 202);"></path>
                                        <path d="m 340.05056,121.35813 -4.55056,4.59748 0.30493,7.34189 39.73534,72.43372 41.26002,26.59969 6.38017,-10.69617 1.52467,-23.52688 -3.33082,-14.66033 -11.23568,-18.95286 -6.6851,2.13454 -3.02589,3.35428 -13.34676,-15.29365 3.33082,-18.03806 15.48131,-10.0863 -1.21974,-9.47643 -15.76278,-0.60987 -8.18633,-13.74552 -4.55056,-1.52468 0.30493,8.2567 -20.31334,24.13675 -15.17637,-3.65921 -0.93826,-8.58509 0,0 z" stroke="#818181" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-opacity="0.25" fill="#223A4F" original="#223A4F" id="jqvmap1_pe" class="jqvmap-region"></path>
                                        <path d="m 419.05211,175.94145 19.30471,-8.42089 6.38018,0.60987 4.24563,17.73312 29.41447,9.78137 4.8555,14.98872 12.12702,1.52467 5.16043,12.83071 -3.63576,11.61098 -19.72693,1.52467 -7.27152,18.64793 -15.4813,-0.30493 -4.8555,-0.9148 -8.93693,8.67891 -4.40983,-0.42222 -15.17636,-35.16132 4.19871,-6.28635 1.47776,-24.8639 -3.75304,-14.80107 -3.91724,-6.75547 0,0 z" stroke="#818181" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-opacity="0.25" fill="#223A4F" original="#223A4F" id="jqvmap1_bo" class="jqvmap-region"></path>
                                        <path d="m 496.5759,238.61726 5.16043,5.62956 -0.60987,11.91592 14.87144,-0.91481 11.23567,14.37885 -0.91481,12.83071 -7.27152,11.00111 -14.87143,0.60987 -0.60987,-6.12215 4.24563,-10.08631 -14.5665,-9.17149 h -12.12701 l -9.10113,-9.78137 6.61473,-18.90595 17.94424,-1.38394 0,0 z" stroke="#818181" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-opacity="0.25" fill="#223A4F" original="#223A4F" id="jqvmap1_py" class="jqvmap-region" style="fill: rgb(202, 202, 202);"></path>
                                        <path d="m 516.7485,314.66329 -4.80858,5.13698 1.9938,27.63177 15.106,4.38637 19.21089,-19.2578 -31.50211,-17.89732 0,0 z" stroke="#818181" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-opacity="0.25" fill="#223A4F" original="#223A4F" id="jqvmap1_uy" class="jqvmap-region"></path>
                                        <path d="m 528.73478,282.45749 4.55057,4.26909 -17.28745,25.68488 -6.07524,6.73202 2.11108,29.34411 13.34676,16.20845 -11.21221,19.56273 -8.49126,3.65922 h -9.711 l 2.72096,15.27019 -15.17637,5.20735 3.63576,12.83072 -9.10113,29.03916 11.23567,9.1715 -6.07523,14.96526 -10.32087,16.20845 5.46537,11.30604 -13.34676,2.13455 -10.93074,-13.44059 -1.82961,-41.86988 -16.98252,-71.12016 5.13698,-24.86391 -10.93074,-31.78358 7.27153,-41.26001 6.6851,-7.95176 -1.64195,-6.02833 8.58508,-7.83447 19.14052,1.31356 10.69617,11.42333 12.36159,0.21111 12.66651,7.74065 -3.72958,8.72582 0.89135,8.81965 17.94423,-0.84443 8.39743,-12.83072 0,0 m -38.82053,235.94911 0.60987,13.44058 10.32087,-0.9148 8.79619,-5.81722 -14.87143,-3.04935 -4.8555,-3.65921 0,0 z" stroke="#818181" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-opacity="0.25" fill="#223A4F" original="#223A4F" id="jqvmap1_ar" class="jqvmap-region" style="fill: rgb(202, 202, 202);"></path>
                                        <path d="m 480.81312,507.40549 -10.01593,22.00221 17.28745,1.82961 0.30494,-14.66032 -7.57646,-9.1715 0,0 m -3.40119,-3.44811 -7.52955,8.32706 -0.9148,9.78137 -14.5665,-8.25669 -15.4813,-22.30715 -4.55056,-7.95176 6.38017,-8.25669 -0.60987,-10.39124 -7.27152,-3.04935 -5.7703,-4.26908 1.21974,-5.81722 7.57645,-2.13454 1.52467,-33.61319 -11.82208,-6.73202 -7.71719,-174.96216 1.9938,-3.47157 15.106,34.83293 4.83204,0.0938 1.57159,5.55919 -6.42709,7.78757 -7.3888,41.91679 10.50852,32.27617 -4.8555,24.44169 17.12325,71.87077 1.80616,42.03408 12.26776,14.19119 12.99491,-1.89998 0,0 m -49.98584,-87.93848 -3.02589,4.57402 1.52467,7.95176 3.02589,0.30493 1.52468,-10.0863 -3.04935,-2.74441 0,0 z" stroke="#818181" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-opacity="0.25" fill="#223A4F" original="#223A4F" id="jqvmap1_cl" class="jqvmap-region"></path>
                                        <path d="m 549.30615,330.8952 14.66032,-28.19473 0.5395,-23.69109 27.3503,-17.6393 h 15.31711 l 12.03319,-20.38371 2.18146,-39.12547 -4.92587,-10.46161 28.99225,-26.45895 1.10246,-29.20336 -39.38349,-19.28126 -47.56982,-14.87143 -22.42443,-2.204914 6.02833,-12.666519 -1.64196,-19.281257 -4.90241,-1.6185 -7.24807,14.402301 -3.79995,4.761673 -9.75791,-4.315999 -32.81567,11.564063 -10.93074,-13.768975 1.75924,-14.378845 -10.32087,10.508519 -11.39986,-6.145607 -1.14937,1.6185 0.0235,4.996238 9.82828,5.277716 -14.75414,15.551671 -9.31224,-0.09383 -9.42952,-9.593716 -10.67272,0.328391 -1.31356,11.399868 6.12215,7.435722 -7.22461,23.15158 -8.44434,0.65678 -13.44059,8.49126 -3.28391,16.67759 11.65789,12.47886 2.13454,-2.41602 8.18633,-2.20491 6.99004,11.77517 20.00841,-8.58509 7.7641,0.44568 5.34809,18.92941 28.54658,9.05421 4.92587,15.106 12.15048,1.4543 5.79375,14.42576 -3.91723,12.83072 5.11352,6.70856 -0.75061,9.99248 13.6986,-1.29011 12.54924,15.8566 -0.98517,11.14185 7.43571,6.28635 -17.82695,26.99845 31.38482,17.56893 0,0 z" stroke="#818181" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-opacity="0.25" fill="#223A4F" original="#223A4F" id="jqvmap1_br" class="jqvmap-region" style="fill: rgb(202, 202, 202);"></path>
                                    </g>
                                </svg>
                            </div>
                            <div class="hidden-md-up">
                                <div class="Info" style="display: none;">
                                    <h4><strong>Nuestros clientes en:</strong></h4>
                                    <figure class="js-countries-mb js-country-ec" style="display: none;">
                                        <img src="www.aspirante.htcmagedev.com/assets/front/wp-content/uploads/flag-ecuador.png" alt="Ecuador">
                                        <h4>Ecuador</h4>
                                    </figure>
                                    <figure class="js-countries-mb js-country-pe" style="display: none;">
                                        <img src="www.aspirante.htcmagedev.com/assets/front/wp-content/uploads/flag-peru-3.png" alt="Perú">
                                        <h4>Perú</h4>
                                    </figure>
                                    <figure class="js-countries-mb js-country-cl" style="display: none;">
                                        <img src="www.aspirante.htcmagedev.com/assets/front/wp-content/uploads/flag-chile.png" alt="Chile">
                                        <h4>Chile</h4>
                                    </figure>
                                    <figure class="js-countries-mb js-country-bo" style="display: none;">
                                        <img src="www.aspirante.htcmagedev.com/assets/front/wp-content/uploads/flag-bolivia.png" alt="Bolivia">
                                        <h4>Bolivia</h4>
                                    </figure>
                                    <figure class="js-countries-mb js-country-uy" style="display: none;">
                                        <img src="www.aspirante.htcmagedev.com/assets/front/wp-content/uploads/flag-uruguay.png" alt="Uruguay">
                                        <h4>Uruguay</h4>
                                    </figure>
                                </div>
                                <select name="clients" id="clients-select" class="Clients__select">
                                    <option value="">Seleccionar país</option>
                                    <option value="js-country-ec">Ecuador</option>
                                    <option value="js-country-pe">Perú</option>
                                    <option value="js-country-cl">Chile</option>
                                    <option value="js-country-bo">Bolivia</option>
                                    <option value="js-country-uy">Uruguay</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-7 Info__allCountry">
                            <div class="js-country-">
                                <div>
                                    <h4>Clientes en toda la región</h4>
                                </div>
                                <div>Más de 700 empresas de la región ya trabajan con aspirantecertificado.com</div>
                                <div class="row align-items-center">
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-ec-claro.png" alt="Claro" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-ec-corporacion-favorita.png" alt="Corporación Favorita C.A." class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-ec-chevrolet.png" alt="Chevrolet" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-ec-dhl.png" alt="DHL" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-pe-mapfre.png" alt="Mapfre" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-cl-cencosud.png" alt="Cencosud" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-pe-lg.png" alt="LG" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-pe-sodimac.png" alt="Sodimac" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-pe-banco-falabella.png" alt="Banco Falabella" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-pe-ey-building.png" alt="EY" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-pe-volvo.png" alt="Volvo" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-pe-roche.png" alt="Roche" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-pe-colgate.png" alt="Colgate" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/Dibujo-sin-t%C3%ADtulo-12.png" alt="Citi" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/Samsung.png" alt="Samsung" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/bp.jpg" alt="Banco del Pacífico" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/dc.jpg" alt="Diners Club" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/d.jpg" alt="Deloitte" class="Clients__logo animated">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-7 Info" style="display: none;">
                            <div class="js-countries-dt js-country-ec" style="display: none;">
                                <div class="hidden-sm-down">
                                    <h4><strong>Estamos en:</strong> Ecuador <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/03001027/flag-ecuador.png" alt="Ecuador" class="Info__flag"></h4>
                                </div>
                                <div>Con 8 años de experiencia en el mercado, contamos con oficinas en Quito y Guayaquil para brindar el mejor soporte a nuestros clientes.
                                    Tenemos más de 400 clientes en Ecuador.
                                </div>
                                <div class="row align-items-center">
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-ec-claro.png" alt="Claro" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-ec-corporacion-favorita.png" alt="Corporación Favorita C.A." class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-ec-chevrolet.png" alt="Chevrolet" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-ec-dhl.png" alt="DHL" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-ec-de-prati.png" alt="Deprati" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-ec-netlife.png" alt="Netlife" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-ec-seguros-equinoccial.png" alt="Seguros Equinoccial" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-bo-quimiza.png" alt="Química Suiza" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-cl-gmo.png" alt="GMO" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-ec-grupo-kcf.png" alt="KFC" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-ec-grupo-mavesa.png" alt="Mavesa" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/11/Dibujo-sin-t%C3%ADtulo-16.png" alt="ICESA" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/11/Dibujo-sin-t%C3%ADtulo-17.png" alt="Chaide" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/11/Dibujo-sin-t%C3%ADtulo-18.png" alt="Grupasa" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/05/bp.jpg" alt="Banco del Pacífico" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/05/dc.jpg" alt="Diners Club" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/05/d.jpg" alt="Deloitte" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/05/kruger.png" alt="Kruger Corporation" class="Clients__logo animated">
                                    </div>
                                </div>
                            </div>
                            <div class="js-countries-dt js-country-pe" style="display: none;">
                                <div class="hidden-sm-down">
                                    <h4><strong>Estamos en:</strong> Perú <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/03001028/flag-peru-3.png" alt="Perú" class="Info__flag"></h4>
                                </div>
                                <div>Contamos con más de 250 clientes en Perú.
                                    Nuestra oficina en Lima cuenta con un equipo de 17 personas para atender de la mejor manera el exigente mercado peruano.
                                </div>
                                <div class="row align-items-center">
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-pe-mapfre.png" alt="Mapfre" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-bo-quimiza.png" alt="Química Suiza" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-pe-lg.png" alt="LG" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-pe-sodimac.png" alt="Sodimac" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-pe-banco-falabella.png" alt="Banco Falabella" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-pe-ey-building.png" alt="EY" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-pe-volvo.png" alt="Volvo" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-cl-skechers.png" alt="Skechers" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-pe-ripley.png" alt="Ripley" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-cl-gmo.png" alt="GMO" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-pe-roche.png" alt="Roche" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-pe-colgate.png" alt="Colgate" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-bo-world-vision.png" alt="World Vision" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/11/Dibujo-sin-t%C3%ADtulo-12.png" alt="Citi" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/11/Samsung.png" alt="Samsung" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/11/Dibujo-sin-t%C3%ADtulo-19.png" alt="Caja Trujillo" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/11/Dibujo-sin-t%C3%ADtulo-20.png" alt="Inkafarma" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="www.aspirante.htcmagedev.com/assets/front/wp-content/uploads/2019/08/tfc-logo-financiera.png" alt="Tfc Financiera" class="Clients__logo animated">
                                    </div>
                                </div>
                            </div>
                            <div class="js-countries-dt js-country-cl" style="display: none;">
                                <div class="hidden-sm-down">
                                    <h4><strong>Estamos en:</strong> Chile <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/03001040/flag-chile.png" alt="Chile" class="Info__flag"></h4>
                                </div>
                                <div>Contamos con oficinas en Santiago de Chile.
                                    Durante estos 3 años en el mercado, consolidamos clientes de gran importancia.
                                </div>
                                <div class="row align-items-center">
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-cl-grupo-sura.png" alt="Sura" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-cl-cencosud.png" alt="Cencosud" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-pe-sodimac.png" alt="Sodimac" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-cl-skechers.png" alt="Skechers" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-cl-gmo.png" alt="GMO" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-cl-mega.png" alt="Mega" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-cl-poch.png" alt="Poch" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-bo-world-vision.png" alt="World Vision" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-cl-excon.png" alt="Excon" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-cl-enaex.png" alt="Enaex" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-cl-brotec.png" alt="Brotec" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-cl-barrick.png" alt="Barrick" class="Clients__logo animated">
                                    </div>
                                </div>
                            </div>
                            <div class="js-countries-dt js-country-bo" style="display: none;">
                                <div class="hidden-sm-down">
                                    <h4><strong>Estamos en:</strong> Bolivia <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/flag-bolivia.png" alt="Bolivia" class="Info__flag"></h4>
                                </div>
                                <div>Las empresas más grandes de Bolivia confían en aspirante.
                                    En nuestro primer año en el mercado boliviano, la Banca y otras industrias nos ven como sus aliados estratégicos.
                                </div>
                                <div class="row align-items-center">
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-bo-quimiza.png" alt="Química Suiza" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-bo-world-vision.png" alt="World Vision" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-bo-bcp.png" alt="BCP" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-bo-banco-sol.png" alt="BancoSol" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-bo-unifranz.png" alt="Unifranz" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-bo-banco-fortaleza.png" alt="Banco Fortaleza" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-bo-banco-fie.png" alt="Banco Fie" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/08/clients-bo-ceare.png" alt="Ceare" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/11/grupo-roda-logo-blanco.png" alt="Grupo Roda" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/11/Pan-American-1.png" alt="Panamerican Silver" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/11/Futuro-Bolivia.jpg" alt="AFP" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="www.aspirante.htcmagedev.com/assets/front/wp-content/uploads/2019/05/prodem.png" alt="Banco Prodem" class="Clients__logo animated">
                                    </div>
                                </div>
                            </div>
                            <div class="js-countries-dt js-country-uy" style="display: none;">
                                <div class="hidden-sm-down">
                                    <h4><strong>Estamos en:</strong> Uruguay <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/12/flag-uruguay.png" alt="Uruguay" class="Info__flag"></h4>
                                </div>
                                <div>¡Gracias Uruguay por su bienvenida!</div>
                                <div class="row align-items-center">
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/05/Point-Americas-1.jpg" alt="Point" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/05/GAMM-Vertical.jpg" alt="GAMM Consultores" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/05/republica.jpg" alt="República Microfinanzas" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/05/lumin.jpg" alt="Lumin" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/05/hiperviajes.jpg" alt="Hiper Viajes" class="Clients__logo animated">
                                    </div>
                                    <div class="col-3 col-md-2 text-center">
                                        <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/05/saman.jpg" alt="Saman" class="Clients__logo animated">
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <a href="#" class="ev__button return-clients-regionals">Conoce los clientes regionales.</a>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="testimonials" class="Testimonials">
                    <div class="col-12">
                        <div id="owl-demo" class="Testimonials__carousel owl-carousel owl-theme owl-loaded owl-drag">
                            <div class="owl-item">
                                <div class="Testimonials__item">
                                    <div class="col-md-8 offset-md-2">
                                        <div class="text-center">
                                            <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/05/image-1-1.png" class="Testimonials__avatar">
                                            <p class="Testimonials__quote">"La plataforma nos está apoyando desde hace un año en selecciones de múltiples cargos y la encontramos de mucha utilidad, permitiendo diseñar procesos totalmente a medida, optimizando los tiempos de evaluación de candidatos y abriendo la posibilidad de postulación a personas que de otra forma no hubiésemos considerado por los tiempos que insume un proceso de selección tradicional. Contamos con el apoyo constante de nuestra ejecutiva de cuentas tanto para la configuración del proceso como para solucionar los imprevistos que surgen a medida que aprendemos a usar la herramienta."</p>
                                            <div class="Testimonials__author">
                                                <strong class="Testimonials__author--name">Valentina Scarzolo</strong>
                                                <span class="Testimonials__author--title">Responsable de RRHH HIPERVIAJES</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="owl-item">
                                <div class="Testimonials__item">
                                    <div class="col-md-8 offset-md-2">
                                        <div class="text-center">
                                            <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/05/unnamed-9.png" class="Testimonials__avatar">
                                            <p class="Testimonials__quote">"Las evaluaciones de desempeño, son una excelente ayuda para conocer las fortalezas y las oportunidades de mejora y desarrollo de nuestro capital humano y es por esto que, luego de realizarlas períodicamente de manera manual, identificamos Aspirantecertificado.com una plataforma eficiente, que automatiza y agiliza el proceso, para todas las partes involucradas. Resulta ideal también a la hora de contar con informes en tiempo real, registrar comentarios de avance, promover y documentar las reuniones de feedback, entre otros beneficios. Así mismo, el equipo de aspirante demostró ser muy profesional y colaborador a la hora de apoyarnos en el desarrollo las Competencias y Objetivos a relevar, en un lenguaje claro y comprensible para todas las áreas y niveles jerárquicos."</p>
                                            <div class="Testimonials__author">
                                                <strong class="Testimonials__author--name">Mara Pisano</strong>
                                                <span class="Testimonials__author--title">Gerente de RRHH Lumin</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="owl-item">
                                <div class="Testimonials__item">
                                    <div class="col-md-8 offset-md-2">
                                        <div class="text-center">
                                            <img src="https://www.aspirantecertificado.com/assets/front/wp-content/uploads/2017/04/noelia-sierra-testimonio.png" class="Testimonials__avatar">
                                            <p class="Testimonials__quote">"Aspirante nos ha permitido continuar trabajando con el equipo de profesionales estables sin sumar funcionarios, manteniendo los costos operativos controlados. La plataforma mantiene una inmejorable relación servicio/precio. El equipo se ha adaptado muy bien a su uso, es dinámico, muy intuitivo y nos acerca un montón de indicadores.&nbsp;La recomiendo ampliamente."</p>
                                            <div class="Testimonials__author">
                                                <strong class="Testimonials__author--name">Noelia Sierra</strong>
                                                <span class="Testimonials__author--title">Gerente de Gestión Humana República Microfinanzas</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="subscribe" class="Subscribe">
                    <div class="ev-container row align-items-center">
                        <div class="col-12">
                            <h6>Suscríbete a nuestro boletín</h6>
                        </div>
                        <div id="subscribeForm" class="col-12 Subscribe__form">
                            <form novalidate="" accept-charset="UTF-8" action="https://forms.hsforms.com/submissions/v3/public/submit/formsnext/multipart/398634/bb211d1f-c959-40e5-9b61-0327c69ba528" enctype="multipart/form-data" id="hsForm_bb211d1f-c959-40e5-9b61-0327c69ba528" method="POST" class="hs-form stacked hs-form-private hsForm_bb211d1f-c959-40e5-9b61-0327c69ba528 hs-form-bb211d1f-c959-40e5-9b61-0327c69ba528 hs-form-bb211d1f-c959-40e5-9b61-0327c69ba528_2a98b59a-4ce1-4c43-9d3d-78f7185466b4" data-form-id="bb211d1f-c959-40e5-9b61-0327c69ba528" data-portal-id="398634" target="target_iframe_bb211d1f-c959-40e5-9b61-0327c69ba528" data-reactid=".hbspt-forms-0">
                                <div class="hs_firstname hs-firstname hs-fieldtype-text field hs-form-field" data-reactid=".hbspt-forms-0.1:$0">
                                    <label id="label-firstname-bb211d1f-c959-40e5-9b61-0327c69ba528" class="" placeholder="Enter your Nombre" for="firstname-bb211d1f-c959-40e5-9b61-0327c69ba528" data-reactid=".hbspt-forms-0.1:$0.0"><span data-reactid=".hbspt-forms-0.1:$0.0.0">Nombre</span><span class="hs-form-required" data-reactid=".hbspt-forms-0.1:$0.0.1">*</span></label>
                                    <legend class="hs-field-desc" style="display:none;" data-reactid=".hbspt-forms-0.1:$0.1"></legend>
                                    <div class="input" data-reactid=".hbspt-forms-0.1:$0.$firstname"><input id="firstname-bb211d1f-c959-40e5-9b61-0327c69ba528" class="hs-input" type="text" name="firstname" required="" value="" placeholder="Ingresa tu Nombre" autocomplete="given-name" data-reactid=".hbspt-forms-0.1:$0.$firstname.0"></div>
                                </div>
                                <div class="hs_email hs-email hs-fieldtype-text field hs-form-field" data-reactid=".hbspt-forms-0.1:$1">
                                    <label id="label-email-bb211d1f-c959-40e5-9b61-0327c69ba528" class="" placeholder="Enter your Email" for="email-bb211d1f-c959-40e5-9b61-0327c69ba528" data-reactid=".hbspt-forms-0.1:$1.0"><span data-reactid=".hbspt-forms-0.1:$1.0.0">Email</span><span class="hs-form-required" data-reactid=".hbspt-forms-0.1:$1.0.1">*</span></label>
                                    <legend class="hs-field-desc" style="display:none;" data-reactid=".hbspt-forms-0.1:$1.1"></legend>
                                    <div class="input" data-reactid=".hbspt-forms-0.1:$1.$email"><input id="email-bb211d1f-c959-40e5-9b61-0327c69ba528" class="hs-input" type="email" name="email" required="" placeholder="Ingresa tu email" value="" autocomplete="email" data-reactid=".hbspt-forms-0.1:$1.$email.0"></div>
                                </div>
                                <div class="hs_blog_default_hubspot_blog_subscription hs-blog_default_hubspot_blog_subscription hs-fieldtype-radio field hs-form-field" style="display:none;" data-reactid=".hbspt-forms-0.1:$2">
                                    <label id="label-blog_default_hubspot_blog_subscription-bb211d1f-c959-40e5-9b61-0327c69ba528" class="" placeholder="Enter your Frecuencia de Notificaciones" for="blog_default_hubspot_blog_subscription-bb211d1f-c959-40e5-9b61-0327c69ba528" data-reactid=".hbspt-forms-0.1:$2.0"><span data-reactid=".hbspt-forms-0.1:$2.0.0">Frecuencia de Notificaciones</span></label>
                                    <legend class="hs-field-desc" style="display:none;" data-reactid=".hbspt-forms-0.1:$2.1"></legend>
                                    <div class="input" data-reactid=".hbspt-forms-0.1:$2.$blog_default_hubspot_blog_subscription"><input name="blog_default_hubspot_blog_subscription" class="hs-input" type="hidden" value="weekly" data-reactid=".hbspt-forms-0.1:$2.$blog_default_hubspot_blog_subscription.0"></div>
                                </div>
                                <div class="hs_lifecyclestage hs-lifecyclestage hs-fieldtype-text field hs-form-field" style="display:none;" data-reactid=".hbspt-forms-0.1:$3">
                                    <label id="label-lifecyclestage-bb211d1f-c959-40e5-9b61-0327c69ba528" class="" placeholder="Enter your Lifecycle Stage" for="lifecyclestage-bb211d1f-c959-40e5-9b61-0327c69ba528" data-reactid=".hbspt-forms-0.1:$3.0"><span data-reactid=".hbspt-forms-0.1:$3.0.0">Lifecycle Stage</span></label>
                                    <legend class="hs-field-desc" style="display:none;" data-reactid=".hbspt-forms-0.1:$3.1"></legend>
                                    <div class="input" data-reactid=".hbspt-forms-0.1:$3.$lifecyclestage"><input name="lifecyclestage" class="hs-input" type="hidden" value="subscriber" data-reactid=".hbspt-forms-0.1:$3.$lifecyclestage.0"></div>
                                </div>
                                <div class="hs_producto_de_interes hs-producto_de_interes hs-fieldtype-select field hs-form-field" style="display:none;" data-reactid=".hbspt-forms-0.1:$4">
                                    <label id="label-producto_de_interes-bb211d1f-c959-40e5-9b61-0327c69ba528" class="" placeholder="Enter your Producto de Interés" for="producto_de_interes-bb211d1f-c959-40e5-9b61-0327c69ba528" data-reactid=".hbspt-forms-0.1:$4.0"><span data-reactid=".hbspt-forms-0.1:$4.0.0">Producto de Interés</span></label>
                                    <legend class="hs-field-desc" style="display:none;" data-reactid=".hbspt-forms-0.1:$4.1"></legend>
                                    <div class="input" data-reactid=".hbspt-forms-0.1:$4.$producto_de_interes"><input name="producto_de_interes" class="hs-input" type="hidden" value="" data-reactid=".hbspt-forms-0.1:$4.$producto_de_interes.0"></div>
                                </div>
                                <noscript data-reactid=".hbspt-forms-0.2"></noscript>
                                <div class="hs_submit hs-submit" data-reactid=".hbspt-forms-0.5">
                                    <div class="hs-field-desc" style="display:none;" data-reactid=".hbspt-forms-0.5.0"></div>
                                    <div class="actions" data-reactid=".hbspt-forms-0.5.1"><input type="submit" value="Enviar" class="hs-button primary large" data-reactid=".hbspt-forms-0.5.1.0"></div>
                                </div>
                                <noscript data-reactid=".hbspt-forms-0.6"></noscript>
                                <input name="hs_context" type="hidden" value="{&quot;rumScriptExecuteTime&quot;:2999.6000000001004,&quot;rumServiceResponseTime&quot;:4418.540000000576,&quot;rumFormRenderTime&quot;:4.514999999628344,&quot;rumTotalRenderTime&quot;:4424.930000000131,&quot;rumTotalRequestTime&quot;:1242.9400000000896,&quot;lang&quot;:&quot;esMX&quot;,&quot;renderRawHtml&quot;:&quot;true&quot;,&quot;embedAtTimestamp&quot;:&quot;1611046178893&quot;,&quot;formDefinitionUpdatedAt&quot;:&quot;1585183610877&quot;,&quot;pageUrl&quot;:&quot;https://www.aspirantecertificado.com/assets/front/&quot;,&quot;pageTitle&quot;:&quot;Evaluar.com | Soluciones para Talento Humano&quot;,&quot;source&quot;:&quot;FormsNext-static&quot;,&quot;timestamp&quot;:1611046178893,&quot;userAgent&quot;:&quot;Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.162 Safari/537.36&quot;,&quot;originalEmbedContext&quot;:{&quot;portalId&quot;:&quot;398634&quot;,&quot;target&quot;:&quot;#subscribeForm&quot;,&quot;formId&quot;:&quot;bb211d1f-c959-40e5-9b61-0327c69ba528&quot;},&quot;redirectUrl&quot;:&quot;http://landing.evaluar.com/agradecimiento&quot;,&quot;formTarget&quot;:&quot;#subscribeForm&quot;,&quot;correlationId&quot;:&quot;f73b3ae3-073d-4c0c-b688-e7dcd3fd68b4&quot;,&quot;hutk&quot;:&quot;57d511789a3fc5de6b9c7bba50d0dfdb&quot;,&quot;useRecaptchaEnterprise&quot;:true}" data-reactid=".hbspt-forms-0.7"><iframe name="target_iframe_bb211d1f-c959-40e5-9b61-0327c69ba528" style="display:none;" data-reactid=".hbspt-forms-0.8"></iframe>
                            </form>
                        </div>
                    </div>
                </section>
                <!-- Blog -->
                <section id="blog" class="Blog">
                    <div class="ev-container row align-items-center">
                        <div class="col-12">
                            <h2>Blog</h2>
                        </div>
                        <?php $stt = 0; ?>
                        <?php foreach ($blogs as $blog): ?>
                        <?php $stt++; ?>
                        <?php if ($stt >= 5) {break;}?>
                        <?php 
                            $imgBlogUrl = '';
                            switch ($stt) {
                              case 1: 
                                $imgBlogUrl = 'https://f.hubspotusercontent30.net/hubfs/398634/social-suggested-images/universidad-corporativa-1.png';
                                break;
                              case 2:
                                $imgBlogUrl = 'https://f.hubspotusercontent30.net/hubfs/398634/Fotos-blog-home/TAL.png';
                                break;
                              case 3:
                                $imgBlogUrl = 'https://f.hubspotusercontent30.net/hubfs/398634/job_interview_ok.jpg';
                                break;
                              default:
                                $imgBlogUrl = 'https://www.aspirantecertificado.com/assets/front/images/digitalizar.jpg';
                                break;
                            }
                            ?>
                        <article class="col-12 col-sm-6">
                            <div class="row">
                                <div class="col-sm-4">
                                    <a href="<?php echo base_url(); ?>blog/<?php echo encode($blog['blog_id']); ?>"><img class="d-flex mr-3" src="<?= $imgBlogUrl?>" alt="Lecciones 2020 que nos ayudarán ser más eficientes en 2021"></a>
                                </div>
                                <div class="col-sm-8">
                                    <h4 class="mt-0"><a href="<?php echo base_url(); ?>blog/<?php echo encode($blog['blog_id']); ?>"><?= $blog['title']?></a></h4>
                                </div>
                            </div>
                        </article>
                        <?php endforeach ?>
                    </div>
                </section>
                <!-- End Blog -->
            </main>
        </div>
    </div>
    <?php include(VIEW_ROOT.'/front/layout/footer.php'); ?>
    <div class="modal fade" id="modalHubspot" tabindex="-1" role="dialog" aria-labelledby="modalHubspotLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalHubspotLabel">Contacto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body"></div>
            </div>
        </div>
    </div>
    <style>
        .Funnel__section--top {
            padding: 0px;
            margin-bottom: 0;
            line-break: anywhere;
        }
    </style>
    <!--<script type="text/javascript" src="https://www.aspirantecertificado.com/assets/front/wp-content/plugins/contact-form-7/includes/js/scripts.js" id="contact-form-7-js"></script>-->
    <script type="text/javascript" src="https://www.aspirantecertificado.com/assets/front/wp-content/themes/evaluar/dist/scripts/main_40e37c92.js" id="sage/main.js-js"></script><iframe data-olark="true" aria-hidden="true" title="empty" style="width: 0px; height: 0px; position: absolute; left: 0px; bottom: 0px; border: none;"></iframe>
    <script type="text/javascript" src="https://www.aspirantecertificado.com/assets/front/wp-includes/js/wp-embed.min.js" id="wp-embed-js"></script>
    <div style="font-size:10rem;"></div>
    <div class="jqvmap-label" style="display: none;"></div>
    <!--<script type="text/javascript" id="">_linkedin_partner_id="1231978";window._linkedin_data_partner_ids=window._linkedin_data_partner_ids||[];window._linkedin_data_partner_ids.push(_linkedin_partner_id);</script><script type="text/javascript" id="">(function(){var b=document.getElementsByTagName("script")[0],a=document.createElement("script");a.type="text/javascript";a.async=!0;a.src="https://snap.licdn.com/li.lms-analytics/insight.min.js";b.parentNode.insertBefore(a,b)})();</script>-->
    <!--<noscript>-->
        <!--<img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=1231978&amp;fmt=gif">-->
    <!--</noscript>-->

</body>