  <!--==========================
    Intro Section
  ============================-->
  <section id="intro" class="clearfix front-intro-section">
    <div class="container">
      <div class="intro-img">
      </div>
      <div class="intro-info">
        <h2>
          <span>
            <a href="<?php echo base_url(); ?>business"><?php echo lang('business'); ?></a> 
          </span>
        </h2>
      </div>
    </div>
  </section><!-- #intro --> 
  <div class="container">
    <header class="section-header mt-5">
      <h3><?php echo lang('business'); ?></h3>
    </header>
    <div class="row row-eq-height justify-content-center">
      <div class="col-lg-12 mb-12" style="margin-bottom: 50px">
        <div>
            <p><span style="font-size:12pt"><span >Publica&nbsp;tus&nbsp;vacantes&nbsp;e&nbsp;inicia&nbsp;el&nbsp;proceso&nbsp;de&nbsp;reclutamiento.</span></span></p>

            <p><span style="font-size:12pt"><span >Nosotros&nbsp;nos&nbsp;encargamos&nbsp;de&nbsp;validar&nbsp;la&nbsp;informaci&oacute;n&nbsp;de&nbsp;los&nbsp;candidatos.</span></span></p>
            
            <p><span style="font-size:12pt"><span >Entregamos&nbsp;un&nbsp;estudio&nbsp;socio&nbsp;econ&oacute;mico&nbsp;y&nbsp;perfil&nbsp;del&nbsp;aspirante.</span></span></p>
            
            <p><span style="font-size:12pt"><span >Podr&aacute;s&nbsp;tener&nbsp;un&nbsp;panel&nbsp;de&nbsp;control&nbsp;donde&nbsp;llevar&nbsp;el&nbsp;expediente&nbsp;de&nbsp;tus&nbsp;vacantes.</span></span></p>
            
            <p>&nbsp;</p>
            
            <p><span style="font-size:12pt"><span >Entrevistas.</span></span></p>
            
            <p><span style="font-size:12pt"><span >Documentos&nbsp;verificados</span></span></p>
            
            <p><span style="font-size:12pt"><span >Comentarios&nbsp;de&nbsp;varios&nbsp;operadores</span></span></p>

        </div>
      </div>
    </div>
  </div>
 </main>

  <?php include(VIEW_ROOT.'/front/layout/footer.php'); ?>
