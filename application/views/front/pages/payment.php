<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Checkout</title>
    <script type="text/javascript" src="https://pay.conekta.com/v1.0/js/conekta-checkout.min.js"></script>
</head>
<body>
   <div id="conektaIframeContainer" style="height: 700px;"></div>
    <script type="text/javascript">
    window.ConektaCheckoutComponents.Integration({
        targetIFrame: "#conektaIframeContainer",
        checkoutRequestId: "<?=$checkout_request_id?>",
        publicKey: "key_EMQqzc6zizscLEtLRJNp9SA",
        options: {},
        styles: {},
        onFinalizePayment: function(event){
            console.log(event);
        }
    })
    </script>
</body>
</html>