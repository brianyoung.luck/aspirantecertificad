  <!--==========================
    Intro Section
  ============================-->
      <section id="intro" class="clearfix front-intro-section">
        <div class="container">
          <div class="intro-img">
          </div>
          <div class="intro-info">
            <h2>
              <span>
                <a href="<?php echo base_url(); ?>aboutus"><?php echo lang('about_us'); ?></a> 
              </span>
            </h2>
          </div>
        </div>
      </section><!-- #intro --> 
      <div class="container">
        <header class="section-header mt-5">
          <h3><?php echo lang('about_us'); ?></h3>
        </header>
        <div class="row row-eq-height justify-content-center">
          <div class="col-lg-12 mb-12" style="margin-bottom: 50px">
            <div>
                <p><?php echo lang('about_content_1'); ?></p>
                <h5 class="card-title"><?php echo lang('about_content_2'); ?></h5>
                <p><?php echo lang('about_content_3'); ?></p>
                <p><?php echo lang('about_content_4'); ?></p>
                <p><?php echo lang('about_content_5'); ?></p>
            </div>
          </div>
        </div>
      </div>
  </main>

  <?php include(VIEW_ROOT.'/front/layout/footer.php'); ?>
