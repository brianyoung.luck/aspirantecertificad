  <!--==========================
    Intro Section
  ============================-->
      <section id="intro" class="clearfix front-intro-section">
        <div class="container">
          <div class="intro-img">
          </div>
          <div class="intro-info">
            <h2>
              <span>
                <a href="<?php echo base_url(); ?>candidate"><?php echo lang('candidate'); ?></a> 
              </span>
            </h2>
          </div>
        </div>
      </section><!-- #intro --> 
      <div class="container">
        <header class="section-header mt-5">
          <h3><?php echo lang('candidate'); ?></h3>
        </header>
        <div class="row row-eq-height justify-content-center">
          <div class="col-lg-12 mb-12" style="margin-bottom: 50px">
            <div>
            <p><span style="font-size:12pt"><span >A&nbsp;los&nbsp;aspirantes&nbsp;les&nbsp;permite&nbsp;ver&nbsp;el&nbsp;estado&nbsp;de&nbsp;una&nbsp;contrataci&oacute;n,&nbsp;pre&nbsp;calificar&nbsp;y&nbsp;tener&nbsp;en&nbsp;una&nbsp;b&oacute;veda&nbsp;de&nbsp;documentos&nbsp;confidenciales.</span></span></p>

            <p><span style="font-size:12pt"><span >Los&nbsp;aspirantes&nbsp;podr&aacute;n&nbsp;limitar&nbsp;el&nbsp;acceso&nbsp;o&nbsp;permitir&nbsp;para&nbsp;futuros&nbsp;procesos&nbsp;siempre&nbsp;autorizando&nbsp;qui&eacute;n&nbsp;podr&aacute;&nbsp;verlos.</span></span></p>
            
            <p><span style="font-size:12pt"><span >Las&nbsp;empresas&nbsp;podr&aacute;n&nbsp;tener&nbsp;una&nbsp;validaci&oacute;n&nbsp;del&nbsp;candidato&nbsp;en&nbsp;autom&aacute;tico&nbsp;validando&nbsp;la&nbsp;informaci&oacute;n.</span></span></p>
            
            <p>&nbsp;</p>
            
            <p><span style="font-size:12pt"><span >Sencillos&nbsp;Pasos&nbsp;Para&nbsp;Aspirantes.</span></span></p>
            
            <p>&nbsp;</p>
            
            <p><span style="font-size:12pt"><span >1)&nbsp;Reg&iacute;strate</span></span></p>
            
            <p><span style="font-size:12pt"><span >2)&nbsp;Sube&nbsp;tus&nbsp;documentos</span></span></p>
            
            <p><span style="font-size:12pt"><span >3)&nbsp;Aspirante&nbsp;Certificado.&nbsp;certificar&aacute;&nbsp;tus&nbsp;CV.</span></span></p>
            
            <p><span style="font-size:12pt"><span >4)&nbsp;Podr&aacute;s&nbsp;Pre&nbsp;Calificar&nbsp;a&nbsp;distintas&nbsp;Vacantes.</span></span></p>
            
            <p>&nbsp;</p>
            
            <p><span style="font-size:12pt"><span >Emitimos&nbsp;un&nbsp;certificado&nbsp;de&nbsp;validaci&oacute;n&nbsp;de&nbsp;informaci&oacute;n.</span></span></p>

            </div>
          </div>
        </div>
      </div>
  </main>

  <?php include(VIEW_ROOT.'/front/layout/footer.php'); ?>
